package cz.mrqev.manager;

import com.google.gson.internal.LinkedTreeMap;
import com.ibm.mq.MQException;
import com.ibm.mq.MQQueue;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.main.Main;
import cz.mrqev.model.mq.wrapper.transport.MQRequest;
import cz.mrqev.model.mq.actions.*;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.session.SessionWrapper;
import cz.mrqev.model.ws.actions.WSWriterReader;
import cz.mrqev.model.ws.wrapper.transport.HttpHeader;
import cz.mrqev.model.ws.wrapper.transport.WSRequest;
import cz.mrqev.model.ws.wrapper.transport.WSResponse;
import cz.mrqev.utils.cmd.CommandLineParser;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * possible combinations for command line interface:
 *
 * -help            shows help with all possible combinations
 *
 * Calling MQ / WS
 * -mq | -ws      call MQ or WS (required)
 * -session         path to session file (required)
 * -prettyprint     pretty prints response
 * -verbose         shows information about call process
 * -count           how many times is call repeated (default=1)
 * -json            returns response with SID as json
 *
 * Save queue
 * -queue           source queue name
 * -qmgr            source queue manager name
 * -file            target file
 *
 * Load queue
 * -queue           target queue name
 * -qmgr            target queue manager name
 * -file            source file
 */
public class CommandLineManager {
    private CommandLineParser cmdParser;

    public CommandLineManager(String[] args) {
        cmdParser = new CommandLineParser(args);

        // Help
        if (cmdParser.checkArgument("-help")) {
            cmdParser.printHelp();
            System.exit(0);
        }
        // Save queue
        if (cmdParser.checkArgument("-savequeue")) {
            if (!cmdParser.checkArgument("-queue", "-qmgr", "-file")) {
                System.out.println("Missing required parameters");
                System.exit(0);
            }

            String queueName = cmdParser.getArgumentValue("-queue");
            String queueMgr = cmdParser.getArgumentValue("-qmgr");
            String file = cmdParser.getArgumentValue("-file");

            System.out.println("");
            System.out.println(String.format("Saving queue %s on %s to file %s", queueName, queueMgr, file));

            MQConnectionEntry connectionEntry = Main.configManager.MQConnections.stream().filter(a -> a.getQueueManager().equals(queueMgr)).findFirst().get();

            MQSaver mqSaver = new MQSaver(file, connectionEntry, queueName, this::printToConsole);
            mqSaver.save();

            System.out.println("Done! :)");
        }
        // Load queue
        if (cmdParser.checkArgument("-loadqueue")) {
            if (!cmdParser.checkArgument("-queue", "-qmgr", "-file")) {
                System.out.println("Missing required parameters");
                System.exit(0);
            }

            String queueName = cmdParser.getArgumentValue("-queue");
            String queueMgr = cmdParser.getArgumentValue("-qmgr");
            String file = cmdParser.getArgumentValue("-file");


            System.out.println("");
            System.out.println(String.format("Loading queue %s on %s from file %s", queueName, queueMgr, file));

            MQLoader mqLoader = new MQLoader(file, queueName, queueMgr, this::printToConsole);
            mqLoader.load();

            System.out.println("Done! :)");
        }
        // Copy queue
        if (cmdParser.checkArgument("-copyqueue")) {
            if (!cmdParser.checkArgument("-sourcequeue", "-sourceqmgr", "-targetqueue")) {
                System.out.println("Missing required parameters");
                System.exit(0);
            }

            String sourceQueueName = cmdParser.getArgumentValue("-sourcequeue");
            String sourceQueueMgr = cmdParser.getArgumentValue("-sourceqmgr");;
            String targetQueueName = cmdParser.getArgumentValue("-targetqueue");;
            String targetQueueMgr = cmdParser.getArgumentValue("-targetqmgr");;

            if (targetQueueMgr == null) {
                targetQueueMgr = sourceQueueMgr;
            }

            System.out.println("");
            System.out.println(String.format("Copying messages from %s on %s -> %s on %s", sourceQueueName, sourceQueueMgr, targetQueueName, targetQueueMgr));

            try {
                MQConnectionEntry connectionEntry = Main.configManager.MQConnections.stream().filter(a -> a.getQueueManager().equals(sourceQueueMgr)).findFirst().get();
                MQBrowser browser = new MQBrowser(connectionEntry, sourceQueueName, this::printToConsole);

                ArrayList<MQMessageWrapper> messages = browser.browseMessages();
                ArrayList<MQRequest> requests = new ArrayList<>();

                for (MQMessageWrapper message: messages) {
                    requests.add(new MQRequest(message, false, false));
                }

                MQLoader mqLoader = new MQLoader(requests, targetQueueName, targetQueueMgr, this::printToConsole);
                mqLoader.load();

                System.out.println("Done! :)");
            } catch (MQException e) {
                e.printStackTrace();
            }
        }
        // WS call
        if (cmdParser.checkArgument("-ws")) {
            if (!cmdParser.checkArgument("-session")) {
                System.out.println("Missing required parameters");
                System.exit(0);
            }

            SessionManager sessionManager = new SessionManager();
            sessionManager.setPath(cmdParser.getArgumentValue("-session"));
            callWebService(sessionManager.createSessionWrapper());
        }
        // MQ call
        if (cmdParser.checkArgument("-mq")) {
            if (!cmdParser.checkArgument("-session")) {
                System.out.println("Missing required parameters");
                System.exit(0);
            }

            SessionManager sessionManager = new SessionManager();
            sessionManager.setPath(cmdParser.getArgumentValue("-session"));
            callMQ(sessionManager.createSessionWrapper());
        }

        System.exit(0);
    }

    private void printToConsole(String message, boolean whatever) {
        printToConsole(message);
    }

    private void printToConsole(String message) {
        message = message.replace("ended browsing", "Saved")
                .replace("browsing", "Saving")
                .replace("browsed", "Saved");

        System.out.println(message);
    }

    private void printResponse(String response, String sid, boolean verbose, boolean prettyPrint, boolean json, int count) {
        if (verbose || count>1)
            System.out.println("Got response:");

        if (json) {
            System.out.println("{\"sid\": \"" + sid + "\", \"data\": \"" + response.replace("\"", "\\\"") + "\"}");
        } else if (prettyPrint) {
            System.out.println(UtilsManager.prettyPrint(response));
        } else {
            System.out.println(response);
        }
    }

    private void callWebService(SessionWrapper session) {
        int count = (cmdParser.getArgumentValue("-ws").isEmpty()) ? 1 : cmdParser.getIntArgumentValue("-ws");

        boolean verbose = cmdParser.checkArgument("-verbose");
        boolean prettyPrint = cmdParser.checkArgument("-prettyprint");
        boolean json = cmdParser.checkArgument("-json");
        boolean soap = cmdParser.checkArgument("-soap") || !cmdParser.checkArgument("-rest");
        HttpHeader httpHeader = new HttpHeader();
        session.getProperty("HttpHeader").ifPresent(value -> httpHeader.saveProperties((LinkedTreeMap<String, String>) value));

        for (int i = 1; i <= count; i++) {
            if (count>1) System.out.println("Call "+i+"/"+count);
            try {
                WSRequest request;
                // in old sessions there are no AddWSS element so when its null we have to full wss
                if (!session.getProperty("WSAddWss").isPresent() || session.getBoolProperty("WSAddWss")) {
                    request = new WSRequest(
                            session.getStringProperty("Data"),
                            session.getBoolProperty("WSFillTimestamps"),
                            session.getBoolProperty("WSFillSid"),
                            soap,
                            session.getStringProperty("WSSUser"),
                            session.getStringProperty("WSSPassword"),
                            httpHeader);
                } else {
                    request = new WSRequest(
                            session.getStringProperty("Data"),
                            session.getBoolProperty("WSFillTimestamps"),
                            session.getBoolProperty("WSFillSid"),
                            soap,
                            httpHeader);
                }
                request.setMaximumWaitTime(60);

                if (verbose || count>1) {
                    System.out.println("Calling webservice" + (session.getBoolProperty("WSFillSid") ? (" with SID " + request.getSid()) : "") + " on " + session.getStringProperty("WebServiceUrl"));
                }

                WSWriterReader wswr = new WSWriterReader(new URL(session.getStringProperty("WebServiceUrl")), request);
                wswr.callWebService();
                WSResponse response = wswr.getResponse();

                printResponse(response.getResponseData(), request.getSid(), verbose, prettyPrint, json, count);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private void callMQ(SessionWrapper session) {
        int count = (cmdParser.getArgumentValue("-mq").isEmpty()) ? 1 : cmdParser.getIntArgumentValue("-mq");

        boolean verbose = cmdParser.checkArgument("-verbose");
        boolean prettyPrint = cmdParser.checkArgument("-prettyprint");
        boolean json = cmdParser.checkArgument("-json");

        for (int i = 1; i <= count; i++) {
            if (count > 1)
                System.out.println("Call " + i + "/" + count);

            MQConnection connection = null;
            MQQueue queue = null;

            try {
                // initialize mq connection
                connection = new MQConnection(session.getStringProperty("QueueManager"));
                // access queue
                int openOptions = MQConstants.MQOO_OUTPUT + MQConstants.MQOO_FAIL_IF_QUIESCING;
                queue = connection.accessQueue(session.getStringProperty("Queue"), openOptions);

                // create new request
                MQRequest request;
                MQMessageWrapper mqMessageWrapper = new MQMessageWrapper(session.getStringProperty("Data"), session.wrapMqmd());

                if (session.getBoolProperty("AddWSS")) {
                    request = new MQRequest(
                            mqMessageWrapper,
                            session.getBoolProperty("FillTimestamps"),
                            session.getBoolProperty("FillSid"),
                            session.getStringProperty("WSSUser"),
                            session.getStringProperty("WSSPassword"));
                } else {
                    request = new MQRequest(
                            mqMessageWrapper,
                            session.getBoolProperty("FillTimestamps"),
                            session.getBoolProperty("FillSid"));
                }

                // initialize mq writer and put message
                MQWriter mqWriter = new MQWriter(queue, request);
                byte[] messageId = mqWriter.put();

                if (verbose || count>1) {
                    System.out.println("Message with" + (session.getBoolProperty("FillSid") ? (" SID " + request.getSid()) : "") + " length " +
                        session.getStringProperty("Data").length() + " inserted to queue " + session.getStringProperty("Queue"));
                }

                String response = MQReader.replyCheckerByCorrelId(connection, session.getStringProperty("ReplyToQueue"), session.getIntProperty("WaitTime"), messageId);
                printResponse(response, request.getSid(), verbose, prettyPrint, json, count);
            } catch (MQException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                System.out.println("An MQ IO error occurred : " + e);
            } finally {
                try {
                    if (queue != null)
                        queue.close();
                    if (connection != null)
                        connection.close();
                } catch (MQException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

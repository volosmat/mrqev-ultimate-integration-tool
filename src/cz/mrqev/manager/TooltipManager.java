package cz.mrqev.manager;

import javafx.geometry.Point2D;
import javafx.scene.control.Control;
import javafx.scene.control.Tooltip;

public class TooltipManager {
    public final static String TOOLTIP_FILL_TIMESTAMPS =
            "Use " + PlaceholderManager.PLACEHOLDER_CURRENT_TIMESTAMP + " placeholder to fill ISO timestamp (MH/OriginalSource/Timestamp)\n" +
            "Use " + PlaceholderManager.PLACEHOLDER_TIMEOUT_TIMESTAMP + " placeholder to fill timeout timestamp (MH/Extension/InternalHeader/Timeout)\n" +
            "Use " + PlaceholderManager.PLACEHOLDER_CURRENT_XM_TIMESTAMP + " or " + PlaceholderManager.PLACEHOLDER_TIMEOUT_XM_TIMESTAMP +" to fill XM timestamps";
    public final static String TOOLTIP_FILL_SID = "Use " + PlaceholderManager.PLACEHOLDER_SID + " placeholder to generate SID (MH/SessionID)";
    public final static String TOOLTIP_QUEUE_NAME = "Inputing queue name filters list of all queues in combobox";

    public Tooltip createTooltip(String message) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(message);

        return tooltip;
    }

    public Tooltip createFillSidTooltip() {
        return createTooltip(TOOLTIP_FILL_SID);
    }

    public Tooltip createFillTimestampsTooltip() {
        return createTooltip(TOOLTIP_FILL_TIMESTAMPS);
    }

    public Tooltip createCipherSuiteTooltip() {
        return createTooltip("Cipher suite must be same as Cipher spec defined for SSL channel. \nDifferent IBM QM versions and TLS versions have different available Cipher specs. \nCheck if your Cipher spec is available for you QM before you choose it.");
    }

    public Tooltip createQueueNameTooltip() {
        return createTooltip(TOOLTIP_QUEUE_NAME);
    }

    public void createAndShowNodeTooltip(Control controlNode, String text) {
        Tooltip tooltip = createTooltip(text);
        controlNode.setTooltip(tooltip);
        tooltip.setAutoHide(true);

        Point2D point = controlNode.localToScene(0,0);

        // show tooltip only when we have scene (scene is null when we close tab with opened textField)
        if (controlNode.getScene() != null) {
            tooltip.show(controlNode,
                    point.getX() + controlNode.getScene().getX() + controlNode.getScene().getWindow().getX(),
                    point.getY() + controlNode.getScene().getY() + controlNode.getScene().getWindow().getY() - 30);
        }
    }
}

package cz.mrqev.manager;

import com.google.gson.internal.LinkedTreeMap;
import cz.mrqev.controller.tab.*;
import cz.mrqev.controller.window.MainController;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.model.json.JsonHolder;
import cz.mrqev.model.session.SessionWrapper;
import org.apache.commons.configuration2.ex.ConfigurationException;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.view.MQNode;
import cz.mrqev.view.MQTab;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


public class SessionManager {
    public SessionWrapper session = new SessionWrapper();
    private String path;

    public void setPath(String path) {
        this.path = path;
    }

    public boolean saveSession() {
        try {
            return saveSession(null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void saveSessionAs() {
        // store with initial filename
        if (path != null && !path.isEmpty()) {
            String[] pathTokens = path.split("\\\\");
            String fileName = pathTokens[pathTokens.length - 1];
            try {
                saveSession(fileName);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        saveSession();
    }

    private boolean saveSession(String initialFileName) throws IOException {
        File file;
        if (path == null || path.isEmpty() || initialFileName != null) {
            Stage stage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MrQev sessions", "*.mrqev"));
            fileChooser.setTitle("Save session file...");
            if (initialFileName != null)
                fileChooser.setInitialFileName(initialFileName);

            // load last session folder path
            String lastSessionFolder = Main.configManager.getProperty("interface.lastsessionfolder");
            if (lastSessionFolder != null) {
                File lastFolder = new File(lastSessionFolder);
                if (lastFolder.exists())
                    fileChooser.setInitialDirectory(lastFolder);
            }

            file = fileChooser.showSaveDialog(stage);
        } else
            file = new File(path);

        if (file != null) {
            path = file.getAbsolutePath();
            setSessionValuesFromUIControls();
            UtilsManager.storeToFile(path, session.toJson());
            // set new last session folder path
            Main.configManager.setProperty("interface.lastsessionfolder", file.getParent());

            MqTabController.getInstance().addLogMessage("Session successfully saved...", true);
            WsTabController.getInstance().addLogMessage("Session successfully saved...", true);
            BatchPutTabController.getInstance().addLogMessage("Session successfully saved...", true);

            return true;
        }

        return false;
    }

    public void openSession() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("MrQev sessions", "*.mrqev"));
        fileChooser.setTitle("Open session file...");

        // load last session folder path
        String lastSessionFolder = Main.configManager.getProperty("interface.lastsessionfolder");
        if (lastSessionFolder != null) {
            File lastFolder = new File(lastSessionFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            path = file.getAbsolutePath();

            createSessionWrapper();
            setUIControlValuesFromSession();
            String newName;

            // legacy support - some old mrqev session dont have session name
            if (session.getStringProperty("SessionName").isEmpty())
                newName = MainController.getInstance().getNextAvailableTabNameFor(file.getName().split("\\.")[0]);
            else
                newName = MainController.getInstance().getNextAvailableTabNameFor(session.getStringProperty("SessionName"));


            MQTab.changeTabName(newName);

            Main.configManager.setProperty("interface.lastsessionfolder", file.getParent());
        }
    }

    public SessionWrapper createSessionWrapper() {
        try {
            String fileContent = UtilsManager.loadFromFile(path);
            if (JsonHolder.isJson(fileContent)) {
                session = SessionWrapper.fromJson(fileContent);
            } else {
                session = SessionWrapper.fromXml(path);
            }
            return session;
        } catch (IOException | ConfigurationException e) {
            MQDialogBox.openException(e);
        }

        return null;
    }

    public void setProperty(String key, Object value) {
        session.setProperty(key, value);
    }

    private void setUIControlValuesFromSession() {
        List<MQNode> serializableNodes = MainController.getInstance().getAllSerializableNodesForTab(MainController.getInstance().getSelectedTabName());

        for (MQNode node: serializableNodes) {
            session.getProperty(node.getSerializableName()).ifPresent(value -> {
                try {
                    System.out.println("deserializujem: " + node.getSerializableName());
                    // sometimes when there is empty node, his values is serializes as null value
                    if (!String.valueOf(value).equals("null"))
                        node.setValue(value);
                } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            });
        }

        // Custom properties
        // HttpHeader
        session.getProperty("HttpHeader").ifPresent(value -> {
            MqmdTabController.getInstance().httpHeader.saveProperties((LinkedTreeMap<String, String>) value);
            MqmdTabController.getInstance().refreshHttpHeaderProperties();
        });

        if (session.getProperty("XsdContent").isPresent()) {
            ReplyTabController.getInstance().label_validationStatus.setText(ReplyTabController.VALIDATION_READY);
        } else {
            ReplyTabController.getInstance().label_validationStatus.setText(ReplyTabController.VALIDATION_NO_XSD);
        }
    }

    private void setSessionValuesFromUIControls() {
        List<MQNode> serializableNodes = MainController.getInstance().getAllSerializableNodesForTab(MainController.getInstance().getSelectedTabName());

        for (MQNode node: serializableNodes) {
            try {
                System.out.println("serializujem: " + node.getSerializableName());
                Optional<String> optionalValue = Optional.ofNullable(node.getStringValue());

                optionalValue.ifPresent(value -> setProperty(node.getSerializableName(), value));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }

        // Custom properties
        // HttpHeader
        setProperty("HttpHeader", MqmdTabController.getInstance().httpHeader.getProperties());
        // SessionName
        setProperty("SessionName", MainController.getInstance().getSelectedTabName());
    }
}

package cz.mrqev.manager;

import com.google.gson.Gson;
import cz.mrqev.controller.tab.MqmdTabController;
import cz.mrqev.main.Main;
import cz.mrqev.model.ws.wrapper.transport.HttpHeader;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import cz.mrqev.view.MQDialogBox;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;

public class WSSManager {
    private String username;
    private String password;

    public WSSManager(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Generates WSS username token
     * @return - string UsernameToken- and 33 random alphanumeric characters.
     */
    private String generateUsernameToken() {
        return "UsernameToken-" + RandomStringUtils.randomAlphanumeric(33).toUpperCase();
    }

    /**
     * Generates WSS nonce
     * @return - random string of 22 random alphanumeric characters.
     */
    private String generateNonce() {
        try {
            return new String(RandomStringUtils.randomAlphanumeric(22).getBytes(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            MQDialogBox.openException(e);
            return "";
        }
    }

    /**
     * Generates timestamp in ISO8601 format.
     * @return - timestamp in ISO8601 format
     */
    private String generateCreatedTimestamp () {
        return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(new Date());
    }

    /**
     * Generates WSS digest
     * @param nonce - WSS nonce, can be generated using generateNonce()
     * @param createdTimestamp - timestamp, can be generated using generateCreatedTimestamp()
     * @return - returns WSS digest encoded with Base64
     */
    private String generateDigestBase64Encoded(String nonce, String createdTimestamp) {
        byte[] digest = DigestUtils.sha1(nonce + createdTimestamp + this.password);
        return Base64.getEncoder().encodeToString(digest);
    }

    /**
     * Generates XML data with WSS header
     * @return - returns string of XML data with WSS header
     */
    public String generateWssXml() {
        String usernameToken = generateUsernameToken();
        String nonce = generateNonce();
        String nonceB64 = Base64.getEncoder().encodeToString(nonce.getBytes());
        String created = generateCreatedTimestamp();
        String digest = generateDigestBase64Encoded(nonce, created);

        return "<wsse:Security xmlns:wsse=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd\" xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\">" +
                "<wsse:UsernameToken wsu:Id=\"" + usernameToken + "\">" +
                "<wsse:Username>" + username + "</wsse:Username>" +
                "<wsse:Password Type=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest\">" + digest + "</wsse:Password>" +
                "<wsse:Nonce EncodingType=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary\">" + nonceB64 + "</wsse:Nonce>" +
                "<wsu:Created>" + created + "</wsu:Created>" +
                "</wsse:UsernameToken>" +
                "</wsse:Security>";
    }

    public HttpHeader addWssToHttpHeader(HttpHeader httpHeader) {
        String nonce = generateNonce();
        String nonceB64 = Base64.getEncoder().encodeToString(nonce.getBytes());
        String created = generateCreatedTimestamp();
        String digest = generateDigestBase64Encoded(nonce, created);

        httpHeader.saveProperty("X-Wss-Nonce", nonceB64);
        httpHeader.saveProperty("X-Wss-User", username);
        httpHeader.saveProperty("X-Wss-Password", digest);
        httpHeader.saveProperty("X-Wss-Created", created);

        return httpHeader;
    }
}

package cz.mrqev.manager;

import cz.mrqev.controller.tab.DataTabController;
import org.fxmisc.richtext.CodeArea;

import java.util.regex.Pattern;

public class TemplateManager {

    public void addSoapTemplate() {
        CodeArea data = DataTabController.getInstance().codearea_requestData;
        String oldData = getOldData();

        data.replaceText("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">\n" +
                "  <soapenv:Header>\n" +
                "  </soapenv:Header>\n" +
                "  <soapenv:Body>\n    request\n".replace("request", oldData) +
                "  </soapenv:Body>\n" +
                "</soapenv:Envelope>\n");

        DataTabController.getInstance().refreshDataStyling();
    }

    private String getOldData() {
        CodeArea data = DataTabController.getInstance().codearea_requestData;

        String oldData = "request";

        if (!data.getText().contains("<SRVC_REQ>") && !Pattern.compile("<soapenv:Envelope(.*?)>").matcher(data.getText()).find() && data.getText().length()>0)
            oldData = data.getText();

        return oldData;
    }
}

package cz.mrqev.manager;

import cz.mrqev.api.*;
import cz.mrqev.interfaces.BaseConfiguration;
import cz.mrqev.interfaces.Plugin;
import cz.mrqev.main.DefaultConfiguration;
import cz.mrqev.utils.jar.JarLoader;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static cz.mrqev.main.Main.pluginManager;

public class PluginManager {
    private List<Class> loadedClasses = new ArrayList<>();
    private BaseConfiguration pluginConfiguration;
    private Plugin plugin;

    public void loadPlugin(String pluginFilePath) throws ClassNotFoundException {
        JarLoader jarLoader = new JarLoader(pluginFilePath);
        loadedClasses = jarLoader.loadClassesToRuntime();

        // Create plugin configuration first for plugin initialization
        pluginConfiguration = createPluginConfiguration();
        pluginConfiguration.setViewApi(new ViewApiImpl());
        pluginConfiguration.setDatabaseApi(new DatabaseApiImpl());
        pluginConfiguration.setUtilsApi(new UtilsApiImpl());
        pluginConfiguration.setListenerApi(new ListenerApiImpl());
        pluginConfiguration.setPlaceholderApi(new PlaceholderApiImpl());

        plugin = createPlugin();
    }

    public BaseConfiguration getPluginConfiguration() {
        if (pluginConfiguration == null) {
            pluginConfiguration = createPluginConfiguration();
        }
        return pluginConfiguration;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    private BaseConfiguration createPluginConfiguration() {
        Class clazz = getClassBySuperClass(BaseConfiguration.class);

        if (clazz != null) {
            return (BaseConfiguration) getClassInstance(clazz);
        } else {
            return new DefaultConfiguration();
        }
    }

    private Plugin createPlugin() throws ClassNotFoundException {
        Class clazz = getClassByInterface(Plugin.class);

        if (clazz != null) {
            return (Plugin) getClassInstance(clazz);
        }

        throw new ClassNotFoundException("Cannot found plugin class.");
    }

    public List<Class> getLoadedClasses() {
        return loadedClasses;
    }

    public List<Class> getClassesByType(Class type) {
        return loadedClasses.stream().filter(clazz -> clazz == type).collect(Collectors.toList());
    }

    public Class getClassByInterface(Class interfaceType) {
        List<Class> classes = getClassesByInterface(interfaceType);

        return classes.isEmpty() ? null : classes.get(0);
    }

    public List<Class> getClassesByInterface(Class interfaceType) {
        return loadedClasses.stream().filter(clazz -> !clazz.isInterface() && interfaceType.isAssignableFrom(clazz)).collect(Collectors.toList());
    }

    public Class getClassBySuperClass(Class superClass) {
        List<Class> classes = getClassesBySuperClass(superClass);

        return classes.isEmpty() ? null : classes.get(0);
    }

    public List<Class> getClassesBySuperClass(Class superClass) {
        return loadedClasses.stream().filter(clazz -> !Modifier.isAbstract(clazz.getModifiers()) && superClass.isAssignableFrom(clazz)).collect(Collectors.toList());
    }

    public Object getClassInstance(Class clazz) {
        try {
            return clazz.getConstructor().newInstance();
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Object getClassInstanceByInterface(Class interfaceType) {
        try {
            return getClassByInterface(interfaceType).getConstructor().newInstance();
        } catch (Exception e) {
            // if class dont exists just return null
            return null;
        }
    }

    public boolean checkClassInterface(Class clazz) {
        return getClassByInterface(clazz) != null;
    }

    public boolean checkClass(Class clazz) {
        return getClassesByType(clazz) != null;
    }
}

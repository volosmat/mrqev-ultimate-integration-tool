package cz.mrqev.manager;

import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.transport.ssl.Keystore;
import org.apache.commons.configuration2.HierarchicalConfiguration;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.tree.ImmutableNode;
import cz.mrqev.view.MQDialogBox;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by je01514 on 15.5.2017.
 */
public class ConfigManager {
    HashMap<String, String> configuration = new HashMap<>();

    public ArrayList<MQConnectionEntry> MQConnections = new ArrayList<>();
    public HashMap<String, Keystore> keystores = new LinkedHashMap<>();

    Configurations configs = new Configurations();

    public ConfigManager() {
        createConfiguration();
        loadConfiguration();
    }

    public void createConfiguration() {
        File f = new File(".\\config.xml");
        if (!f.exists())
        {
            try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(".\\config.xml"), "utf-8")))
            {
                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n" +
                        "<configuration xmlns=\"http://www.csob.cz/Thub/MrQev/Config/v1\">\n" +
                        "<interface>\n" +
                        "<lastfolder></lastfolder>\n" +
                        "<lastsessionfolder></lastsessionfolder>\n" +
                        "<consumers></consumers>\n" +
                        "</interface>\n" +
                        "<connections/>\n" +
                        "</configuration>\n");
            } catch (IOException e) {
                MQDialogBox.openException(e);
            }
        }
    }

    public void loadConfiguration() {
        try {
            XMLConfiguration config = configs.xml(".\\config.xml");

            configuration.put("interface.lastfolder", config.getString("interface.lastfolder"));
            configuration.put("interface.lastsessionfolder", config.getString("interface.lastsessionfolder"));
            configuration.put("interface.lastxsdfolder", config.getString("interface.lastxsdfolder"));
            configuration.put("interface.lastjarfolder", config.getString("interface.lastjarfolder"));
            configuration.put("interface.lastkeystorefolder", config.getString("interface.lastkeystorefolder"));
            configuration.put("interface.consumers", config.getString("interface.consumers"));

            List<String> keys = new ArrayList<>();
            config.getKeys().forEachRemaining(keys::add);

            for (String key: keys) {
                if (key.startsWith("settings."))
                configuration.put(key, config.getString(key));
            }

            List<HierarchicalConfiguration<ImmutableNode>> keystoreEntries = config.configurationsAt("keystores.keystore");

            if (keystoreEntries.isEmpty()) {
                // default thub keystore
                keystores.put("ThubIntAcc2.jks", new Keystore("ThubIntAcc2.jks", "ThubIntAcc2"));
            }

            for (HierarchicalConfiguration entry: keystoreEntries) {
                Keystore keystore = new Keystore(
                        entry.getString("keystoreFile"),
                        entry.getString("keystorePassword")
                );

                keystores.put(keystore.getKeystoreFile(), keystore);
            }

            List<HierarchicalConfiguration<ImmutableNode>> configentries = config.configurationsAt("connections.connection");

            for (HierarchicalConfiguration entry: configentries) {
                MQConnectionEntry newentry = new MQConnectionEntry(
                        entry.getString("QueueManager"),
                        entry.getString("Hostname"),
                        entry.getString("Port"),
                        entry.getString("Channel"),
                        entry.getString("UserID"),
                        entry.getString("UserPassword"),
                        entry.getString("CipherSpec"),
                        entry.getString("Keystore")
                );

                MQConnections.add(newentry);
            }
        } catch (ConfigurationException e) {
            MQDialogBox.openException(e);
        }
    }

    public void saveConfiguration() {
        try {
            FileBasedConfigurationBuilder<XMLConfiguration> builder = configs.xmlBuilder(".\\config.xml");
            XMLConfiguration config = builder.getConfiguration();

            config.setProperty("interface.lastfolder", configuration.get("interface.lastfolder"));
            config.setProperty("interface.lastsessionfolder", configuration.get("interface.lastsessionfolder"));
            config.setProperty("interface.lastxsdfolder", configuration.get("interface.lastxsdfolder"));
            config.setProperty("interface.lastjarfolder", configuration.get("interface.lastjarfolder"));
            config.setProperty("interface.lastkeystorefolder", configuration.get("interface.lastkeystorefolder"));
            config.setProperty("interface.consumers", configuration.get("interface.consumers"));

            for (String key: configuration.keySet().stream().filter(a -> a.startsWith("settings.")).collect(Collectors.toSet())) {
                config.setProperty(key, configuration.get(key));
            }

            config.clearTree("keystores");
            int i = 0;
            for (Map.Entry<String, Keystore> entry: keystores.entrySet()) {
                config.addProperty("keystores.keystore("+i+").keystoreFile", entry.getValue().getKeystoreFile());
                config.addProperty("keystores.keystore("+i+").keystorePassword", entry.getValue().getKeystorePassword());
                i++;
            }

            config.clearTree("connections");
            for (i = 0; i < MQConnections.size(); i++) {
                MQConnectionEntry entry = MQConnections.get(i);
                config.addProperty("connections.connection("+i+").QueueManager", entry.getQueueManager());
                config.addProperty("connections.connection("+i+").Hostname", entry.getHostname());
                config.addProperty("connections.connection("+i+").Port", entry.getPort());
                config.addProperty("connections.connection("+i+").UserID", entry.getUserId());
                config.addProperty("connections.connection("+i+").UserPassword", entry.getUserPassword());
                config.addProperty("connections.connection("+i+").Channel", entry.getChannel());
                config.addProperty("connections.connection("+i+").CipherSpec", entry.getCipherSuite());
                config.addProperty("connections.connection("+i+").Keystore", entry.getKeyStore());
            }

            builder.save();
        } catch (ConfigurationException e) {
            MQDialogBox.openException(e);
        }
    }

    public void checkForEmptyProperties()
    {
        if (!configuration.containsKey("settings.alwaysConfirmClosingOfMrQev"))
            setBoolProperty("settings.alwaysConfirmClosingOfMrQev", true);
        if (!configuration.containsKey("settings.alwaysConfirmClosingOfTab"))
            setBoolProperty("settings.alwaysConfirmClosingOfTab", true);
    }

    public void setProperty(String key, String value) {
        configuration.put(key, value);
        saveConfiguration();
    }

    public void setIntProperty(String key, int value) {
        configuration.put(key, String.valueOf(value));
        saveConfiguration();
    }

    public String getProperty(String key) {
        return configuration.get(key);
    }

    public int getIntProperty(String key) {
        return Integer.parseInt(configuration.get(key));
    }

    public void setBoolProperty(String key, boolean value) {
        configuration.put(key, String.valueOf(value));
        saveConfiguration();
    }

    public boolean getBoolProperty(String key) {
        return Boolean.parseBoolean(configuration.get(key));
    }
}

package cz.mrqev.manager;

import cz.mrqev.controller.tab.*;
import cz.mrqev.controller.window.MainController;
import cz.mrqev.controller.window.SearchController;
import cz.mrqev.model.Placeholder;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;
import cz.mrqev.main.Main;
import cz.mrqev.model.cache.MqCache;
import cz.mrqev.model.ws.wrapper.transport.HttpHeader;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.view.MQTab;
import cz.mrqev.view.MQWindow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ListenerManager {
    private DataTabController dataTabController;
    private MqTabController mqTabController;
    private MqmdTabController mqmdTabController;
    private WsTabController wsTabController;
    private RequestTabController requestTabController;

    private Placeholder uidPlaceholder;
    private Placeholder timestampPlaceholder;

    public List<ChangeListener<String>> dataCodeAreaListeners = new ArrayList<>();

    // stupid hack to not create new request tab during program initialization
    private int createTabListenerCalledCount = 0;

    private void initialize() {
        dataTabController = DataTabController.getInstance();
        mqTabController = MqTabController.getInstance();
        mqmdTabController = MqmdTabController.getInstance();
        wsTabController = WsTabController.getInstance();
        requestTabController = RequestTabController.getInstance();
    }

    private void initializePlaceholders() {
        uidPlaceholder = Main.placeholderManager.getUidPlaceholder();
        timestampPlaceholder = Main.placeholderManager.getTimestampPlaceholder();
    }

    public void createDataCodeAreaListeners() {
        dataCodeAreaListeners.forEach(item -> DataTabController.getInstance().codearea_requestData.textProperty().addListener(item));
    }

    public void createMqGenerateUidListener() {
        initialize();

        // listener to automatically add placeholders for SID generation in message
        mqTabController.checkbox_MQ_generateUID.selectedProperty().addListener((observable, oldValue, newValue) -> {
            initializePlaceholders();
            // if we do not want to generate sid anymore, fill static sid to REQ
            String data = dataTabController.codearea_requestData.getText();
            if (!newValue) {
                dataTabController.codearea_requestData.replaceText(uidPlaceholder.replace(data));
            } else {
                dataTabController.codearea_requestData.replaceText(uidPlaceholder.fill(data, mqTabController::addLogMessage));
            }
            DataTabController.getInstance().refreshDataStyling();
        });
    }

    public void createMqFillTimestampsListener() {
        initialize();

        // listener to automatically add placeholders for Timestamp and Timeout generation in message
        mqTabController.checkbox_MQ_fillTimestamps.selectedProperty().addListener((observable, oldValue, newValue) -> {
            initializePlaceholders();
            // if we do not want to generate sid anymore, fill static sid to REQ
            String data = dataTabController.codearea_requestData.getText();
            if (!newValue) {
                dataTabController.codearea_requestData.replaceText(timestampPlaceholder.replace(data));
            } else {
                dataTabController.codearea_requestData.replaceText(timestampPlaceholder.fill(data, mqTabController::addLogMessage));
            }
            DataTabController.getInstance().refreshDataStyling();
        });
    }

    public void createWsGenerateUidListener() {
        initialize();

        // listener to automatically add placeholders for SID generation in message
        wsTabController.checkbox_WS_generateUID.selectedProperty().addListener((observable, oldValue, newValue) -> {
            initializePlaceholders();
            // if we do not want to generate sid anymore, fill static uid to REQ
            String data;
            // in REST JSON we have metaheader in http header
            if (wsTabController.radiobutton_httpRest.isSelected()) {
                data = mqmdTabController.httpHeader.getProperty("metaheader");

                if (!newValue) {
                    mqmdTabController.httpHeader.saveProperty("metaheader", uidPlaceholder.replace(data));
                } else {
                    mqmdTabController.httpHeader.saveProperty("metaheader", uidPlaceholder.fill(data, wsTabController::addLogMessage));
                }

                mqmdTabController.refreshHttpHeaderProperties();
            } else {
                data = dataTabController.codearea_requestData.getText();

                if (!newValue) {
                    dataTabController.codearea_requestData.replaceText(uidPlaceholder.replace(data));
                } else {
                    dataTabController.codearea_requestData.replaceText(uidPlaceholder.fill(data, wsTabController::addLogMessage));
                }

                DataTabController.getInstance().refreshDataStyling();
            }
        });
    }

    public void createWsFillTimestampsListener() {
       initialize();

        // listener to automatically add placeholders for Timestamp and Timeout generation in message
        wsTabController.checkbox_WS_fillTimestamps.selectedProperty().addListener((observable, oldValue, newValue) -> {
            initializePlaceholders();
            // if we do not want to generate timestamp anymore, fill static actual timestamp to REQ
            String data;

            // in REST JSON we have metaheader in http header
            if (wsTabController.radiobutton_httpRest.isSelected()) {
                data = mqmdTabController.httpHeader.getProperty("metaheader");

                if (!newValue) {
                    mqmdTabController.httpHeader.saveProperty("metaheader", timestampPlaceholder.replace(data));
                } else {
                    mqmdTabController.httpHeader.saveProperty("metaheader", timestampPlaceholder.fill(data, wsTabController::addLogMessage));
                }

                mqmdTabController.refreshHttpHeaderProperties();
            } else {
                data = dataTabController.codearea_requestData.getText();

                if (!newValue) {
                    dataTabController.codearea_requestData.replaceText(timestampPlaceholder.replace(data));
                } else {
                    dataTabController.codearea_requestData.replaceText(timestampPlaceholder.fill(data, wsTabController::addLogMessage));
                }
                DataTabController.getInstance().refreshDataStyling();
            }


        });
    }

    public void createSearchInComboboxListener() {
        initialize();

        // queue box show when you type smt to input field
        addKeyTypedListenerToComboBox(mqTabController.combobox_queues, mqTabController.queueList, null);
        // wss user search
        addKeyTypedListenerToComboBox(mqmdTabController.combobox_wssUser, MainController.getInstance().getWssNames(false), mqmdTabController.textfield_wssPassword);
        // http header property name search
        addKeyTypedListenerToComboBox(mqmdTabController.combobox_requestPropertyName, HttpHeader.getDefaultPropertyList().keySet(), mqmdTabController.textarea_requestPropertyValue);
    }

    public void addKeyTypedListenerToComboBox(ComboBox comboBox, Collection<String> searchList, TextInputControl relatedInputField) {
        comboBox.getEditor().setOnKeyTyped(keyEvent -> {
            String text = getTextInComboBoxAfterKeyTyped(comboBox, keyEvent.getCharacter());


            // there were problem when user select item from popup with enter
            if ((int)keyEvent.getCharacter().charAt(0) == 13 || keyEvent.isControlDown() || keyEvent.isShiftDown() || keyEvent.isAltDown()) {
                keyEvent.consume();
                return;
            }

            int oldSize = comboBox.getItems().size();
            if (text.length() > 0) {
                comboBox.getItems().setAll(searchList.stream().filter(q-> q.toLowerCase().contains(text.toLowerCase())).toArray());
            } else {
                comboBox.getItems().setAll(searchList);
                comboBox.getEditor().setText("");
                if (relatedInputField != null) {
                    relatedInputField.setText("");
                }
            }

            int newItemsSize = comboBox.getItems().size();
            // refresh popup size by hide and show
            if (text.length() > 0 && newItemsSize > 0 && oldSize != newItemsSize) {
                comboBox.hide();
                comboBox.show();
            }
            // hide popup when we have no found items
            if (comboBox.getItems().size() == 0) {
                comboBox.hide();
            }
        });
    }

    private String getTextInComboBoxAfterKeyTyped(ComboBox comboBox, String keyTyped) {
        if (comboBox == null || comboBox.getEditor() == null) {
            return "";
        }

        String text = comboBox.getEditor().getText();

        // check java version because in Java9 was changed time when keyType listener was fired
        if (Integer.parseInt(System.getProperty("java.version").split("\\.")[0]) < 9 && !keyTyped.equals("\b") && !keyTyped.equals("\n")) {
            text += keyTyped;
        }

        return text;
    }

    public void createNewRequestTabListener() {
        MainController mainController = MainController.getInstance();

        // tabPane with "+" has only one tab so i have to clear selection to trigger selection changed
        mainController.addTabPane.getSelectionModel().clearSelection();

        // add listener on first tab
        mainController.addTabPane.getSelectionModel().selectedItemProperty()
                .addListener((oldValue, oldSelectedTab, newSelectedTab) -> {
                    // tabPane with "+" tab
                    TabPane tabPane = mainController.addTabPane;
                    SelectionModel selectionModel = tabPane.getSelectionModel();
                    // createTabListenerCalledCount is for skip first two calls of listener during program initialization (small hack :)
                    if (selectionModel.isSelected(0) && createTabListenerCalledCount > 1) {
                        // new tab will be added default to place directly after default tab
                        mainController.addRequestTab();
                    }

                    createTabListenerCalledCount++;
                    selectionModel.clearSelection();
                });
    }

    public void createClosingDialogListener() {
        Platform.runLater(() -> MainController.getInstance().mainGrid.getScene().getWindow().setOnCloseRequest(windowevent -> {
            if (Main.configManager.getBoolProperty("settings.alwaysConfirmClosingOfMrQev")) {
                int result = MQDialogBox.openConfirmation("MrQev", "Are you sure you want to close MrQev?", "");
                if (result == -1 || result == 0)
                    if (windowevent != null) windowevent.consume();
            }
        }));

    }

    /**
     * Menu shortcuts are created from main.fxml template directly
     */
    public void createShortcuts() {
        MainController mainController = MainController.getInstance();
        RequestTabController requestTabController = RequestTabController.getInstance();

        mainController.mainGrid.setOnKeyPressed(keyEvent -> {
            // create tab (CTRL+T)
            if (keyEvent.getCode() == KeyCode.T && keyEvent.isControlDown()) {
                // select "+" tab to invoke createNewRequestTabListener
                mainController.addTabPane.getSelectionModel().selectFirst();
            }

            // remove tab (CTRL+W)
            if (keyEvent.getCode() == KeyCode.W && keyEvent.isControlDown()) {
                // we can remove only tabs that were created dynamically
                if (mainController.requestTabPane.getSelectionModel().getSelectedIndex() > 0) {
                    Tab selectedTab = mainController.requestTabPane.getTabs().get(mainController.requestTabPane.getSelectionModel().getSelectedIndex());

                    if (MQTab.checkIfSaved(null, selectedTab.getText())) {
                        MQTab.checkIfTabIsLastOpened();
                        mainController.requestTabPane.getTabs().remove(selectedTab);
                    }
                }
            }

            // data search (CTRL+F) when DataTab or ReplyTab is selected
            if (keyEvent.getCode() == KeyCode.F && keyEvent.isControlDown() &&
                    (RequestTabController.getInstance().tabs.getSelectionModel().isSelected(RequestTabController.DATA_TAB_POSITION) ||
                            RequestTabController.getInstance().tabs.getSelectionModel().isSelected(RequestTabController.REPLY_TAB_POSITION))) {
                new MQWindow("window/search", "Find/Replace", 230, 340, false, null, false, false, true);
            }

            if (keyEvent.getCode() == KeyCode.ENTER && requestTabController != null && requestTabController.tabs.getSelectionModel().isSelected(RequestTabController.MQMD_TAB_POSITION)) {
                MqmdTabController.getInstance().saveRequestProperty();
                MqmdTabController.getInstance().listView_requestProperties.requestFocus();
            }

            // import batch file (CTRL+I) when BatchPut tab is selected
            if (keyEvent.getCode() == KeyCode.I && keyEvent.isControlDown() && requestTabController.tabs.getSelectionModel().isSelected(RequestTabController.BATCH_PUT_TAB_POSITION)) {
                BatchPutTabController.getInstance().selectBatchFile(new ActionEvent());
            }
        });
    }

    public void createProcessTabNameChangeListener(Tab tab, Label nameLabel, TextField renameField) {
        // change tab name after ENTER pres
        renameField.setOnKeyPressed((keyEvent) -> {
            if (keyEvent.getCode() == KeyCode.ENTER) {
                MQTab.changeTabName(tab, nameLabel, renameField, false);
            }
        });

        // change tab name after unfocus textField
        renameField.focusedProperty().addListener((value, oldPropertyValue, newPropertyValue) -> {
            if (!newPropertyValue) {
                MQTab.changeTabName(tab, nameLabel, renameField, true);
            }
        });
    }

    public void createInitializeTabNameChangeListener(Tab tab, Label nameLabel, TextField renameField) {
        // listener to show textField to edit tab name
        nameLabel.setOnMouseClicked((mouseEvent) -> {
            if (mouseEvent.getClickCount() == 2) {
                tab.setGraphic(renameField);
                renameField.requestFocus();
                Font font = Font.font("Arial",11);
                renameField.setFont(font);
                renameField.setMinWidth(nameLabel.getLayoutBounds().getWidth()*1.1);
                renameField.setMaxWidth(nameLabel.getLayoutBounds().getWidth()*1.3);
            }
    });
    }

    public void createAddPlaceholdersInDataAfterCopyListener() {
        initialize();
        // listener to add placeholders when text is copied to Data tab
        dataTabController.codearea_requestData.textProperty().addListener((observable, oldValue, newValue) -> {
            initializePlaceholders();
            String data = dataTabController.codearea_requestData.getText();
            // check if its really necessary to call replaceText on codearea
            String replacedSidData = uidPlaceholder.fill(data);
            String replacedTimestampData = timestampPlaceholder.fill(data);

            Platform.runLater(() -> {
                if (mqTabController.checkbox_MQ_generateUID.isSelected() && !data.equals(replacedSidData))
                    dataTabController.codearea_requestData.replaceText(uidPlaceholder.fill(data));

                if (mqTabController.checkbox_MQ_fillTimestamps.isSelected() && !data.equals(replacedTimestampData))
                    dataTabController.codearea_requestData.replaceText(timestampPlaceholder.fill(data));

                if (wsTabController.checkbox_WS_generateUID.isSelected() && wsTabController.radiobutton_httpRest.isSelected() && !data.equals(replacedSidData))
                    dataTabController.codearea_requestData.replaceText(uidPlaceholder.fill(data));

                if (wsTabController.checkbox_WS_fillTimestamps.isSelected() && !data.equals(replacedTimestampData))
                    dataTabController.codearea_requestData.replaceText(timestampPlaceholder.fill(data));

                dataTabController.refreshDataStyling();
            });
        });
    }

    public void createSearchButtonsListener() {
        SearchController searchController = SearchController.getInstance();
        searchController.textfield_find.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                searchController.button_find.setDisable(true);
                searchController.button_replaceAll.setDisable(true);
            } else {
                searchController.button_find.setDisable(false);
                searchController.button_replaceAll.setDisable(false);
            }

            searchController.button_replace.setDisable(true);
            searchController.button_replaceFind.setDisable(true);
        });

        // ReplaceAll and Replace is possible only in data tab
        DataTabController.getInstance().codearea_requestData.selectedTextProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.isEmpty() || searchController.textfield_find.getText().isEmpty()) {
                searchController.button_replace.setDisable(true);
                searchController.button_replaceFind.setDisable(true);
            } else {
                searchController.button_replace.setDisable(false);
                searchController.button_replaceFind.setDisable(false);
            }
        });
    }

    public void createFindOnEnterPressListener() {
        SearchController.getInstance().anchorPane.setOnKeyPressed(keyEvent -> {
            // close window (ESC)
            if (keyEvent.getCode() == KeyCode.ESCAPE && SearchController.getInstance() != null) {
                SearchController.getInstance().close();
            }

            if (keyEvent.getCode() == KeyCode.ENTER) {
                SearchController.getInstance().find(new ActionEvent());
            }
        });
    }

    public void createQueueManagerChangedListener() {
        initialize();
        mqTabController.combobox_queueManagers.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null || newValue.toString().isEmpty())
                return;

            String selectedQueueManager = mqTabController.getSelectedMqConnection().getQueueManager();

            ArrayList cachedQueues = (ArrayList) MqCache.getInstance().getCachedRecord("loadedQueues" + selectedQueueManager);
            String lastUpdated = (String) MqCache.getInstance().getCachedRecord("lastUpdatedQueues" + selectedQueueManager);

            // set last updated label
            if (lastUpdated != null) {
                mqTabController.label_lastUpdatedQueuesDate.setText(lastUpdated);
            } else if (cachedQueues != null && !cachedQueues.isEmpty()) {
                mqTabController.label_lastUpdatedQueuesDate.setText("No last updated time");
            } else {
                mqTabController.label_lastUpdatedQueuesDate.setText("No cached queues");
            }

            if (cachedQueues != null) {
                // for queue filter
                mqTabController.queueList.clear();
                mqTabController.queueList.addAll(cachedQueues);
                mqTabController.combobox_queues.getItems().clear();
                mqTabController.combobox_queues.getItems().addAll(cachedQueues);
            } else {
                mqTabController.queueList.clear();
                mqTabController.combobox_queues.getItems().clear();
            }
        });
    }

    public void createGenerateUidWhenGetLogSelectedListener() {
        initialize();

        // MQ Tab - select generate sid checkbox when get log is selected
        mqTabController.radiobutton_getLog.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue && newValue) {
                mqTabController.checkbox_MQ_generateUID.setSelected(true);
            }
        });

        // MQ Tab - deselect get log radiobutton when generateSid checkbox was deselected
        mqTabController.checkbox_MQ_generateUID.selectedProperty().addListener((observable, oldValue, newValue) -> {
            // when we deselect SID generation we cannot get log for this specific message
            if (oldValue && !newValue && mqTabController.radiobutton_getLog.isSelected()) {
                mqTabController.radiobutton_getLog.setSelected(false);
                mqTabController.radioButton_none.setSelected(true);
            }
        });

        // WS Tab - select generate sid checkbox when get log is selected
        wsTabController.radiobutton_getLog.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue && newValue) {
                wsTabController.checkbox_WS_generateUID.setSelected(true);
            }
        });

        // WS Tab - deselect get log radiobutton when generateSid checkbox was deselected
        wsTabController.checkbox_WS_generateUID.selectedProperty().addListener((observable, oldValue, newValue) -> {
            // when we deselect SID generation we cannot get log for this specific message
            if (oldValue && !newValue && wsTabController.radiobutton_getLog.isSelected()) {
                wsTabController.radiobutton_getLog.setSelected(false);
                wsTabController.radiobutton_getReply.setSelected(true);
            }
        });
    }

    public void createAddMetaheaderToHttpHeaderForRestListener() {
        initialize();

        wsTabController.radiobutton_httpRest.selectedProperty().addListener((observable, oldValue, newValue) -> {
           if (!oldValue && newValue && !mqmdTabController.httpHeader.containsKey("metaheader")) {
               mqmdTabController.httpHeader.saveProperty("metaheader", HttpHeader.getDefaultPropertyList().get("metaheader"));
               mqmdTabController.refreshHttpHeaderProperties();
           }
        });
    }
}

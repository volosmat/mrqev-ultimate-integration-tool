package cz.mrqev.manager;

import cz.mrqev.model.Placeholder;
import cz.mrqev.model.json.JsonHolder;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

public class PlaceholderManager {
    private Map<String, Placeholder> placeholders = new HashMap<>();
    private Placeholder uidPlaceholder;
    private Placeholder timestampPlaceholder;

    public PlaceholderManager() {

    }

    public Placeholder getUidPlaceholder() {
        return uidPlaceholder;
    }

    public void setUidPlaceholder(Placeholder uidPlaceholder) {
        this.uidPlaceholder = uidPlaceholder;
    }

    public Placeholder getTimestampPlaceholder() {
        return timestampPlaceholder;
    }

    public void setTimestampPlaceholder(Placeholder timestampPlaceholder) {
        this.timestampPlaceholder = timestampPlaceholder;
    }

    public void addCustomPlaceholder(Placeholder placeholder) {
        placeholders.put(placeholder.getName(), placeholder);
    }

    public void removeCustomPlaceholder(String key) {
        placeholders.remove(key);
    }

    public Collection<Placeholder> getCustomPlaceholders() {
        return placeholders.values();
    }


    public final static String PLACEHOLDER_SID = "##SID##";
    public final static String PLACEHOLDER_CURRENT_TIMESTAMP = "##CURRENT_TIMESTAMP##";
    public final static String PLACEHOLDER_TIMEOUT_TIMESTAMP = "##TIMEOUT_TIMESTAMP##";
    public final static String PLACEHOLDER_CURRENT_XM_TIMESTAMP = "##CURRENT_XM_TIMESTAMP##";
    public final static String PLACEHOLDER_TIMEOUT_XM_TIMESTAMP = "##TIMEOUT_XM_TIMESTAMP##";
    /*


    public String addDataPlaceholderSid(String data) {
        return addDataPlaceholderSid(data, null);
    }

    public String addDataPlaceholderSid(String data, Function<String, Void> log) {
        String result = data;

        if (data != null && data.contains("http://ed6.kbc.com/metaheader.1")) {
            // fill placeholders for MetaHeader.v1
            result = result.replaceAll(createRegex("SessionID"), createReplacement("SessionID", PLACEHOLDER_SID));
            result = result.replaceAll(createRegex("BusinessUniqueID"), createReplacement("BusinessUniqueID", PLACEHOLDER_SID));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_SID + " was added to MetaHeader.v1 SessionID and BusinessUniqueID tags.");

        } else if (data != null && data.contains("http://www.csob.cz/Thub/Common/MetaHeader/v2")) {
            // fill placeholders for MetaHeader.v2
            result = result.replaceAll(createRegex("Sid"), createReplacement("Sid", PLACEHOLDER_SID));
            result = result.replaceAll(createRegex("CallChainId"), createReplacement("CallChainId", PLACEHOLDER_SID));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_SID + " was added to MetaHeader.v2 Sid and CallChainId tags.");

        } else if (data != null && !data.isEmpty() && JsonHolder.isJson(data)) {
            // fill placeholders for JSON (e.g. rest data or metaheader in http header)
            result = result.replaceAll(" ", "");
            result = result.replaceAll(createJsonRegex("sid"), createJsonReplacement("sid", PLACEHOLDER_SID));
            result = result.replaceAll(createJsonRegex("buid"), createJsonReplacement("buid", PLACEHOLDER_SID));
            result = UtilsManager.prettyPrintJson(result);
            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_SID + " was added to JSON metaheader element in http header.");
        }  else if (data != null && data.contains("http://distribution.csob.cz/dirheader/v2")) {
            // fill placeholders for DirHeader.v2
            result = result.replaceAll(createRegex("UserSessionId"), createReplacement("UserSessionId", PLACEHOLDER_SID));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_SID + " was added to DirHeader.v2 UserSessionId tag.");

        } else if (data != null && data.contains("<MH>")) {
            // fill placeholders for XM
            result = result.replaceAll(createRegex("SID"), createReplacement("SID", PLACEHOLDER_SID));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_SID + " was added to XM MH tag.");

        } else {
            if (log != null)
                log.apply(PLACEHOLDER_SID + " could not be added. Add it manually, or paste request to data and it will be added automatically.");
        }

        return result;
    }

    public String addDataPlaceholderTimestamp(String data) {
        return addDataPlaceholderTimestamp(data, null);
    }

    public String addDataPlaceholderTimestamp(String data, Function<String, Void> log) {
        String result = data;

        if (data != null && (data.contains("http://ed6.kbc.com/metaheader.1") || data.contains("http://www.csob.cz/Thub/Common/MetaHeader/v2"))) {
            // fill placeholders for MetaHeader.v1 and MetaHeader.v2
            result = result.replaceAll(createRegex("TimeStamp"), createReplacement("TimeStamp", PLACEHOLDER_CURRENT_TIMESTAMP));
            result = result.replaceAll(createRegex("Timeout"), createReplacement("Timeout", PLACEHOLDER_TIMEOUT_TIMESTAMP));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_CURRENT_TIMESTAMP + " and " + PLACEHOLDER_TIMEOUT_TIMESTAMP + " was added to MetaHeader Timestamp tag.");
        } else if (data != null && !data.isEmpty() && JsonHolder.isJson(data)) {
            result = result.replaceAll(" ", "");
            // fill placeholders for JSON (e.g. rest data or metaheader in http header)
            result = result.replaceAll(createJsonRegex("timestamp"), createJsonReplacement("timestamp", PLACEHOLDER_CURRENT_TIMESTAMP));
            result = UtilsManager.prettyPrintJson(result);

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_CURRENT_TIMESTAMP + " was added to JSON metaheader element in http header.");
        } else if (data != null && data.contains("<MH>")) {
            // fill placeholders for XM
            result = result.replaceAll(createRegex("OT"), createReplacement("OT", PLACEHOLDER_CURRENT_XM_TIMESTAMP));
            result = result.replaceAll(createRegex("TC"), createReplacement("TC", PLACEHOLDER_CURRENT_XM_TIMESTAMP));
            result = result.replaceAll(createRegex("DF"), createReplacement("DF", PLACEHOLDER_CURRENT_XM_TIMESTAMP));
            result = result.replaceAll(createRegex("DT"), createReplacement("DT", PLACEHOLDER_TIMEOUT_XM_TIMESTAMP));

            if (log != null)
                log.apply("Placeholder " + PLACEHOLDER_CURRENT_XM_TIMESTAMP + " and " + PLACEHOLDER_TIMEOUT_XM_TIMESTAMP + " were added to XM MH tags.");

        } else {
            if (log != null)
                log.apply("Timestamp placeholders could not be added. Add them manually, or paste request to data and they will be added automatically.");
        }

        return result;
    }

    public String removeDataPlaceholderSid(String data, String sid) {
        return data.replace(PLACEHOLDER_SID, sid);
    }

    public String removeDataPlaceholderSid(String data) {
        return removeDataPlaceholderSid(data, UtilsManager.generateSID());
    }

    public String removeDataPlaceholderTimestamp(String data) {
        String result = data;

        String currentTimestamp = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX").format(new Date());
        String timeoutTimestamp = new SimpleDateFormat("yyyyMMddHHmmssSSSSSS").format(new Date(new Date().getTime() + (1000 * 60 * 60 * 24)));
        String currentXMTimestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String timeoutXMTimestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date(new Date().getTime() + (1000 * 60 * 60 * 24)));
        result = result.replace(PLACEHOLDER_CURRENT_TIMESTAMP, currentTimestamp);
        result = result.replace(PLACEHOLDER_TIMEOUT_TIMESTAMP, timeoutTimestamp);
        result = result.replace(PLACEHOLDER_CURRENT_XM_TIMESTAMP, currentXMTimestamp);
        result = result.replace(PLACEHOLDER_TIMEOUT_XM_TIMESTAMP, timeoutXMTimestamp);

        return result;
    }*/

    /**
     * Create regex string to match XML element with given name
     * @param element
     * @return
     */
    private String createRegex(String element) {
        return "<([a-zA-Z0-9:]*)" + element + "(.*?)>(.*?)</([a-zA-Z0-9:]*)" + element + ">";
    }

    /**
     * Create replace string with regex variables and given placeholder
     * @param element
     * @param placeholder
     * @return
     */
    private String createReplacement(String element, String placeholder) {
        return "<$1" + element + "$2>" + placeholder + "</$4" + element + ">";
    }

    private String createJsonRegex(String element) {
        return "\"" + element + "\":\"(.*?)\"";
    }

    private String createJsonReplacement(String element, String placeholder) {
        return "\"" + element + "\":\"" + placeholder + "\"";
    }
}

package cz.mrqev.manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import cz.mrqev.interfaces.Controller;
import cz.mrqev.interfaces.TabController;
import cz.mrqev.interfaces.annotation.Responsive;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.model.json.JsonHolder;
import cz.mrqev.view.MQNode;
import javafx.scene.layout.Region;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import cz.mrqev.view.MQDialogBox;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * Created by JE01514 on 12. 7. 2017.
 */
public class UtilsManager {
    public static String generateSID() {
        String sid = "MrQev";
        sid += new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        sid += RandomStringUtils.randomAlphabetic(10);
        return sid;
    }

    public static String prettyPrint(String input) {
        try {
            if (JsonHolder.isJson(input)) {
                return prettyPrintJson(input);
            } else {
                return prettyPrintXML(input);
            }
        } catch (Exception e) {
            // if anything happened, not modified text is returned
            return input;
        }
    }

    public static String prettyPrintJson(String input) {
        JsonParser parser = new JsonParser();
        JsonObject json = parser.parse(input).getAsJsonObject();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        return gson.toJson(json);
    }

    public static String prettyPrintXML(String input) {
        try {
            // Turn xml string into a document
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new ByteArrayInputStream(input.getBytes(StandardCharsets.UTF_8))));

            // Remove whitespaces outside tags
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                    document,
                    XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }

            //Source xmlInput = new StreamSource(new StringReader(input));
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(new StringWriter());
            transformer.transform(new DOMSource(document), result);
            return result.getWriter().toString();

        } catch (TransformerException | IOException | ParserConfigurationException | XPathExpressionException e) {
            MQDialogBox.openException(e);
        } catch (SAXException e) {
            System.out.println("Cannot parse xml with error: " + e.toString());
        }

        // if pretty print fails return same input
        return input;
    }

    public static String reverseString(String input) {
        return new StringBuilder(input).reverse().toString();
    }

    public static void storeToFile(String path, String content) throws IOException {
        FileUtils.writeStringToFile(new File(path), content, Charset.defaultCharset(), false);
    }

    public static void appendToFile(String path, String content) throws IOException {
        Files.write(Paths.get(path), content.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }

    public static String loadFromFile(String path) throws IOException {
        return FileUtils.readFileToString(new File(path), Charset.defaultCharset());
    }

    public static String getRfc1123Datetime() {
        return DateTimeFormatter.RFC_1123_DATE_TIME.format(ZonedDateTime.now(ZoneOffset.UTC));
    }

    public static Class getWrapperForPrimitive(Class primitive) {
        if (primitive == int.class) {
            return Integer.class;
        } else if (primitive == boolean.class) {
            return Boolean.class;
        } else if (primitive == double.class) {
            return Double.class;
        } else if (primitive == float.class) {
            return Float.class;
        }

        // if its not primitive class return itself
        return primitive;
    }

    /**
     * Get annotated nodes only from controller instance
     * @param controller - controller instance
     * @param annotation - annotation class
     * @return
     */
    public static List<MQNode> getAnnotatedNodes(Controller controller, Class annotation) {
        Objects.requireNonNull(annotation);
        Objects.requireNonNull(controller);

        List<MQNode> result = new ArrayList<>();
        for(Field field : controller.getClass().getFields()){
            try {
                Object fieldInstance = field.get(controller);

                if (fieldInstance instanceof javafx.scene.Node) {
                    MQNode node = new MQNode((Region) field.get(controller));
                    Responsive responsive = field.getAnnotation(Responsive.class);
                    Serializable serializable = field.getAnnotation(Serializable.class);
                    node.setResponsive(responsive);
                    node.setSerializable(serializable);

                    if (field.getAnnotation(annotation) != null) {
                        result.add(node);
                    }
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}

package cz.mrqev.controller.window;

import cz.mrqev.controller.tab.MqTabController;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import cz.mrqev.main.Main;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.transport.ssl.Keystore;

public class MQConnectionController extends BaseWindowController {
    public AnchorPane anchorPane;

    public TextField textfield_qmname;
    public TextField textfield_hostname;
    public TextField textfield_port;
    public TextField textfield_channel;
    public TextField textfield_userid;
    public TextField textfield_userPassword;
    public ComboBox combobox_cipherSuite;
    public ComboBox combobox_keystore;
    public Button button_save;
    public ListView listview_mqConnectionList;
    public Label label_helpCipherSuite;

    public static MQConnectionController getInstance() {
        return (MQConnectionController) instance;
    }

    public void initialize() {
        super.initialize();
        label_helpCipherSuite.setTooltip(tooltipManager.createCipherSuiteTooltip());
        combobox_cipherSuite.getItems().addAll(Keystore.getDefaultCipherSuites());
        listenerManager.addKeyTypedListenerToComboBox(combobox_cipherSuite, Keystore.getDefaultCipherSuites(), null);

        // set context menu for mq connection list
        listview_mqConnectionList.setCellFactory( lv -> {
            ListCell<String> cell = new ListCell<>();
            final ContextMenu contextMenu = new ContextMenu();

            cell.setOnMouseClicked(mouseEvent -> {
                if (mouseEvent.getButton() == MouseButton.SECONDARY && !cell.isEmpty()) {
                    contextMenu.hide();
                    contextMenu.getItems().clear();
                    MenuItem editProperty = new MenuItem("Edit");
                    MenuItem removeProperty = new MenuItem("Remove");
                    contextMenu.getItems().addAll(editProperty, removeProperty);
                    editProperty.setOnAction(event -> {
                        setValuesForEdit();
                        listview_mqConnectionList.setDisable(true);
                    });
                    removeProperty.setOnAction(event -> {
                        removeConnection();
                    });

                    contextMenu.show(cell, mouseEvent.getScreenX(), mouseEvent.getScreenY());
                }
            });

            cell.textProperty().bind(cell.itemProperty());

            return cell ;
        });

        refresh();
    }

    private void removeConnection() {
        Main.configManager.MQConnections.remove(listview_mqConnectionList.getSelectionModel().getSelectedIndex());
        Main.configManager.saveConfiguration();
        refresh();
    }

    private void setValuesForEdit() {
        MQConnectionEntry entry = Main.configManager.MQConnections.get(listview_mqConnectionList.getSelectionModel().getSelectedIndex());

        textfield_qmname.setText(entry.getQueueManager());
        textfield_hostname.setText(entry.getHostname());
        textfield_port.setText(entry.getPort());
        textfield_channel.setText(entry.getChannel());
        textfield_userid.setText(entry.getUserId());
        textfield_userPassword.setText(entry.getUserPassword());
        combobox_cipherSuite.getEditor().setText(entry.getCipherSuite());

        if (entry.getKeyStore() != null) {
            combobox_keystore.getSelectionModel().select(entry.getKeyStore());
        }
    }

    public void refresh() {
        combobox_keystore.getItems().clear();
        combobox_keystore.getItems().add(null);
        combobox_keystore.getItems().addAll(Main.configManager.keystores.values());

        listview_mqConnectionList.getItems().clear();
        for (MQConnectionEntry entry: Main.configManager.MQConnections) {
            listview_mqConnectionList.getItems().add(entry.getQueueManager() + " - (" + entry.getUserId() + "@" + entry.getHostname() + ":" + entry.getPort() + "/" + entry.getChannel() + ")");
        }

        MqTabController.getInstance().refreshQueueManagerList();
    }

    public void save(ActionEvent actionEvent) {
        if (!textfield_qmname.getText().isEmpty() && !textfield_hostname.getText().isEmpty() && !textfield_port.getText().isEmpty() && !textfield_channel.getText().isEmpty()) {

            MQConnectionEntry entry = new MQConnectionEntry(
                    textfield_qmname.getText(),
                    textfield_hostname.getText(),
                    textfield_port.getText(),
                    textfield_channel.getText(),
                    textfield_userid.getText(),
                    textfield_userPassword.getText()
            );

            if (combobox_keystore.getSelectionModel().getSelectedItem() != null) {
                entry.setCipherSuite(combobox_cipherSuite.getEditor().getText());
                entry.setKeyStore(combobox_keystore.getSelectionModel().getSelectedItem().toString());
            }

            if (listview_mqConnectionList.getSelectionModel().getSelectedIndex() >= 0 && listview_mqConnectionList.isDisabled())
                Main.configManager.MQConnections.set(listview_mqConnectionList.getSelectionModel().getSelectedIndex(), entry);
            else
                Main.configManager.MQConnections.add(entry);
            Main.configManager.saveConfiguration();

            listview_mqConnectionList.setDisable(false);
            textfield_qmname.setText("");
            textfield_hostname.setText("");
            textfield_port.setText("");
            textfield_channel.setText("");
            textfield_userid.setText("");
            textfield_userPassword.setText("");
            combobox_cipherSuite.getEditor().setText("");
            combobox_keystore.getSelectionModel().select(null);

            refresh();

        } else {
            tooltipManager.createAndShowNodeTooltip(button_save, "Fill all required items.");
        }
    }

    public Pane getPane() {
        return anchorPane;
    }
}

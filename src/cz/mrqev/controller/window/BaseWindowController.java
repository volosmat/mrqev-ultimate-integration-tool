package cz.mrqev.controller.window;

import cz.mrqev.controller.BaseController;
import cz.mrqev.interfaces.BaseWindow;
import cz.mrqev.interfaces.WindowController;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import cz.mrqev.view.MQWindow;

public abstract class BaseWindowController extends BaseController implements WindowController {
    protected static BaseWindowController instance;

    public MQWindow window;

    public static BaseWindowController getInstance() {
        return instance;
    }

    public void initialize() {
        instance = this;
        // focusNode is small trick to focus new createdWindow and handle keyPress event
        Label focusNode = new Label();
        getPane().getChildren().add(focusNode);
        focusNode.setFocusTraversable(true);
        focusNode.requestFocus();

        if (getPane() != null) {
            getPane().setOnKeyPressed(keyEvent -> {
                // close window (ESC)
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    close();
                }
            });
        }
    }

    public void setWindow(BaseWindow window) {
        this.window = (MQWindow) window;
    }

    public void close() {
        window.getStage().close();
        instance = null;
    }
}

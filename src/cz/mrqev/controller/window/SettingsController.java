package cz.mrqev.controller.window;

import cz.mrqev.view.MQDialogBox;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import cz.mrqev.main.Main;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class SettingsController extends BaseWindowController {
    public AnchorPane anchorPane;

    private static boolean restartRequired = false;

    // General
    public CheckBox checkbox_alwaysConfirmClosingOfMrQev;
    public CheckBox checkbox_alwaysConfirmClosingOfTab;
    // Custom settings
    public CheckBox checkbox_automaticallyQueueLoad;
    // Plugin
    public TextField textField_pluginFilePath;
    public Button button_selectJarFile;
    public Button button_savePlugin;
    public Label label_pluginName;

    public void initialize() {
        super.initialize();
        checkbox_alwaysConfirmClosingOfMrQev.setSelected(Main.configManager.getBoolProperty("settings.alwaysConfirmClosingOfMrQev"));
        checkbox_alwaysConfirmClosingOfTab.setSelected(Main.configManager.getBoolProperty("settings.alwaysConfirmClosingOfTab"));
        checkbox_automaticallyQueueLoad.setSelected(Main.configManager.getBoolProperty("settings.automaticallyQueueLoad"));

        String pluginPath = Main.configManager.getProperty("settings.pluginPath");
        if (pluginPath != null) {
            deselectPluginButton();
            textField_pluginFilePath.setText(pluginPath);
        }

        label_pluginName.setText(pluginManager.getPluginConfiguration().getName() + " v" + pluginManager.getPluginConfiguration().getVersion());

        if (restartRequired) {
            label_pluginName.setText(label_pluginName.getText() + " (restart required)");
        }
        textField_pluginFilePath.setOnMouseClicked(event -> selectJarFile());
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }

    public void checkbox_alwaysConfirmClosingOfMrQev(ActionEvent actionEvent) {
        if (checkbox_alwaysConfirmClosingOfMrQev.isSelected())
            Main.configManager.setBoolProperty("settings.alwaysConfirmClosingOfMrQev", true);
        else
            Main.configManager.setBoolProperty("settings.alwaysConfirmClosingOfMrQev", false);
    }

    public void checkbox_alwaysConfirmClosingOfTab(ActionEvent actionEvent) {
        if (checkbox_alwaysConfirmClosingOfMrQev.isSelected())
            Main.configManager.setBoolProperty("settings.alwaysConfirmClosingOfTab", true);
        else
            Main.configManager.setBoolProperty("settings.alwaysConfirmClosingOfTab", false);
    }

    public void checkbox_automaticallyQueueLoad(ActionEvent actionEvent) {
        if (checkbox_automaticallyQueueLoad.isSelected())
            Main.configManager.setBoolProperty("settings.automaticallyQueueLoad", true);
        else
            Main.configManager.setBoolProperty("settings.automaticallyQueueLoad", false);
    }

    public void selectJarFile() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Select java archive file", "*.jar"));
        fileChooser.setTitle("Open java archive file...");

        // load last jar folder path
        String lastJarFolder = Main.configManager.getProperty("interface.lastjarfolder");
        if (lastJarFolder != null) {
            File lastFolder = new File(lastJarFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            String path = file.getAbsolutePath();
            textField_pluginFilePath.setText(path);

            Main.configManager.setProperty("interface.lastjarfolder", file.getParent());
        }
    }

    public void savePlugin(ActionEvent actionEvent) {
        if (textField_pluginFilePath.getText() != null && !textField_pluginFilePath.getText().isEmpty()) {
            deselectPluginButton();
            Main.configManager.setProperty("settings.pluginPath", textField_pluginFilePath.getText());

            MQDialogBox.openInformation("Restart needed", "You successfully selected plugin.", "Restart application to load selected plugin.");
        } else {
            MQDialogBox.openError("No plugin selected", "Select jar file and then press save plugin button.", "");
        }

        restartRequired = true;
    }

    public void deselectPlugin(ActionEvent actionEvent) {
        button_savePlugin.setText("Save plugin");
        button_savePlugin.setOnAction(this::savePlugin);

        Main.configManager.setProperty("settings.pluginPath", null);
        textField_pluginFilePath.setText(null);
        label_pluginName.setText("No plugin selected");
        restartRequired = true;
    }

    private void deselectPluginButton() {
        button_savePlugin.setText("Deselect actual plugin");
        button_savePlugin.setOnAction(this::deselectPlugin);
    }
}

package cz.mrqev.controller.window;

import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.transport.ssl.Keystore;
import cz.mrqev.view.MQDialogBox;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class KeystoreManagerController extends BaseWindowController {
    public AnchorPane anchorPane;
    public ListView listview_keystores;
    public TextField textfield_keystoreFile;
    public Button button_saveKeystore;
    public PasswordField passwordfield_keystorePassword;

    public static KeystoreManagerController getInstance() {
        return (KeystoreManagerController) instance;
    }

    public void initialize() {
        super.initialize();
        // set context menu for mq connection list
        listview_keystores.setCellFactory( lv -> {
            ListCell<String> cell = new ListCell<>();
            final ContextMenu contextMenu = new ContextMenu();
            cell.setOnMouseClicked(mouseEvent -> {
                if (mouseEvent.getButton() == MouseButton.SECONDARY && !cell.isEmpty()) {
                    contextMenu.hide();
                    contextMenu.getItems().clear();
                    MenuItem removeProperty = new MenuItem("Remove");
                    contextMenu.getItems().add(removeProperty);
                    removeProperty.setOnAction(event -> {
                        removeKeystore();
                    });

                    contextMenu.show(cell, mouseEvent.getScreenX(), mouseEvent.getScreenY());
                }
            });

            cell.textProperty().bind(cell.itemProperty());

            return cell ;
        });
        
        listview_keystores.setPlaceholder(new Label("No configured keystores"));
        refreshKeystoreList();
    }

    private void removeKeystore() {
        String selectedKeystore = listview_keystores.getSelectionModel().getSelectedItem().toString();

        if (checkKeystoreInConnection(selectedKeystore)) {
            MQDialogBox.openError(
                    "Error to remove keystore",
                    "Keystore cannot be removed because is assigned to at least one defined MQ connection.",
                    "Remove keystore from existing MQ connection and try again.");
            return;
        }

        Main.configManager.keystores.remove(selectedKeystore);
        Main.configManager.saveConfiguration();
        refreshKeystoreList();
    }

    private boolean checkKeystoreInConnection(String keystore) {
        for (MQConnectionEntry entry: Main.configManager.MQConnections) {
            if (entry.getKeyStore() != null && entry.getKeyStore().getKeystoreFile().equals(keystore)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }

    public void saveKeystore(ActionEvent actionEvent) {
        if (!textfield_keystoreFile.getText().isEmpty() && !passwordfield_keystorePassword.getText().isEmpty()) {
            try {
                KeyStore keyStore = KeyStore.getInstance("JKS");
                keyStore.load(new FileInputStream(textfield_keystoreFile.getText()),passwordfield_keystorePassword.getText().toCharArray());
            } catch (KeyStoreException | CertificateException | NoSuchAlgorithmException | IOException e) {
                MQDialogBox.openError("Error to save keystore","Cannot open keystore " + textfield_keystoreFile.getText(), "Error message: " + e.getMessage());
            }

            Keystore keystore = new Keystore(textfield_keystoreFile.getText(), passwordfield_keystorePassword.getText());
            Main.configManager.keystores.put(keystore.getKeystoreFile(), keystore);
            Main.configManager.saveConfiguration();
            refreshKeystoreList();
        } else {
            tooltipManager.createAndShowNodeTooltip(button_saveKeystore, "Fill all required items.");
        }
    }

    public void refreshKeystoreList() {
        listview_keystores.getItems().clear();
        listview_keystores.getItems().addAll(Main.configManager.keystores.keySet());
    }

    public void selectKeystoreFile() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Select JKS file", "*.jks"));
        fileChooser.setTitle("Open keystore file...");

        // load last session folder path
        String lastXsdFolder = Main.configManager.getProperty("interface.lastkeystorefolder");
        if (lastXsdFolder != null) {
            File lastFolder = new File(lastXsdFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            String path = file.getAbsolutePath();
            textfield_keystoreFile.setText(path);

            Main.configManager.setProperty("interface.lastkeystorefolder", file.getParent());
        }
    }
}

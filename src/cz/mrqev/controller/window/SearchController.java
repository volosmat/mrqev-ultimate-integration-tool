package cz.mrqev.controller.window;

import cz.mrqev.controller.tab.DataTabController;
import cz.mrqev.controller.tab.ReplyTabController;
import cz.mrqev.controller.tab.RequestTabController;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import cz.mrqev.manager.UtilsManager;
import org.fxmisc.richtext.CodeArea;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchController extends BaseWindowController {

    public TextField textfield_find;
    public TextField textfield_replace;
    public RadioButton radiobutton_forward;
    public RadioButton radiobutton_backward;
    public CheckBox checkbox_caseSensitive;
    public CheckBox checkbox_wholePhase;
    public CheckBox checkbox_wrapSearch;
    public Label label_infoText;
    public Button button_find;
    public Button button_replace;
    public Button button_replaceFind;
    public Button button_replaceAll;
    public AnchorPane anchorPane;

    // textArea of codeArea with search data
    private CodeArea searchTextArea;

    private int matchesCount = 0;
    private String lastSearchPhase = "";

    public static SearchController getInstance() {
        return (SearchController) instance;
    }

    public void initialize() {
        super.initialize();

        listenerManager.createSearchButtonsListener();
        listenerManager.createFindOnEnterPressListener();
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }

    public void find(ActionEvent actionEvent) {
        label_infoText.setText("");
        CodeArea dataTextArea = getDataTextArea();
        String searchText = textfield_find.getText();
        String data = dataTextArea.getText();

        // when backward is selected, search from the end of string
        if (radiobutton_backward.isSelected()) {
            searchText = UtilsManager.reverseString(searchText);
            data = UtilsManager.reverseString(data);
        }

        // check if we have new search phase, if yes refresh counter
        if (!lastSearchPhase.equals(searchText)) {
            matchesCount = 0;
            lastSearchPhase = searchText;
        }
        // non case sensitive (user lower case for data and find)
        if (!checkbox_caseSensitive.isSelected()) {
            searchText = searchText.toLowerCase();
            data = data.toLowerCase();
        }

        int startPos = 0;
        String patternString = searchText;

        if (checkbox_wholePhase.isSelected()) {
            patternString = "\\b" + patternString + "\\b";
        }

        Pattern pattern = Pattern.compile(patternString);
        Matcher matcher = pattern.matcher(data);

        try {
            for (int i = 0; i <= matchesCount; i++) {
                matcher.find();
                startPos = matcher.start();
            }

            if (radiobutton_backward.isSelected()) {
                startPos = data.length() - startPos - searchText.length();
            }

            // select found string
            dataTextArea.selectRange(startPos, startPos + searchText.length());

            matchesCount++;
        } catch (IllegalStateException e) {
            if (matchesCount > 0) {
                // search reached end of data
                label_infoText.setText("End of data");
            } else {
                // string not found in whole data text
                label_infoText.setText("String not found");

            }
            // deselect all highlighted text
            dataTextArea.selectRange(0,0);
            // we are at the end of text, if user want set counter to zero and start again
            if (checkbox_wrapSearch.isSelected()) {
                matchesCount = 0;
            }
        }
    }

    public void replace(ActionEvent actionEvent) {
        CodeArea dataTextArea = getDataTextArea();
        StringBuffer buffer = new StringBuffer(dataTextArea.getText());
        // caretPosition is position at the end of textarea selection
        buffer.replace(
                dataTextArea.getCaretPosition() - textfield_find.getText().length(),
                dataTextArea.getCaretPosition(),
                textfield_replace.getText());

        dataTextArea.replaceText(buffer.toString());
        // now we minus one match
        matchesCount--;
    }

    public void replaceFind(ActionEvent actionEvent) {
        replace(actionEvent);
        find(actionEvent);
    }

    public void replaceAll(ActionEvent actionEvent) {
        CodeArea dataTextArea = getDataTextArea();
        String data = dataTextArea.getText();

        dataTextArea.replaceText(data.replaceAll(textfield_find.getText(), textfield_replace.getText()));
    }

    private CodeArea getDataTextArea() {
        if (searchTextArea != null) {
            return searchTextArea;
        } else if (RequestTabController.getInstance().tabs.getSelectionModel().isSelected(RequestTabController.DATA_TAB_POSITION)) {
            return DataTabController.getInstance().codearea_requestData;
        } else if (RequestTabController.getInstance().tabs.getSelectionModel().isSelected(RequestTabController.REPLY_TAB_POSITION)) {
            return ReplyTabController.getInstance().codearea_reply;
        } else {
            // if we open search window and switch to another tab
            return new CodeArea();
        }
    }

    public void setSearchTextArea(CodeArea textAreaOrCodeArea) {
        this.searchTextArea = textAreaOrCodeArea;
    }
}

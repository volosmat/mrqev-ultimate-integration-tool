package cz.mrqev.controller.window;

import cz.mrqev.controller.tab.DataTabController;
import cz.mrqev.controller.tab.RequestTabController;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.model.xml.XmlHolder;
import cz.mrqev.model.xml.XsdHolder;

import javax.xml.transform.TransformerConfigurationException;
import java.io.File;

public class GenerateRequestController extends BaseWindowController {
    public AnchorPane anchorPane;
    public TextField textfield_rootElement;
    public TextField textfield_namespace;
    public CheckBox checkbox_generateOptionalElements;
    public CheckBox checkbox_generateOptionalAttributes;
    public TextField textfield_numberOfRepetitions;
    public TextField textfield_xsdPath;
    public Button button_generate;

    public static GenerateRequestController getInstance() {
        return (GenerateRequestController) instance;
    }

    public void initialize() {
        super.initialize();
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }

    public void generate(ActionEvent actionEvent) {
        try {
            XmlHolder xmlHolder = XmlHolder.fromXsd(textfield_xsdPath.getText());
            DataTabController.getInstance().codearea_requestData.replaceText(xmlHolder.getSourceXml());
            RequestTabController.getInstance().tabs.getSelectionModel().select(RequestTabController.DATA_TAB_POSITION);
            close();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void selectXsd() {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Select XSD file", "*.xsd"));
        fileChooser.setTitle("Open XSD file...");

        // load last session folder path
        String lastXsdFolder = Main.configManager.getProperty("interface.lastxsdfolder");
        if (lastXsdFolder != null) {
            File lastFolder = new File(lastXsdFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            String path = file.getAbsolutePath();
            textfield_xsdPath.setText(path);
            XsdHolder xsdHolder = new XsdHolder(path);
            textfield_rootElement.setText(xsdHolder.getRootElementName());
            textfield_namespace.setText(xsdHolder.getRootElementNamespace());
            button_generate.setDisable(false);

            Main.configManager.setProperty("interface.lastxsdfolder", file.getParent());
        }
    }
}

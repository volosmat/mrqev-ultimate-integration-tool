package cz.mrqev.controller.window;

import com.ibm.mq.*;
import cz.mrqev.controller.tab.MqTabController;
import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import cz.mrqev.model.mq.actions.MQBrowser;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.view.MQBrowseBox;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.util.ArrayList;


public class BrowseController extends BaseWindowController {
    public TableView<MQMessageWrapper> messagetable;
    public TableColumn tcPos;
    public TableColumn tcDatetime;
    public TableColumn tcLength;
    public TableColumn tcData;
    public TableColumn tcMessageID;
    public TableColumn tcCorrelID;
    public TableColumn tcEncoding;
    public TableColumn tcExpiry;
    public TableColumn tcMessageType;
    public AnchorPane anchorPane;

    private MqTabController relatedMqController;
    private MQBrowser mqBrowser;

    public void initialize() {
        super.initialize();

        relatedMqController = MqTabController.getInstance();

        tcPos.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, Integer>("Position")
        );
        tcDatetime.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("DateTime")
        );
        tcLength.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, Integer>("Length")
        );
        tcData.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("Data")
        );
        tcMessageID.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("MessageIdAsString")
        );
        tcCorrelID.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("CorrelIdAsString")
        );
        tcEncoding.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("Encoding")
        );
        tcExpiry.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, Integer>("Expiry")
        );
        tcMessageType.setCellValueFactory(
                new PropertyValueFactory<MQMessageWrapper, String>("MessageType")
        );

        // open message dialog after double click on row
        messagetable.setRowFactory( tv -> {
            TableRow<MQMessageWrapper> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (event.getClickCount() == 2 && (! row.isEmpty()) ) {
                    MQBrowseBox.openBrowseDialog(row.getItem());
                }
                if (event.getButton() == MouseButton.SECONDARY) {
                    final ContextMenu contextMenu = new ContextMenu();
                    MenuItem copyCorrelId = new MenuItem("Copy CorrelId");
                    MenuItem copyMessageId = new MenuItem("Copy MessageId");
                    contextMenu.getItems().addAll(copyCorrelId, copyMessageId);
                    copyCorrelId.setOnAction(event1 -> {
                        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                        clpbrd.setContents(new StringSelection(row.getItem().getCorrelIdAsString()), null);
                    });
                    copyMessageId.setOnAction(event12 -> {
                        Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                        clpbrd.setContents(new StringSelection(row.getItem().getMessageIdAsString()), null);
                    });
                    contextMenu.show(row, event.getScreenX(), event.getScreenY());
                }
            });
            return row ;
        });

        messagetable.setOnKeyPressed(keyEvent -> {
            if (keyEvent.getCode() == KeyCode.ESCAPE) {
                Stage stage = (Stage) messagetable.getScene().getWindow();
                stage.close();
            }
        });

        try {
            int browseCount = Integer.valueOf(relatedMqController.textfield_browseCount.getText());
            MQConnectionEntry connectionEntry = relatedMqController.getSelectedMqConnection();
            String queueString = relatedMqController.getSelectedQueue();
            String filter = relatedMqController.getFilter();

            mqBrowser = new MQBrowser(connectionEntry, queueString, browseCount, filter, relatedMqController::addLogMessage);

            // set stop browsing button and his action
            setStopBrowsingButton();

            // get messages
            ArrayList<MQMessageWrapper> messages = mqBrowser.browseMessages();
            // add to table
            messagetable.getItems().addAll(messages);

            // restore browse queue button and his action
            setBrowseQueueButton();

        } catch (MQException e) {
            if (e.reasonCode == 2085) {
                relatedMqController.addLogMessage("Error browsing queue. Specified queue "+ relatedMqController.getSelectedQueue() +" does not exist.", true);
            } else {
                relatedMqController.addLogMessage("Error browsing queue. " + e.toString(), true);
            }

            // restore browse queue button and his action
            setBrowseQueueButton();
        }
    }

    private void setStopBrowsingButton() {
        // In JavaFX we cannot update UI component from different threat so we have to use runLater method
        Platform.runLater(() -> {
            relatedMqController.button_browseQueue.setText("Stop browsing");
            relatedMqController.button_browseQueue.setOnAction((e)-> mqBrowser.setBrowseInProgress(false));
        });
    }

    private void setBrowseQueueButton() {
        // In JavaFX we cannot update UI component from different threat so we have to use runLater method
        Platform.runLater(() -> {
            relatedMqController.button_browseQueue.setText("Browse queue");
            relatedMqController.button_browseQueue.setOnAction((e)-> relatedMqController.browseQueue(e));
        });
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }
}

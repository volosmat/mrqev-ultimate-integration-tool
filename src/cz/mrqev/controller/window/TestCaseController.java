package cz.mrqev.controller.window;

import cz.mrqev.controller.tab.DataTabController;
import cz.mrqev.controller.tab.MqmdTabController;
import cz.mrqev.controller.tab.WsTabController;
import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import cz.mrqev.view.MQDialogBox;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class TestCaseController extends BaseWindowController {
    public ListView listview_testCases;
    public AnchorPane anchorPane;

    String filepath;

    HashMap<Integer, String> testCases = new HashMap<>();

    public void selectTestcase(ActionEvent actionEvent) {
        try {
            if (listview_testCases.getSelectionModel().isEmpty()) {
                MQDialogBox.openError("Error", "No test case selected", "Please select a test case to be imported");
                return;
            }

            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = domFactory.newDocumentBuilder();

            Document doc = builder.parse(filepath);
            XPath xpath = XPathFactory.newInstance().newXPath();

            String testSuiteName = testCases.get(listview_testCases.getSelectionModel().getSelectedIndex()).split("_____")[0];
            String testCaseName = testCases.get(listview_testCases.getSelectionModel().getSelectedIndex()).split("_____")[1];

            XPathExpression exprRequest = xpath.compile("/*[local-name()='soapui-project']/*[local-name()='testSuite'][@name='"+testSuiteName+"']/*[local-name()='testCase'][@name='"+testCaseName+"']/*[local-name()='testStep'][@type='request']/*[local-name()='config']/*[local-name()='request']/*[local-name()='request']/text()");
            XPathExpression exprEndpoint = xpath.compile("/*[local-name()='soapui-project']/*[local-name()='testSuite'][@name='"+testSuiteName+"']/*[local-name()='testCase'][@name='"+testCaseName+"']/*[local-name()='testStep'][@type='request']/*[local-name()='config']/*[local-name()='request']/*[local-name()='endpoint']/text()");
            XPathExpression exprRequestWSS = xpath.compile("/*[local-name()='soapui-project']/*[local-name()='testSuite'][@name='"+testSuiteName+"']/*[local-name()='testCase'][@name='"+testCaseName+"']/*[local-name()='testStep'][@type='request']/*[local-name()='config']/*[local-name()='request']/@outgoingWss");

            Node nodeWSS = (Node) exprRequestWSS.evaluate(doc, XPathConstants.NODE);

            String WSSusername = "";
            String WSSpassword = "";

            if (nodeWSS != null) {
                XPathExpression exprRequestWSSUsername = xpath.compile("/*[local-name()='soapui-project']/*[local-name()='wssContainer']/*[local-name()='outgoing']/*[local-name()='name' and text()='" + nodeWSS.getNodeValue() + "']/../*[local-name()='entry']/@username");
                XPathExpression exprRequestWSSPassword = xpath.compile("/*[local-name()='soapui-project']/*[local-name()='wssContainer']/*[local-name()='outgoing']/*[local-name()='name' and text()='" + nodeWSS.getNodeValue() + "']/../*[local-name()='entry']/@password");

                WSSusername = ((Node) exprRequestWSSUsername.evaluate(doc, XPathConstants.NODE)).getNodeValue();
                WSSpassword = ((Node) exprRequestWSSPassword.evaluate(doc, XPathConstants.NODE)).getNodeValue();
            }

            String requestData = ((Node) exprRequest.evaluate(doc, XPathConstants.NODE)).getNodeValue();
            String endpoint = ((Node) exprEndpoint.evaluate(doc, XPathConstants.NODE)).getNodeValue();

            requestData = requestData.replace("\\r", "");

            if (!endpoint.equals("")) {
                WsTabController.getInstance().textfield_webServiceUrl.setText(endpoint);
                WsTabController.getInstance().addLogMessage("Successfully extracted endpoint "+endpoint, true);
            } else {
                WsTabController.getInstance().addLogMessage("Couldn't extract endpoint. Please enter it manually in WS tab.", true);
            }
            if (!requestData.equals("")) {
                DataTabController.getInstance().codearea_requestData.replaceText(requestData);
                WsTabController.getInstance().addLogMessage("Successfully extracted request data", true);
            } else {
                WsTabController.getInstance().addLogMessage("Couldn't extract request data. Please enter it manually in Data tab.", true);
            }
            if (!WSSusername.equals("")) {
                MqmdTabController.getInstance().combobox_wssUser.setValue(WSSusername);
                WsTabController.getInstance().addLogMessage("Successfully extracted WSS username "+WSSusername, true);
            } else {
                WsTabController.getInstance().addLogMessage("Couldn't extract WSS username. Please select in manually in MQMD/WSS tab.", true);
            }
            if (!WSSpassword.equals("")) {
                MqmdTabController.getInstance().textfield_wssPassword.setText(WSSpassword);
                WsTabController.getInstance().addLogMessage("Successfully extracted WSS password "+WSSpassword, true);
            } else {
                WsTabController.getInstance().addLogMessage("Couldn't extract WSS password. Please select/enter in manually in MQMD/WSS tab.", true);
            }

            Stage stage = (Stage) listview_testCases.getScene().getWindow();
            stage.close();

        } catch (ParserConfigurationException | IOException | XPathExpressionException | SAXException e) {
            MQDialogBox.openException(e);
        }
    }

    public void initialize() {
        try {
            listview_testCases.setPlaceholder(new Label("No SoapUI TestCases found"));

            Stage stage = new Stage();
            FileChooser fileChooser = new FileChooser();
            fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("SoapUI projects", "*.xml"));
            fileChooser.setTitle("Open SoapUI project...");

            if (Main.configManager.getProperty("interface.lastsessionfolder") != null) {
                File f = new File(Main.configManager.getProperty("interface.lastsessionfolder"));
                if (f.exists())
                    fileChooser.setInitialDirectory(new File(Main.configManager.getProperty("interface.lastsessionfolder")));
            }

            File file = fileChooser.showOpenDialog(stage);

            if (file != null && file.exists()) {
                filepath = file.getAbsolutePath();
                DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = domFactory.newDocumentBuilder();

                Document doc = builder.parse(filepath);

                // XPath Query for showing all nodes value
                XPathExpression exprTestSuites = XPathFactory.newInstance().newXPath().compile("/*[local-name()='soapui-project']/*[local-name()='testSuite']/@name");

                NodeList nodes = (NodeList) exprTestSuites.evaluate(doc, XPathConstants.NODESET);
                int position = 0;
                for (int i = 0; i < nodes.getLength(); i++) {
                    XPathExpression exprTestCases = XPathFactory.newInstance().newXPath().compile("/*[local-name()='soapui-project']/*[local-name()='testSuite'][@name='"+nodes.item(i).getNodeValue()+"']/*[local-name()='testCase']/@name");
                    NodeList nodesTestCases = (NodeList) exprTestCases.evaluate(doc, XPathConstants.NODESET);
                    for (int j = 0; j < nodesTestCases.getLength(); j++) {
                        listview_testCases.getItems().add(nodes.item(i).getNodeValue() + " - " + nodesTestCases.item(j).getNodeValue());
                        testCases.put(position, nodes.item(i).getNodeValue()+"_____"+nodesTestCases.item(j).getNodeValue());
                        position++;
                    }
                }

                Main.configManager.setProperty("interface.lastsessionfolder", file.getParent());
            }
        } catch (ParserConfigurationException | IOException | XPathExpressionException | SAXException e) {
            MQDialogBox.openException(e);
        }

        super.initialize();
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }
}

package cz.mrqev.controller.window;

import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

public class ShortcutsController extends BaseWindowController {
    public AnchorPane anchorPane;

    public void initialize() {
        super.initialize();
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }
}

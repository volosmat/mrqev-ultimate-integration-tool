package cz.mrqev.controller.window;

import javafx.event.ActionEvent;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import cz.mrqev.main.Main;
import cz.mrqev.view.MQDialogBox;

import java.awt.*;
import java.net.URI;

public class AboutController extends BaseWindowController {

    public Label labelVersion;
    public AnchorPane anchorPane;

    public void initialize() {
        super.initialize();

        labelVersion.setText("v. " + Main.version);
    }

    @Override
    public Pane getPane() {
        return anchorPane;
    }

    public void openConfluence(ActionEvent actionEvent) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(new URI("https://s08983dt.servers.kbc.be:6081/confluence/pages/viewpage.action?pageId=151532677"));
            } catch (Exception e) {
                MQDialogBox.openException(e);
            }
        }
    }
}

package cz.mrqev.controller.window;

import cz.mrqev.controller.tab.*;
import cz.mrqev.interfaces.Plugin;
import cz.mrqev.event.listener.TabListener;
import cz.mrqev.interfaces.model.Consumer;
import cz.mrqev.interfaces.model.Consumers;
import cz.mrqev.manager.SessionManager;
import cz.mrqev.model.Version;
import cz.mrqev.view.MQDialogBox;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.MenuBar;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import cz.mrqev.main.Main;
import org.apache.commons.lang3.StringUtils;
import cz.mrqev.view.MQTab;
import cz.mrqev.view.MQWindow;
import cz.mrqev.interfaces.annotation.Responsive;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * MainController is root controller where all others UI controls are placed and controlled.
 *
 * Template associated with this controller:
 * @see <a href="./view/templates/window/main.fxml'>Main window template</a>
 * For more information:
 * @see <a href="https://s08983dt.servers.kbc.be:6081/confluence/display/CZSI/MrQev+Development+Documentation'>MrQev Development Documentation</a>
 */
public class MainController extends BaseWindowController {
    // Default prefix for new created tab
    public final static String NEW_REQUEST_TAB_NAME = "New request";
    // Base grid where are all ui controls placed
    public GridPane mainGrid;
    // TabPane for requests tabs (bottom tabs)
    public TabPane requestTabPane;
    // Special TabPane where is placed "+" tab for add new request tab
    public TabPane addTabPane;

    // Total number of actually opened request tabs e.g. when we create new tab his name is composed of NEW_REQUEST_TAB_NAME + totalRequestTabsCreated
    public int totalRequestTabsCreated = 1;
    // MainController used singleton design pattern
    private static MainController instance;
    // HashMap of all instances or request tab controllers
    private static HashMap<String, RequestTabController> requestTabControllers = new HashMap<>();

    // Path of folder where was last opened file through FileChooser stored
    public static String dataFileParentPath;
    // Path where application executable file is stored
    public static String applicationPath = Paths.get(".").toAbsolutePath().normalize().toString();
    // Projects tree
    public TreeView treeView_Projects;
    @Responsive(changeWidth = 1, changeHeight = 1)
    public AnchorPane requestAnchorPane;
    public MenuBar menuBar_topMenu;

    // Import WSS users
    public Function<String, Consumers> consumersImportFunction;
    public String consumerFileExtFilter;
    public Consumers consumers;

    public List<String> getWssNames(boolean forceParsing) {
        String consumersPath = Main.configManager.getProperty("interface.consumers");
        // parse consumers
        if (forceParsing || (consumers == null && consumersPath != null && !consumersPath.isEmpty()) && consumersImportFunction != null) {
            consumers = consumersImportFunction.apply(consumersPath);
        }

        // if consumers were already parsed, just return wss names
        if (consumers != null) {
            return consumers.getConsumers().stream().map(Consumer::getWssUsername).filter(name -> name != null && !name.isEmpty()).collect(Collectors.toList());
        }

        // no parsed consumers so return empty list
        return List.of();
    }

    /**
     * Get name of request tab which is controlled by given RequestTabController
     * @param requestTabController - instance of controller you want to get name
     * @return name of request tab or null if controller instance is not stored in requestTabControllers collection
     */
    public static String getRequestTabControllerName(RequestTabController requestTabController) {
        for (HashMap.Entry<String, RequestTabController> requestTabControllerEntry: requestTabControllers.entrySet()) {
            if (requestTabControllerEntry.getValue().equals(requestTabController))
                return requestTabControllerEntry.getKey();
        }
        return null;
    }

    /**
     * Add new instance of RequestTabController to HashMap<tabName, controller instance>
     * Name of new created tab will be calculated from getLastCreatedTabName function and add tab to the end
     * @param requestTabController - instance of controller we want to add
     */
    public static void addTabController(RequestTabController requestTabController) {
        String tabName = MainController.getInstance().getLastCreatedTabName();
        requestTabControllers.put(tabName, requestTabController);
    }

    /**
     * Add new instance of RequestTabController to HashMap<tabName, controller instance>
     * Name of new created tab is received as parameter
     * @param tabName - name of request tab
     * @param requestTabController - instance of controller we want to add
     */
    public static void addTabController(String tabName, RequestTabController requestTabController) {
        requestTabControllers.put(tabName, requestTabController);
    }

    /**
     * Remove instance of RequestTabController
     * @param tabName - name of request tab we want to remove
     */
    public static void removeTabController(String tabName) {
        requestTabControllers.remove(tabName);
    }

    /**
     * Get selected tab controller instance
     * @return instance of actual selected RequestTabController
     */
    public static RequestTabController getSelectedTabController() {
        String tabName = MainController.getInstance().getSelectedTabName();
        return requestTabControllers.get(tabName);
    }

    /**
     * Get instance of RequestTabController for given name of request tab
     * @param tabName - name of request tab we want instance of
     * @return instance of RequestTabController
     */
    public static RequestTabController getTabController(String tabName) {
        return requestTabControllers.get(tabName);
    }

    /**
     * Get all instances of tab controllers
     * @return instances of RequestTabController
     */
    public static HashMap<String, RequestTabController> getTabControllers() {
        return requestTabControllers;
    }

    /**
     * Check if tab with given name was already created in the past
     * @param tabName - tab name to check
     * @return boolean
     */
    public static boolean tabNameExists(String tabName) {
        return requestTabControllers.containsKey(tabName);
    }


    /**
     * MainController used singleton design pattern
     * @return this
     */
    public static MainController getInstance() {
        return instance;
    }

    /**
     * Initialize function is invoked when JavaFX load all @FXML UI controls from template
     */
    public void initialize() {
        instance = this;
        super.initialize();

        new Thread(this::versionCheck).start();

        // add first tab
        addRequestTab();

        // initialize plugin
        if (pluginManager.getPlugin() != null) {
            pluginManager.getPlugin().initialize();
            getSelectedTabController().refreshNodes();
        }

        // select second tab (Default request)
        requestTabPane.getSelectionModel().select(1);
        // add listener to create new request tab
        listenerManager.createNewRequestTabListener();
        listenerManager.createShortcuts();
        listenerManager.createClosingDialogListener();

        Main.configManager.checkForEmptyProperties();

        // i dont know why but i cannot change anchorPane prefSize dynamically during window resize
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        requestAnchorPane.setPrefSize(screenSize.getWidth()*2, screenSize.getHeight()*2);
    }

    /**
     * Check version with confluence hidden version number.
     * On page https://s08983dt.servers.kbc.be:6081/confluence/pages/viewpage.action?pageId=151532677 is hidden html div tag:
     *   <pre>
     *      {@code
     *          <div style="display: none">
     *              <!-- nemazat !!! slouzi pro version check -->
     *              currentversion=2.0.1
     *          </div>
     *      }
     *   </pre>
     * it represents actual production ready version.
     */
    public void versionCheck() {
        try {
            URL url = new URL("https://s08983dt.servers.kbc.be:6081/confluence/plugins/viewsource/viewpagesrc.action?pageId=151532677");
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(2000);
            connection.setReadTimeout(2000);

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    connection.getInputStream(), StandardCharsets.UTF_8));

            String inputLine;
            StringBuilder a = new StringBuilder();
            while ((inputLine = in.readLine()) != null)
                a.append(inputLine);
            in.close();

            Pattern patternVersion = Pattern.compile("currentversion=(\\d).(\\d).(\\d)");
            Matcher matcherVersion = patternVersion.matcher(a);

            Pattern patternChangelog = Pattern.compile("<table.*?data-macro-parameters=\"title=Changelog\".*?><tr><td class=\"wysiwyg-macro-body\">(.*?)</td></tr></table>");
            Matcher matcherChangelog = patternChangelog.matcher(a);

            if (matcherVersion.find()) {
                Version local = new Version(Main.version);
                Version current = new Version(matcherVersion.group(0).replace("currentversion=", ""));
                if (current.compareTo(local) == 1) {
                    String changelog = "Could not get changelog from Confluence :(";
                    if (matcherChangelog.find()) {
                        changelog=matcherChangelog.group(1).replace("<p>","").replace("</p>","\r\n");
                    }
                    String finalChangelog = changelog;
                    Platform.runLater(() ->
                            MQDialogBox.openNewVersionWindow("New version", "MrQev " + matcherVersion.group(0).replace("currentversion=", "") + " is available", finalChangelog));
                }
            } else {
                System.out.println("Could not find version on confluence...");
            }

        } catch (IOException e) {
            e.printStackTrace();
            Platform.runLater(()-> MqTabController.getInstance().addLogMessage("Failed to check version on Confluence, probably a timeout." ,true));
        }
    }

    /**
     * New tab is always created directly after last request tab
     * @return name of last created tab
     */
    public String getLastCreatedTabName() {
        Tab lastCreatedTab = requestTabPane.getTabs().get(requestTabPane.getTabs().size()-1);
        return MQTab.getTabTitle(lastCreatedTab);
    }

    /**
     * Get name of actual selected tab
     * @return tab name
     */
    public String getSelectedTabName() {
        return MQTab.getTabTitle(getSelectedTab());
    }

    /**
     * Get instance of actual selected JavaFX tab
     * @return Tab instance
     */
    public Tab getSelectedTab() {
        return requestTabPane.getSelectionModel().getSelectedItem();
    }

    /**
     * Save session with SessionManager to previously defined file
     * @see SessionManager
     */
    public void saveSession() {
        RequestTabController.getInstance().sessionManager.saveSession();
    }

    /**
     * Open session with SessionManager
     * @see SessionManager
     */
    public void openSession() {
        RequestTabController.getInstance().sessionManager.openSession();
    }

    /**
     * Open session with SessionManager in new tab
     * @see SessionManager
     */
    public void openSessionInNewTab() {
        MainController.getInstance().addRequestTab();
        MainController.getInstance().requestTabPane.getSelectionModel().selectLast();
        RequestTabController.getInstance().sessionManager.openSession();
    }

    /**
     * Save session to selected file
     * @see SessionManager
     */
    public void saveSessionAs() {
        RequestTabController.getInstance().sessionManager.saveSessionAs();
    }

    /**
     * Get main window grid, it is used for instance for default shortcuts
     * @return GridPane instance
     */
    public Pane getPane() {
        return mainGrid;
    }

    /**
     * Create and add new request tab
     * @see MQTab
     */
    public void addRequestTab() {
        // new tab will be added default to place directly after default tab
        String tabName = getNextTabName();
        MQTab tab = new MQTab("tab/mqrequesttabpane", tabName, requestTabPane);

        tab.addListener((TabListener) event -> {
            Plugin plugin = pluginManager.getPlugin();

            if (plugin != null) {
                plugin.initializeTabs().forEach((pair) -> {
                    getSelectedTabController().addPluginTabController(pair.getKey());
                    getSelectedTabController().tabs.getTabs().add(pair.getValue());
                });

                Map<String, Integer> tabOrder = pluginManager.getPluginConfiguration().tabOrder();

                getSelectedTabController().tabs.getTabs().sort(Comparator.comparingInt(o -> tabOrder.get(o.getText())));
                // refresh view of necessary nodes (e.g. some plugin changes)
                getSelectedTabController().refreshNodes();
            }

            // we have to prevent recalculate when only first node is created during main window initialization, at this time window attribute is null
            if (totalRequestTabsCreated > 1) {
                window.getResponsiveness().recalculateSizeAndPositionOfAllNodesInTab(tabName);
            }
        });

        tab.createTabDynamically();

        totalRequestTabsCreated++;
    }

    /**
     * Get name available for new last tab with default name
     * @return tab name
     */
    public String getNextTabName() {
        return MainController.NEW_REQUEST_TAB_NAME + " " + totalRequestTabsCreated;
    }

    /**
     * Get name available for new last tab with specific given prefix e.g. when we open session we want to create tab with session name
     * @param tabName - tab name prefix
     * @return available tab name
     */
    public String getNextAvailableTabNameFor(String tabName) {
        String result = tabName;
        int counter = 1;

        if (result.length() < 3) {
            result = StringUtils.rightPad(result, 2, "0");
        }

        while (true) {
            // new name is valid when not already exists or if new name is same as already selected tab
            if (!requestTabControllers.containsKey(result + " " + counter) || MQTab.getTabTitle(requestTabPane.getSelectionModel().getSelectedItem()).equals(result + " " + counter)) {
                return result + " " + counter;
            }

            counter++;
        }
    }

    /**
     * Open shortcuts window
     * @see MQWindow
     */
    public void openShortcuts() {
        new MQWindow("window/shortcuts", "Shortcuts", 500, 350, false, null, false, false, true);
    }

    /**
     * Open about MrQev window
     * @see MQWindow
     */
    public void openAbout() {
        new MQWindow("window/about", "About MrQev", 450, 250, false, null, false, false, true);
    }

    /**
     * Open settings window
     * @see MQWindow
     */
    public void openSettings() {
        new MQWindow("window/settings", "Settings", 350, 500, false, null, false, false, false);
    }

    public void openGenerateSampleXml(ActionEvent actionEvent) {
        DataTabController.getInstance().generateRequest(actionEvent);
    }

    public void openKeystoreManager(ActionEvent actionEvent) {
        new MQWindow("window/keystoremanager", "Keystore manager", 450, 350, false, requestAnchorPane.getScene().getWindow(), false, false, false);
    }
}

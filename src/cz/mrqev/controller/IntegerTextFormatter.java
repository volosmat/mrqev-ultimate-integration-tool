package cz.mrqev.controller;

import javafx.scene.control.TextFormatter;
import javafx.util.converter.IntegerStringConverter;

import java.util.function.UnaryOperator;

/**
 * Created by JE01514 on 27. 7. 2017.
 */
public class IntegerTextFormatter {
    public static TextFormatter<Integer> getTextFormatterWithDefaultValue(Integer defaultValue) {
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("([0-9]*)?")) {
                return change;
            }
            return null;
        };
        return new TextFormatter<>(new IntegerStringConverter(), defaultValue, integerFilter);
    }

    public static TextFormatter<Integer> getTextFormatterWithDefaultValueWithNegative(Integer defaultValue) {
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?([0-9]*)?")) {
                return change;
            }
            return null;
        };
        return new TextFormatter<>(new IntegerStringConverter(), defaultValue, integerFilter);
    }

    public static TextFormatter<Integer> getTextFormatterWithDefaultValueForExpiry(Integer defaultValue) {
        UnaryOperator<TextFormatter.Change> integerFilter = change -> {
            String newText = change.getControlNewText();
            if (newText.matches("-?(1)?|([0-9]*)?")) {
                return change;
            }
            return null;
        };
        return new TextFormatter<>(new IntegerStringConverter(), defaultValue, integerFilter);
    }
}
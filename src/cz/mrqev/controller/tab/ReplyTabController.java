package cz.mrqev.controller.tab;

import com.ibm.mq.*;
import cz.mrqev.controller.IntegerTextFormatter;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.view.TextHighlighter;
import cz.mrqev.model.mq.actions.MQReader;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.session.SessionWrapper;
import org.apache.commons.codec.binary.Hex;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import org.xml.sax.SAXException;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.interfaces.annotation.Responsive;

import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;
import java.util.Scanner;

public class ReplyTabController extends BaseTabController {
    public final static String VALIDATION_READY = "Ready for validation";
    public final static String VALIDATION_NO_XSD = "No XSD selected";

    //Reply
    @Responsive(changeWidth = 1, changeHeight = 1)
    public CodeArea codearea_reply;
    @Responsive(changeWidth = 1, changeHeight = 1)
    public VirtualizedScrollPane<CodeArea> vsPane;
    @Responsive(changeWidth = 0.5)
    @Serializable(serializedName = "ReplyToQueueOverride")
    public TextField textfield_replyQueueOverride;
    @Responsive(changeWidth = 0.5, shiftHorizontally = 0.5)
    @Serializable(serializedName = "WaitTime")
    public TextField textfield_replyWaitTime;
    public AnchorPane anchorpane_reply;
    public String originalResponse;
    @Responsive(shiftHorizontally = 1, shiftVertically = 1)
    public Button button_prettyPrint;
    @Responsive(shiftHorizontally = 0.5)
    public Label label_waitTime;
    @Responsive(shiftVertically = 1)
    public Button button_selectXsd;
    @Responsive(shiftVertically = 1)
    public Button button_validate;
    @Responsive(shiftVertically = 1)
    public Label label_validationStatus;
    @Responsive(shiftVertically = 1)
    public Label label_validation;

    public static ReplyTabController getInstance() {
        return RequestTabController.getInstance().replyTabController;
    }

    public void initialize() {
        codearea_reply.setParagraphGraphicFactory(LineNumberFactory.get(codearea_reply));
        codearea_reply.setWrapText(true);
        codearea_reply.textProperty().addListener((obs, oldText, newText) -> {
            // codeArea cannot calculate highlighting for empty string
            if (!newText.isEmpty()) {
                codearea_reply.setStyleSpans(0, TextHighlighter.computeHighlighting(newText));
            }
        });
        vsPane = new VirtualizedScrollPane<>(codearea_reply);
        vsPane.setLayoutY(54);
        vsPane.setPrefSize(800,516);

        anchorpane_reply.getChildren().remove(codearea_reply);
        anchorpane_reply.getChildren().add(0, vsPane);
        codearea_reply.getStylesheets().add(getClass().getResource(TextHighlighter.CSS_FILE_PATH).toExternalForm());

        textfield_replyWaitTime.setTextFormatter(IntegerTextFormatter.getTextFormatterWithDefaultValue(60));
    }

    public void prettyPrintReply(ActionEvent actionEvent) {
        doPrettyPrint();
    }

    private void doPrettyPrint() {
        if (codearea_reply.getText().length() > 0) {
            codearea_reply.replaceText(UtilsManager.prettyPrint(codearea_reply.getText()));
            codearea_reply.setStyleSpans(0, TextHighlighter.computeHighlighting(codearea_reply.getText()));
        }
    }

    public void replyCheckerByCorrelId(byte[] CorrelId) {
        new Thread(() -> {
            try {
                String queueName = MqmdTabController.getInstance().textfield_replyToQueue.getText();
                if (textfield_replyQueueOverride.getText() != null && textfield_replyQueueOverride.getText().length()>0)
                    queueName = textfield_replyQueueOverride.getText();

                if (queueName == null || queueName.isEmpty()) {
                    MqTabController.getInstance().addLogMessage("No queue to get reply from. Set ReplyToQueue element in MQMD tab first.",true);
                    return;
                }

                MqTabController.getInstance().addLogMessage("Waiting " + textfield_replyWaitTime.getText() + "s for message in " + queueName + " with CorrelId " + Hex.encodeHexString(CorrelId) + "...", true);
                MQConnection mqConnection;
                if (MqmdTabController.getInstance().combobox_replyQmgr.getSelectionModel().getSelectedIndex() > -1)
                    mqConnection = new MQConnection(MqmdTabController.getInstance().getSelectedMqConnection());
                else
                    mqConnection = new MQConnection(MqTabController.getInstance().getSelectedMqConnection());
                String response = MQReader.replyCheckerByCorrelId(mqConnection, queueName, Integer.parseInt(textfield_replyWaitTime.getText()), CorrelId);

                if (response != null && !response.equals("")) {
                    originalResponse = response;

                    Platform.runLater(() -> codearea_reply.replaceText(UtilsManager.prettyPrint(response)));

                    MqTabController.getInstance().addLogMessage("Got message! Switching to reply tab.", true);
                    SingleSelectionModel<Tab> selectionModel = RequestTabController.getInstance().tabs.getSelectionModel();
                    selectionModel.select(RequestTabController.REPLY_TAB_POSITION);
                }
            } catch (MQException e) {
                MQDialogBox.openException(e);
            }
        }).start();
    }

    public void selectXsd(ActionEvent actionEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open XSD file...");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XSD file", "*.xsd"));
        // load last session folder path
        String lastSessionFolder = Main.configManager.getProperty("interface.lastfolder");
        if (lastSessionFolder != null) {
            File lastFolder = new File(lastSessionFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            try {
                String content = new Scanner(new File(file.getAbsolutePath())).useDelimiter("\\Z").next();
                RequestTabController.getInstance().sessionManager.setProperty("XsdContent", content);
                Main.configManager.setProperty("interface.lastfolder", file.getParent());
                label_validationStatus.setText(VALIDATION_READY);

            } catch (FileNotFoundException e) {
                MQDialogBox.openException(e);
            }
        }
    }

    public void validateXsd(ActionEvent actionEvent) {
        SessionWrapper sessionWrapper = RequestTabController.getInstance().sessionManager.session;

        if (sessionWrapper.getProperty("XsdContent").isPresent()) {
            try {
                SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
                Schema schema = factory.newSchema(new StreamSource(new StringReader(sessionWrapper.getStringProperty("XsdContent"))));
                Validator validator = schema.newValidator();

                String content = originalResponse;
                if (!Optional.ofNullable(content).isPresent()) {
                    MQDialogBox.openWarning("No data", "No response data for validation", "");
                    return;
                }
                if (content.contains("<soapenv:Body>")) {
                    content = content.replaceAll(".*<soapenv:Body>(.*?)<\\/soapenv:Body>.*", "$1");
                }
                if (content.contains("<SRVC_RES>")) {
                    content = content.replaceAll(".*<SRVC_RES>.*MetaHeader>(.*?)<\\/SRVC_RES>.*", "$1");
                }
                if (content.contains("XM")) {
                    content = content.replaceAll(".*<MD>(.*?)<\\/MD>.*", "$1");
                }

                Source source = new StreamSource(new StringReader(content));

                try {
                    validator.validate(source);
                    MQDialogBox.openInformation("Validation successful", "Validation of reply was successful :)", "");
                } catch (SAXException ex) {
                    System.out.println("Response is not valid because ");
                    MQDialogBox.openError("Validation failed", "Validation of reply was unsuccessful, because:", ex.getMessage());
                } catch (IOException e) {
                    MQDialogBox.openException(e);
                    e.printStackTrace();
                }

            } catch (SAXException e) {
                MQDialogBox.openError("Validation failed", "No XSD selected", "Please select valid XSD file to start validation");
                e.printStackTrace();
            }
        }
    }
}

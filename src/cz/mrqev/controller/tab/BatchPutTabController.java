package cz.mrqev.controller.tab;

import com.ibm.mq.MQException;
import com.ibm.mq.MQQueue;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.controller.IntegerTextFormatter;
import cz.mrqev.controller.window.MainController;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.json.JsonHolder;
import cz.mrqev.model.mq.actions.MQLoader;
import cz.mrqev.model.mq.wrapper.message.MQMessageTypeModel;
import cz.mrqev.model.mq.wrapper.transport.MQRequest;
import cz.mrqev.model.mq.actions.MQWriter;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.interfaces.annotation.Responsive;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class BatchPutTabController extends BaseTabController {

    // Batch Put
    public Label label_batchFilePath;
    @Serializable(serializedName = "BatchGenerateUidUnique", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_batchGenerateUidUnique;
    @Serializable(serializedName = "BatchFillTimestamps", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_batchFillTimestamps;
    @Serializable(serializedName = "BatchAddWss", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_batchAddWss;
    @Serializable(serializedName = "BatchGenerateUidSame", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_batchGenerateUidSame;
    @Responsive(shiftVertically = 0.5, changeHeight = 0.5, changeWidth = 1)
    public TextArea textarea_logBatch;
    @Responsive(changeWidth = 1, changeHeight = 0.5)
    public ListView listview_batchMessages;
    @Responsive(shiftHorizontally = 1)
    @Serializable(serializedName = "BatchWaitTime")
    public TextField textfield_batchWaitTime;
    @Responsive(shiftHorizontally = 1)
    public Button button_putAllMessages;
    @Responsive(changeWidth = 1)
    public Separator separator_batchPutOptions;
    @Responsive(changeWidth = 1)
    public Separator separator_importedMessages;
    @Responsive(shiftHorizontally = 1)
    public Label label_waitTime;
    @Responsive(shiftHorizontally = 1)
    public Label label_ms;
    @Responsive(shiftHorizontally = 1)
    public Label label_waitTimeDescription;

    private String batchPutLogFilename;

    // Data
    private ArrayList<MQMessageWrapper> messages = new ArrayList<>();

    private void refreshBatchPutLogFilename() {
        batchPutLogFilename = MainController.dataFileParentPath + "\\batchPut-" + System.currentTimeMillis() + ".log";
    }

    public static BatchPutTabController getInstance() {
        return RequestTabController.getInstance().batchPutTabController;
    }

    public void initialize() {
        listview_batchMessages.setPlaceholder(new Label("No messages imported"));
        addLogMessage("Batch put log",false);
        addLogMessage("----------------------------------------------------------------------",false);
        addLogMessage("For batch put select text file where one row means one message to put.\n",false);
        textfield_batchWaitTime.setTextFormatter(IntegerTextFormatter.getTextFormatterWithDefaultValue(100));
    }

    public void selectBatchFile(ActionEvent actionEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open batch file...");
        // set last initial directory
        if (MainController.dataFileParentPath != null) {
            fileChooser.setInitialDirectory(new File(MainController.dataFileParentPath));
        } else {
            fileChooser.setInitialDirectory(new File(MainController.applicationPath));
        }

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            try {
                String fileContent = UtilsManager.loadFromFile(file.getAbsolutePath());

                if (JsonHolder.isJson(fileContent)) {
                    messages = MQLoader.parseQueueExport(fileContent);
                } else {
                    messages = MQLoader.parseDataFile(fileContent);
                }

                listview_batchMessages.getItems().clear();
                listview_batchMessages.getItems().addAll(messages);
                label_batchFilePath.setText(file.getAbsolutePath());
                MainController.dataFileParentPath = file.getParent();

                addLogMessage("Data file was successfully imported and messages were prepared to put.", true);
            } catch (IOException e) {
                MQDialogBox.openException(e);
            }
        }
    }

    public void putAllMessages(ActionEvent actionEvent) {
        if (MqTabController.getInstance().combobox_queueManagers.getSelectionModel().getSelectedIndex() == -1) {
            this.addLogMessage("Error. No queue manager selected. Please select a QM you want to put message on. Use tab MQ to select.", true);
            return;
        }

        if (String.valueOf(MqTabController.getInstance().combobox_queues.getValue()).length() == 0 || MqTabController.getInstance().combobox_queues.getValue() == null) {
            this.addLogMessage("Error. No queue selected. Please select/input queue name you want your message to be put on. Use tab MQ to select.", true);
            return;
        }

        if (listview_batchMessages.getItems().size() == 0) {
            this.addLogMessage("Error. No messages to put. Please select new request file with at least one row (row means one message).", true);
            return;
        }

        if (((MQMessageTypeModel)MqmdTabController.getInstance().combobox_messageType.getSelectionModel().getSelectedItem()).getFlag() == 1 &&
                MqmdTabController.getInstance().textfield_replyToQueue.getText().isEmpty()) {
            this.addLogMessage("Please set ReplyToQueue when using message type Request.", true);
            return;
        }

        new Thread(() -> {
            MQConnection connection = null;
            MQQueue queue = null;

            // add sid placeholders
            try {
                addLogMessage("Batch put for " + listview_batchMessages.getItems().size() + " messages was started.", true);
                // initialize mq connection
                connection = new MQConnection(MqTabController.getInstance().getSelectedMqConnection());
                // access queue
                int openOptions = MQConstants.MQOO_OUTPUT + MQConstants.MQOO_FAIL_IF_QUIESCING;
                queue = connection.accessQueue(MqTabController.getInstance().getSelectedQueue(), openOptions);

                long waitTime = Long.parseLong(textfield_batchWaitTime.getText());

                // initialize mq writer and put message
                MQWriter writer = new MQWriter(queue, waitTime);
                setStopMessagePuttingButton(writer);
                writer.putInProgress = true;

                // get wss
                String wssUsername = String.valueOf(MqmdTabController.getInstance().combobox_wssUser.getValue());
                String wssPassword = MqmdTabController.getInstance().textfield_wssPassword.getText();
                // initial sid
                String initialSid = UtilsManager.generateSID();

                addLogMessage("Preparing messages for put (filling placeholders, wss and so on) started.", true);
                for (int i = 0; i < messages.size(); i++) {
                    if (!writer.putInProgress) break;

                    // create new request
                    MQRequest request;
                    // get actual message
                    MQMessageWrapper mqMessageWrapper = messages.get(i);
                    // fill sid placeholders
                    if (checkbox_batchGenerateUidUnique.isSelected() || checkbox_batchGenerateUidSame.isSelected()) {
                        mqMessageWrapper.setData(placeholderManager.getUidPlaceholder().fill(mqMessageWrapper.getData()));
                    }
                    // fill timestamp placeholders
                    if (checkbox_batchFillTimestamps.isSelected()) {
                        mqMessageWrapper.setData(placeholderManager.getTimestampPlaceholder().fill(mqMessageWrapper.getData()));
                    }
                    // prepare request
                    if (checkbox_batchAddWss.isSelected()) {
                        request = new MQRequest(mqMessageWrapper, checkbox_batchFillTimestamps.isSelected(), checkbox_batchGenerateUidUnique.isSelected(), wssUsername, wssPassword);
                    } else {
                        request = new MQRequest(mqMessageWrapper, checkbox_batchFillTimestamps.isSelected(), checkbox_batchGenerateUidUnique.isSelected());
                    }
                    // set same sid for all requests (is option is selected)
                    if (checkbox_batchGenerateUidSame.isSelected()) {
                        request.setSid(initialSid);
                    }

                    writer.addRequest(request);
                }
                addLogMessage("Messages are prepared for put.", true);
                addLogMessage("Batch put in progress, please wait ... ", true);

                refreshBatchPutLogFilename();
                addLogMessage("Actual progress you can see in log file " + batchPutLogFilename, true);
                writer.putAll(this::addLogMessageToFile);

                if (writer.putInProgress) {
                    addLogMessage("Batch put for " + listview_batchMessages.getItems().size() + " messages was successfully completed.", true);
                } else {
                    addLogMessage("Batch put was cancelled by user.", true);
                }

                writer.putInProgress = false;
                setPutAllMessagesButton();

                queue.close();
                connection.close();
            } catch (MQException e) {
                this.addLogMessage("Batch put unexpected ended with error: " + e.getMessage(), true);
            } catch (IOException e) {
                this.addLogMessage("Batch put unexpected ended. An MQ IO error occurred : " + e, true);
            } finally {
                setPutAllMessagesButton();
                try {
                    if (queue != null)
                        queue.close();
                    if (connection != null)
                        connection.close();
                } catch (MQException e) {
                    MQDialogBox.openException(e);
                }
            }
        }).start();
    }

    public void addLogMessage(String message, boolean withTime) {
        if (withTime)
            message = new SimpleDateFormat("HH:mm:ss").format(new Date()) + " - " + message;
        message += "\n";
        this.textarea_logBatch.appendText(message);
    }

    public void addLogMessage(String message) {
        addLogMessage(message, true);
    }

    public void addLogMessageToFile(String message) {
        try {
            UtilsManager.appendToFile(batchPutLogFilename, message + "\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setStopMessagePuttingButton(MQWriter writer) {
        // In JavaFX we cannot update UI component from different threat so we have to use runLater method
        Platform.runLater(() -> {
            button_putAllMessages.setText("Stop putting");
            button_putAllMessages.setOnAction((e)-> writer.putInProgress = false);
        });
    }

    private void setPutAllMessagesButton() {
        // In JavaFX we cannot update UI component from different threat so we have to use runLater method
        Platform.runLater(() -> {
            button_putAllMessages.setText("Put all messages");
            button_putAllMessages.setOnAction(this::putAllMessages);
        });
    }
}

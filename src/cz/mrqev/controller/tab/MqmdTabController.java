package cz.mrqev.controller.tab;

import com.ibm.mq.MQMessage;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.controller.IntegerTextFormatter;
import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.model.Consumer;
import cz.mrqev.interfaces.model.Consumers;
import cz.mrqev.model.mq.wrapper.message.MQMessageFormat;
import cz.mrqev.model.mq.wrapper.message.MQMessageType;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.manager.WSSManager;
import cz.mrqev.model.*;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.mq.wrapper.message.MQMessageFormatModel;
import cz.mrqev.model.mq.wrapper.message.MQMessageTypeModel;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import cz.mrqev.model.ws.wrapper.transport.HttpHeader;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.interfaces.annotation.Responsive;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

public class MqmdTabController extends BaseTabController {
    //MQMD / WSS
    @Responsive(changeWidth = 1, changeHeight = 0.5)
    public TitledPane titledPane_mqmd;
    @Responsive(changeWidth = 0.25, shiftHorizontally = 0.5)
    public TitledPane titeledPane_persistence;
    @Responsive(changeWidth = 0.25, shiftHorizontally = 0.75)
    public TitledPane titeledPane_reportOptions;
    @Responsive(changeWidth = 0.25, changeHeight = 0.5, shiftVertically = 0.5)
    public TitledPane titledPane_WSSecurity;
    public Label label_mqMessageFormat;
    @Responsive(shiftHorizontally = 0.125)
    public Label label_codePage;
    @Responsive(shiftHorizontally = 0.25)
    public Label label_messageType;
    @Responsive(shiftHorizontally = 0.375)
    public Label label_expiry;
    @Responsive(shiftHorizontally = 0.5)
    public Label label_replyToQM;
    @Responsive(changeWidth = 0.125, shiftHorizontally = 0.25)
    @Serializable(serializedName = "MessageType", setMethodParam = MQMessageTypeModel.class)
    public ComboBox combobox_messageType;
    @Responsive(changeWidth = 0.125)
    @Serializable(serializedName = "MQMessageFormat", setMethodParam = MQMessageFormatModel.class)
    public ComboBox combobox_messageFormat;
    @Serializable(serializedName = "PersistenceNo", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_persistenceNo;
    @Serializable(serializedName = "PersistenceYes", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_persistenceYes;
    @Serializable(serializedName = "PersistenceDefault", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_persistenceDefault;
    public ToggleGroup togglegroup_persistence = new ToggleGroup();
    @Serializable(serializedName = "PassCorrelId", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_passMessageId;
    @Serializable(serializedName = "PassMessageId", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_passCorrelId;
    @Responsive(changeWidth = 0.25)
    @Serializable(serializedName = "WSSUser")
    public ComboBox combobox_wssUser;
    @Responsive(changeWidth = 0.25)
    @Serializable(serializedName = "WSSPassword")
    public TextField textfield_wssPassword;
    public Label label_username;
    public Label label_password;
    @Responsive(changeWidth = 0.25)
    public Button button_setConsumers;

    // HTTP Header
    @Responsive(changeWidth = 0.75, changeHeight = 0.5, shiftHorizontally = 0.25, shiftVertically = 0.5)
    public TitledPane titledPane_HTTPHeader;
    @Responsive(changeWidth = 0.25, shiftVertically = 0.5)
    public Button button_addRequestProperty;
    @Responsive(changeWidth = 0.25, changeHeight = 0.5)
    public TextArea textarea_requestPropertyValue;
    @Responsive(changeWidth = 0.25)
    public ComboBox combobox_requestPropertyName;
    @Responsive(changeWidth = 0.5, changeHeight = 0.5, shiftHorizontally = 0.25)
    public ListView listView_requestProperties;
    public Label label_requestPropertyName;
    public Label label_requestPropertyValue;

    public HttpHeader httpHeader = new HttpHeader();

    //Data
    @Responsive(changeWidth = 0.125, shiftHorizontally = 0.125)
    @Serializable(serializedName = "Codepage")
    public TextField textfield_codepage;
    @Responsive(changeWidth = 0.125, shiftHorizontally = 0.375)
    @Serializable(serializedName = "Expiry")
    public TextField textfield_expiry;
    @Responsive(changeWidth = 0.5)
    @Serializable(serializedName = "ReplyToQueue")
    public TextField textfield_replyToQueue;
    @Responsive(changeWidth = 0.5, shiftHorizontally = 0.5)
    @Serializable(serializedName = "ReplyToQM")
    public ComboBox combobox_replyQmgr;
    @Responsive(changeWidth = 0.5)
    @Serializable(serializedName = "MessageId")
    public TextField textfield_messageId;
    @Responsive(changeWidth = 0.5)
    @Serializable(serializedName = "CorrelId")
    public TextField textfield_correlId;

    public static MqmdTabController getInstance() {
        return RequestTabController.getInstance().mqmdTabController;
    }

    public void initialize() {
        radiobutton_persistenceDefault.setToggleGroup(togglegroup_persistence);
        radiobutton_persistenceYes.setToggleGroup(togglegroup_persistence);
        radiobutton_persistenceNo.setToggleGroup(togglegroup_persistence);

        combobox_messageFormat.getItems().addAll(MQMessageFormat.formats);
        combobox_messageType.getItems().addAll(MQMessageType.types);
        combobox_messageFormat.getSelectionModel().select(MQMessageFormat.getFormatForFlag(MQConstants.MQFMT_STRING));
        combobox_messageType.getSelectionModel().select(MQMessageType.getTypeForFlag(MQConstants.MQMT_REQUEST));

        textfield_codepage.setTextFormatter(IntegerTextFormatter.getTextFormatterWithDefaultValue(1208));
        textfield_expiry.setTextFormatter(IntegerTextFormatter.getTextFormatterWithDefaultValueForExpiry(-1));

        listView_requestProperties.setPlaceholder(new Label("No additional properties in HTTP header."));

        // set context menu for http header view
        listView_requestProperties.setCellFactory( lv -> {
            ListCell<String> cell = new ListCell<>();
            final ContextMenu contextMenu = new ContextMenu();
            cell.setOnMouseClicked(mouseEvent -> {
                if (mouseEvent.getButton() == MouseButton.SECONDARY && !cell.isEmpty()) {
                    contextMenu.hide();
                    contextMenu.getItems().clear();

                    int splitPlace = cell.getText().indexOf(":");
                    String propertyName = cell.getText().substring(0, splitPlace);
                    String propertyValue = cell.getText().substring(splitPlace+2);

                    MenuItem editProperty = new MenuItem("Edit");
                    MenuItem removeProperty = new MenuItem("Remove");
                    contextMenu.getItems().addAll(editProperty, removeProperty);
                    editProperty.setOnAction(event -> {
                        combobox_requestPropertyName.getEditor().setText(propertyName);
                        textarea_requestPropertyValue.setText(propertyValue);
                    });
                    removeProperty.setOnAction(event -> {
                        httpHeader.removeProperty(propertyName);
                        refreshHttpHeaderProperties();
                    });

                    contextMenu.show(cell, mouseEvent.getScreenX(), mouseEvent.getScreenY());
                }
            });

            cell.textProperty().bind(cell.itemProperty());

            return cell ;
        });
        // necessary http header property
        httpHeader.saveProperty("Content-Type","application/x-www-form-urlencoded");
        refreshHttpHeaderProperties();
        // initialize combobox popup with default properties
        combobox_requestPropertyName.getItems().addAll(HttpHeader.getDefaultPropertyList().keySet());
    }

    public void combobox_wssuser_change(ActionEvent actionEvent) {
        if (combobox_wssUser.getSelectionModel().getSelectedIndex() >= 0) {
            Consumers consumers = MainController.getInstance().consumers;
            Consumer consumer = consumers.getConsumerByWssUsername(String.valueOf(combobox_wssUser.getValue()));
            textfield_wssPassword.setText(consumer.getWssPassword());
            if (consumer.getHttpEndpointAlias() != null && WsTabController.getInstance().textfield_webServiceUrl.getText().isEmpty()) {
                WsTabController.getInstance().textfield_webServiceUrl.setText(consumer.getCallableEndpoint(true));
            }
        }
    }

    public MQConnectionEntry getSelectedMqConnection() {
        return Main.configManager.MQConnections.get(combobox_replyQmgr.getSelectionModel().getSelectedIndex());
    }

    public void importConsumers(ActionEvent actionEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open consumers file...");
        String extensionFilter = MainController.getInstance().consumerFileExtFilter;
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter(extensionFilter, extensionFilter));

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            Main.configManager.setProperty("interface.consumers", file.getAbsolutePath());

            combobox_wssUser.getItems().addAll(MainController.getInstance().getWssNames(true));
        }
    }

    public void addLogMessage(String message, boolean withTime) {
        if (withTime)
            message = new SimpleDateFormat("HH:mm:ss").format(new Date()) + " - " + message;
        message += "\n";
        MqTabController.getInstance().textarea_logMQ.appendText(message);
    }

    public MqmdWrapper wrapMqmd() {
        MQMessage mqMessage = new MQMessage();

        mqMessage.format = ((MQMessageFormatModel)combobox_messageFormat.getSelectionModel().getSelectedItem()).getFlag();
        mqMessage.feedback = MQConstants.MQFB_NONE;
        mqMessage.messageType = ((MQMessageTypeModel)combobox_messageType.getSelectionModel().getSelectedItem()).getFlag();
        mqMessage.characterSet = Integer.parseInt(textfield_codepage.getText());
        mqMessage.expiry = Integer.parseInt(textfield_expiry.getText());

        if (!textfield_replyToQueue.getText().equals("")) {
            mqMessage.replyToQueueName = textfield_replyToQueue.getText();
        }

        if (combobox_replyQmgr.getSelectionModel().getSelectedIndex() > -1) {
            mqMessage.replyToQueueManagerName = Main.configManager.MQConnections.get(combobox_replyQmgr.getSelectionModel().getSelectedIndex()).getQueueManager();
        }

        if (!textfield_messageId.getText().equals("")) {
            mqMessage.messageId = textfield_messageId.getText().getBytes();
        } else {
            mqMessage.messageId = MQConstants.MQMI_NONE;
        }

        if (!textfield_correlId.getText().equals("")) {
            mqMessage.correlationId = textfield_correlId.getText().getBytes();
        } else {
            mqMessage.correlationId = MQConstants.MQCI_NONE;
        }

        if (radiobutton_persistenceNo.isSelected()) {
            mqMessage.persistence = MQConstants.MQPER_NOT_PERSISTENT;
        } else if (radiobutton_persistenceYes.isSelected()) {
            mqMessage.persistence = MQConstants.MQPER_PERSISTENT;
        } else if (radiobutton_persistenceDefault.isSelected()) {
            mqMessage.persistence = MQConstants.MQPER_PERSISTENCE_AS_Q_DEF;
        }

        if (checkbox_passCorrelId.isSelected()) {
            mqMessage.report = mqMessage.report | MQConstants.MQRO_PASS_CORREL_ID;
        }

        if (checkbox_passMessageId.isSelected()) {
            mqMessage.report = mqMessage.report | MQConstants.MQRO_PASS_MSG_ID;
        }

        return new MqmdWrapper(mqMessage);
    }

    public void saveRequestProperty() {
        String name = combobox_requestPropertyName.getEditor().getText();
        String value = textarea_requestPropertyValue.getText();
        if (name != null && value != null && !name.isEmpty()) {
            httpHeader.saveProperty(name, value);

            refreshHttpHeaderProperties();
        }

    }

    public void refreshHttpHeaderProperties() {
        listView_requestProperties.getItems().clear();
        listView_requestProperties.getItems().addAll(httpHeader.getPropertiesArray());
        combobox_requestPropertyName.getEditor().setText("");
        textarea_requestPropertyValue.setText("");
    }

    public void combobox_requestPropertyName_change(ActionEvent actionEvent) {
        if (combobox_requestPropertyName.getSelectionModel().getSelectedIndex() >= 0) {
            if (httpHeader.containsKey(combobox_requestPropertyName.getValue())) {
                textarea_requestPropertyValue.setText(httpHeader.getProperty(combobox_requestPropertyName.getValue()));
            } else {
                textarea_requestPropertyValue.setText(HttpHeader.getDefaultPropertyList().get(combobox_requestPropertyName.getValue()));
            }
        }
    }
}
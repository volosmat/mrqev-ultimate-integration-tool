package cz.mrqev.controller.tab;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.TabController;
import cz.mrqev.interfaces.annotation.Responsive;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.interfaces.model.Consumer;
import cz.mrqev.interfaces.model.Consumers;
import cz.mrqev.main.Main;

import cz.mrqev.manager.UtilsManager;
import cz.mrqev.view.MQNode;
import javafx.scene.Node;
import javafx.scene.control.*;
import cz.mrqev.manager.WSSManager;
import javafx.scene.layout.Region;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class RequestTabController extends BaseTabController {
    public final static int DATA_TAB_POSITION = 2;
    public final static int REPLY_TAB_POSITION = 3;
    public final static int LOG_TAB_POSITION = 4;
    public final static int MQMD_TAB_POSITION = 5;
    public final static int BATCH_PUT_TAB_POSITION = 6;

    public Tab tab_reply;
    public Tab tab_log;
    // upper tabs (MQ, WS, Data ... )
    public TabPane tabs;

    // TabControllers imported from plugin
    private HashMap<String, TabController>  pluginTabControllers = new HashMap<>();
    // TabControllers from application core
    public BatchPutTabController batchPutTabController;
    public MqmdTabController mqmdTabController;
    public ReplyTabController replyTabController;
    public DataTabController dataTabController;
    public MqTabController mqTabController;
    public WsTabController wsTabController;

    /**
     * Get instance of actual selected request tab controller
     * @return
     */
    public static RequestTabController getInstance() {
        return MainController.getSelectedTabController();
    }

    public void initialize() {
        MainController.addTabController(this);

        // create listeners
        listenerManager.createMqGenerateUidListener();
        listenerManager.createMqFillTimestampsListener();
        listenerManager.createWsGenerateUidListener();
        listenerManager.createWsFillTimestampsListener();
        listenerManager.createAddPlaceholdersInDataAfterCopyListener();
        listenerManager.createQueueManagerChangedListener();
        listenerManager.createGenerateUidWhenGetLogSelectedListener();
        listenerManager.createAddMetaheaderToHttpHeaderForRestListener();

    }

    public void addPluginTabController(TabController tabController) {
        pluginTabControllers.put(tabController.getTabName(), tabController);
    }

    public TabController getPluginTabController(String tabName) {
        return pluginTabControllers.get(tabName);
    }

    /**
     * Get all annotated nodes from tabs (MQ, WS etc.) for this request tab
     * @param annotation
     * @return MQNode
     */
    public List<MQNode> getNodes(Class annotation) {
        List<MQNode> result = new ArrayList<>();

        result.addAll(UtilsManager.getAnnotatedNodes(mqTabController, annotation));
        result.addAll(UtilsManager.getAnnotatedNodes(wsTabController, annotation));
        result.addAll(UtilsManager.getAnnotatedNodes(replyTabController, annotation));
        result.addAll(UtilsManager.getAnnotatedNodes(dataTabController, annotation));
        result.addAll(UtilsManager.getAnnotatedNodes(mqmdTabController, annotation));
        result.addAll(UtilsManager.getAnnotatedNodes(batchPutTabController, annotation));
        // get nodes for plugin controllers
        pluginTabControllers.forEach((key, value) -> result.addAll(UtilsManager.getAnnotatedNodes(value, annotation)));

        return result;
    }

    public void refreshNodes() {
        if (placeholderManager.getUidPlaceholder() != null) {
            mqTabController.checkbox_MQ_copyUIDToClipboard.setDisable(false);
            mqTabController.checkbox_MQ_generateUID.setDisable(false);
            mqTabController.radiobutton_getLog.setDisable(false);
            wsTabController.checkbox_WS_generateUID.setDisable(false);
            wsTabController.checkbox_WS_copyUIDToClipboard.setDisable(false);
            wsTabController.radiobutton_getLog.setDisable(false);
            batchPutTabController.checkbox_batchGenerateUidUnique.setDisable(false);
            batchPutTabController.checkbox_batchGenerateUidSame.setDisable(false);

            mqTabController.checkbox_MQ_generateUID.setText("Generate " + placeholderManager.getUidPlaceholder().getName() + " (?)");
            mqTabController.checkbox_MQ_copyUIDToClipboard.setText("Copy " + placeholderManager.getUidPlaceholder().getName() + " to clipboard");
            wsTabController.checkbox_WS_generateUID.setText("Generate " + placeholderManager.getUidPlaceholder().getName());
            wsTabController.checkbox_WS_copyUIDToClipboard.setText("Copy " + placeholderManager.getUidPlaceholder().getName() + " to clipboard");
            batchPutTabController.checkbox_batchGenerateUidSame.setText("Generate unique " + placeholderManager.getUidPlaceholder().getName() + " for every message");
            batchPutTabController.checkbox_batchGenerateUidUnique.setText("Generate one " + placeholderManager.getUidPlaceholder().getName() + " for all messages");
        }

        if (placeholderManager.getTimestampPlaceholder() != null) {
            mqTabController.checkbox_MQ_fillTimestamps.setDisable(false);
            wsTabController.checkbox_WS_fillTimestamps.setDisable(false);
            batchPutTabController.checkbox_batchFillTimestamps.setDisable(false);
        }

        // load consumers
        if (MainController.getInstance().consumerFileExtFilter != null && MainController.getInstance().consumersImportFunction != null) {
            mqmdTabController.button_setConsumers.setDisable(false);

            mqmdTabController.combobox_wssUser.getItems().addAll(MainController.getInstance().getWssNames(false));
        }

        dataTabController.refreshTemplatesMenu();
        mqTabController.refreshCustomPlaceholdersList();
        listenerManager.createDataCodeAreaListeners();
        listenerManager.createSearchInComboboxListener();
    }
}
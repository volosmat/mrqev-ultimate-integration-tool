package cz.mrqev.controller.tab;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.LogTabControllerInterface;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.ws.actions.WSWriterReader;
import cz.mrqev.model.ws.wrapper.transport.WSRequest;
import cz.mrqev.model.ws.wrapper.transport.WSResponse;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.utils.timer.Timer;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.view.MQWindow;
import cz.mrqev.interfaces.annotation.Responsive;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

public class WsTabController extends BaseTabController {
    // WS
    @Responsive(changeWidth = 1)
    @Serializable(serializedName = "WebServiceUrl")
    public TextField textfield_webServiceUrl;
    @Responsive(changeWidth = 1, changeHeight = 1)
    public TextArea textarea_logWS;
    @Serializable(serializedName = "WSFillTimestamps", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_WS_fillTimestamps;
    @Serializable(serializedName = "WSFillUID", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_WS_generateUID;
    @Serializable(serializedName = "WSCopyUIDToClipboard", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_WS_copyUIDToClipboard;
    @Responsive(shiftHorizontally = 1)
    public Button button_callWebService;
    @Responsive(shiftHorizontally = 1)
    public Button button_importFromSoapUi;
    @Serializable(serializedName = "WSGetReply", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_getReply;
    @Serializable(serializedName = "WSGetLog", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_getLog;
    @Responsive(changeWidth = 1)
    public Separator separator_log;
    @Responsive(changeWidth = 1)
    public Separator separator_protocol;
    @Responsive(changeWidth = 1)
    public Separator separator_options;
    @Serializable(serializedName = "WSSoap", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_httpSoap;
    @Serializable(serializedName = "WSRest", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_httpRest;
    @Serializable(serializedName = "WSAddWss", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_WS_addWSS;
    @Responsive(shiftHorizontally = 1)
    public Button button_exportToSoapUi;

    public static WsTabController getInstance() {
        return RequestTabController.getInstance().wsTabController;
    }

    @Override
    public void initialize() {

    }

    public void callWebService(ActionEvent actionEvent) {
        if (!Pattern.compile("http(s|):\\/\\/(.*?)\\/.*").matcher(textfield_webServiceUrl.getText()).find()) {
            MQDialogBox.openError("Error", "Invalid URL", "URL must be formatted as http(s)://alias/*");
            return;
        }

        if (DataTabController.getInstance().codearea_requestData.getText().length() == 0) {
            MQDialogBox.openError("Error", "No request data", "There is no request present in Data tab");
            return;
        }

        if (ReplyTabController.getInstance().textfield_replyWaitTime.getText().length() == 0) {
            MQDialogBox.openError("Error", "No reply wait time", "Please enter maximum wait time for response/connecting in Reply tab");
            return;
        }

        WSRequest wsrequest = prepareRequest();

        addLogMessage("Calling message with"+ (checkbox_WS_generateUID.isSelected()?(" SID "+wsrequest.getSid()):"") +" length "
                + DataTabController.getInstance().codearea_requestData.getText().length()+ " at "+ textfield_webServiceUrl.getText(), true);

        button_callWebService.setDisable(true);

        new Thread(() -> {
            try {
                URL url = new URL(textfield_webServiceUrl.getText());
                WSWriterReader wswr = new WSWriterReader(url, wsrequest);

                // get log
                if (radiobutton_getLog.isSelected() && pluginManager.checkClassInterface(LogTabControllerInterface.class)) {
                    // get plugin log tab name
                    String logTabControllerName = ((LogTabControllerInterface)pluginManager.getClassInstanceByInterface(LogTabControllerInterface.class)).getTabName();
                    // get plugin log tab controller interface from request tab controller
                    LogTabControllerInterface logTabController = (LogTabControllerInterface) RequestTabController.getInstance().getPluginTabController(logTabControllerName);

                    new Timer(3, 1000, Timer.DESC, (actualCounter, stopTimer) -> {
                        if (actualCounter == 0) {
                            Platform.runLater(() -> button_callWebService.setDisable(false));
                            RequestTabController.getInstance().tabs.getSelectionModel().select(RequestTabController.LOG_TAB_POSITION);
                            logTabController.logCheckerBySid(wsrequest.getSid(), 10, 3000);
                        } else {
                            logTabController.stopLoading(null);
                            Platform.runLater(() -> addLogMessage("switching to log tab in " + actualCounter + " seconds.", true));
                        }
                    });
                }

                wswr.callWebService();

                // copy sid to clipboard
                if (checkbox_WS_copyUIDToClipboard.isSelected()) {
                    Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                    clpbrd.setContents(new StringSelection(wsrequest.getSid()), null);
                }

                // get reply
                if (radiobutton_getReply.isSelected()) {
                    processResponse(wswr.getResponse());
                }
            } catch (Exception e) {
                button_callWebService.setDisable(false);
                MQDialogBox.openException(e);
            }
        }).start();
    }

    private void processResponse(WSResponse response) {
        // set reply codeArea content
        Platform.runLater(() -> {
            ReplyTabController.getInstance().codearea_reply.replaceText("");
            ReplyTabController.getInstance().originalResponse = response.getResponseData();
            ReplyTabController.getInstance().codearea_reply.replaceText(UtilsManager.prettyPrint(response.getResponseData()));
        });

        if (String.valueOf(response.getWebServiceResponseCode()).startsWith("4") || String.valueOf(response.getWebServiceResponseCode()).startsWith("5")) {
            addLogMessage("Got reply with error code " + response.getWebServiceResponseCode() + "! Switching to reply tab.", true);
        } else {
            addLogMessage("Got reply! Switching to reply tab.", true);
        }
        Platform.runLater(() -> button_callWebService.setDisable(false));
        // select reply tab
        SingleSelectionModel<Tab> selectionModel = RequestTabController.getInstance().tabs.getSelectionModel();
        selectionModel.select(RequestTabController.REPLY_TAB_POSITION);
    }

    public void importFromSoapUI(ActionEvent actionEvent) {
        new MQWindow("window/testcase", "Select testcase from SoapUI project", 600, 400, false, null, false, false, true);
    }

    public Void addLogMessage(String message, Boolean withTime) {
        if (withTime)
            message = new SimpleDateFormat("HH:mm:ss").format(new Date()) + " - " + message;
        message += "\n";
        this.textarea_logWS.appendText(message);

        return null;
    }

    public Void addLogMessage(String message) {
        addLogMessage(message, true);

        return null;
    }

    private WSRequest prepareRequest() {
        if (checkbox_WS_addWSS.isSelected()) {
            return new WSRequest(
                    DataTabController.getInstance().codearea_requestData.getText(),
                    checkbox_WS_fillTimestamps.isSelected(),
                    checkbox_WS_generateUID.isSelected(),
                    radiobutton_httpSoap.isSelected(),
                    String.valueOf(MqmdTabController.getInstance().combobox_wssUser.getValue()),
                    MqmdTabController.getInstance().textfield_wssPassword.getText(),
                    MqmdTabController.getInstance().httpHeader);
        } else {
            return new WSRequest(
                    DataTabController.getInstance().codearea_requestData.getText(),
                    checkbox_WS_fillTimestamps.isSelected(),
                    checkbox_WS_generateUID.isSelected(),
                    radiobutton_httpSoap.isSelected(),
                    MqmdTabController.getInstance().httpHeader);
        }
    }

    public void exportToSoapUI(ActionEvent event) {
        File file;

        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("SoapUI project", "*.xml"));
        fileChooser.setTitle("Save SoapUI file...");

        // load last session folder path
        String lastSessionFolder = Main.configManager.getProperty("interface.lastsessionfolder");
        if (lastSessionFolder != null) {
            File lastFolder = new File(lastSessionFolder);
            if (lastFolder.exists())
                fileChooser.setInitialDirectory(lastFolder);
        }

        file = fileChooser.showSaveDialog(stage);

        if (file != null) {

            String request = DataTabController.getInstance().codearea_requestData.getText();
            String tabName = MainController.getRequestTabControllerName(MainController.getSelectedTabController());
            String wssUsername = String.valueOf(MqmdTabController.getInstance().combobox_wssUser.getValue());
            String wssPassword = MqmdTabController.getInstance().textfield_wssPassword.getText();
            String endpoint = WsTabController.getInstance().textfield_webServiceUrl.getText();

            String soapUiProject = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<con:soapui-project activeEnvironment=\"Default\" name=\"MrQev session - " + tabName + "\" resourceRoot=\"\" soapui-version=\"5.3.0\" abortOnError=\"false\" runType=\"SEQUENTIAL\" id=\"c70502ce-5023-413b-9e23-b39e6cf094c2\" xmlns:con=\"http://eviware.com/soapui/config\">\n" +
                    "<con:settings/>\n" +
                    "<con:interface xsi:type=\"con:WsdlInterface\" wsaVersion=\"NONE\" name=\"DummyBinding\" type=\"wsdl\" bindingName=\"{MrQev}DummyBinding\" soapVersion=\"1_1\" anonymous=\"optional\" definition=\"file:/c:/dummybinding.wsdl\" id=\"3437fed1-4200-49aa-ac09-67c6301e585b\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                    "<con:settings/>\n" +
                    "<con:definitionCache type=\"TEXT\" rootPart=\"file:/c:/dummybinding.wsdl\">\n" +
                    "<con:part>\n" +
                    "<con:url>file:/c:/dummybinding.wsdl</con:url>\n" +
                    "<con:content>\n" +
                    "<![CDATA[<definitions name=\"Dummy\" targetNamespace=\"MrQev\" xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\" xmlns=\"http://schemas.xmlsoap.org/wsdl/\" xmlns:tns=\"MrQev\">\n" +
                    "<message name=\"DummyReq\">\n" +
                    "<part name=\"DummyReq\" element=\"DummyReq\"/>\n" +
                    "</message>\n" +
                    "<message name=\"DummyRes\">\n" +
                    "<part name=\"DummyRes\" element=\"DummyRes\"/>\n" +
                    "</message>\n" +
                    "<message name=\"DummyFault\">\n" +
                    "<part name=\"DummyFault\" element=\"DummyFault\"/>\n" +
                    "</message>\n" +
                    "<portType name=\"DummyPort\">\n" +
                    "<operation name=\"DummyOperation\">\n" +
                    "<input name=\"DummyReq\" message=\"tns:DummyReq\"/>\n" +
                    "<output name=\"DummyRes\" message=\"tns:DummyRes\"/>\n" +
                    "<fault name=\"DummyFault\" message=\"tns:DummyFault\"/>\n" +
                    "</operation>\n" +
                    "</portType>\n" +
                    "<binding name=\"DummyBinding\" type=\"tns:DummyPort\">\n" +
                    "<soap:binding style=\"document\" transport=\"http://schemas.xmlsoap.org/soap/http\"/>\n" +
                    "<operation name=\"DummyOperation\">\n" +
                    "<soap:operation style=\"document\" soapAction=\"\"/>\n" +
                    "<input name=\"DummyReq\">\n" +
                    "<soap:body use=\"literal\"/>\n" +
                    "</input>\n" +
                    "<output name=\"DummyRes\">\n" +
                    "<soap:body use=\"literal\"/>\n" +
                    "</output>\n" +
                    "<fault name=\"DummyFault\">\n" +
                    "<soap:fault use=\"literal\" name=\"DummyFault\"/>\n" +
                    "</fault>\n" +
                    "</operation>\n" +
                    "</binding>\n" +
                    "<service name=\"Dummy\">\n" +
                    "<port name=\"DummyPort\" binding=\"tns:DummyBinding\">\n" +
                    "<soap:address location=\"https://${hostname}:${port}/\"/>\n" +
                    "</port>\n" +
                    "</service>\n" +
                    "</definitions>]]>\n" +
                    "</con:content>\n" +
                    "<con:type>http://schemas.xmlsoap.org/wsdl/</con:type>\n" +
                    "</con:part>\n" +
                    "<con:part>\n" +
                    "<con:url>file:/c:/dummyschema.xsd</con:url>\n" +
                    "<con:content>&lt;xs:schema targetNamespace=\"MrQev\" elementFormDefault=\"qualified\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">&lt;/xs:schema></con:content>\n" +
                    "<con:type>http://www.w3.org/2001/XMLSchema</con:type>\n" +
                    "</con:part>\n" +
                    "</con:definitionCache>\n" +
                    "<con:endpoints>\n" +
                    "<con:endpoint>https://${hostname}:${port}/</con:endpoint>\n" +
                    "</con:endpoints>\n" +
                    "<con:operation isOneWay=\"false\" action=\"http://www.csob.cz/\" name=\"DummyService\" bindingOperationName=\"DummyOperation\" type=\"Request-Response\" outputName=\"DummyRes\" inputName=\"DummyReq\" receivesAttachments=\"false\" sendsAttachments=\"false\" anonymous=\"optional\" id=\"08047383-5309-4f8c-99d9-effd2cdd88a1\">\n" +
                    "<con:settings/>\n" +
                    "</con:operation>\n" +
                    "</con:interface>\n" +
                    "<con:testSuite name=\"Testsuite\" id=\"9b0a6f26-37a3-4a7f-82c3-8ae187f9ec4f\">\n" +
                    "<con:settings/>\n" +
                    "<con:runType>SEQUENTIAL</con:runType>\n" +
                    "<con:testCase failOnError=\"true\" failTestCaseOnErrors=\"true\" keepSession=\"false\" maxResults=\"0\" name=\"Testcase\" searchProperties=\"true\" id=\"83023261-aa63-4598-9f5c-96e1d84a6f17\">\n" +
                    "<con:settings/>\n" +
                    "<con:testStep type=\"groovy\" name=\"prepare-variables-and-call\" id=\"84b38232-9eb3-4cd7-a7a0-a2fc2b1c8b15\">\n" +
                    "<con:settings/>\n" +
                    "<con:config>\n" +
                    "<script>def currentTM = new java.util.Date();\n" +
                    "\n" +
                    "// generate SID - 32 chars\n" +
                    "def propertySID = \"TestSoapUIx\" + (new java.text.SimpleDateFormat(\"yyyyMMddHHmmss\")).format(currentTM) + \"x\";\n" +
                    "def retRnd = String.valueOf(Math.round(Math.random()*999999))\n" +
                    "def count = 32 - propertySID.length() - retRnd.length()\n" +
                    "for(i=0; i&lt;count; i++)  propertySID += '0';\n" +
                    "propertySID += retRnd\n" +
                    "testRunner.getTestCase().setPropertyValue(\"SID\", propertySID)\n" +
                    "\n" +
                    "//generate CreateDataTime\n" +
                    "// example: 2001-12-17T09:30:47.000\n" +
                    "def propertyCDT =  (new java.text.SimpleDateFormat(\"yyyy-MM-dd'T'HH:mm:ss.SSS\")).format(currentTM)\n" +
                    "testRunner.getTestCase().setPropertyValue(\"CreateDateTime\", propertyCDT)</script>\n" +
                    "</con:config>\n" +
                    "</con:testStep>\n" +
                    "<con:testStep type=\"request\" name=\"Request\" id=\"b3d53ad6-6025-4468-a4e1-81b5f5dc2e5f\">\n" +
                    "<con:settings/>\n" +
                    "<con:config xsi:type=\"con:RequestStep\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" +
                    "<con:interface>DummyBinding</con:interface>\n" +
                    "<con:operation>DummyService</con:operation>\n" +
                    "<con:request name=\"Request\" outgoingWss=\"security\" id=\"1a334019-b236-4603-8d35-bb18f48ba8fc\">\n" +
                    "<con:settings>\n" +
                    "<con:setting id=\"com.eviware.soapui.impl.wsdl.WsdlRequest@request-headers\">&lt;xml-fragment/></con:setting>\n" +
                    "</con:settings>\n" +
                    "<con:encoding>UTF-8</con:encoding>\n" +
                    "<con:endpoint>" + endpoint + "</con:endpoint>\n" +
                    "<con:request>\n" +
                    "<![CDATA[" + request + "]]>\n" +
                    "</con:request>\n" +
                    "<con:assertion type=\"SOAP Response\" id=\"7926048c-21b9-4d4d-9dc1-c49d459b129c\"/>\n" +
                    "<con:assertion type=\"SOAP Fault Assertion\" name=\"Not SOAP Fault\" id=\"12a7a336-ac63-4c4f-aeeb-680d642dc121\"/>\n" +
                    "<con:credentials>\n" +
                    "<con:selectedAuthProfile>Basic</con:selectedAuthProfile>\n" +
                    "<con:addedBasicAuthenticationTypes>Basic</con:addedBasicAuthenticationTypes>\n" +
                    "<con:authType>Global HTTP Settings</con:authType>\n" +
                    "</con:credentials>\n" +
                    "<con:jmsConfig JMSDeliveryMode=\"PERSISTENT\"/>\n" +
                    "<con:jmsPropertyConfig/>\n" +
                    "<con:wsaConfig mustUnderstand=\"NONE\" version=\"200508\"/>\n" +
                    "<con:wsrmConfig version=\"1.2\"/>\n" +
                    "</con:request>\n" +
                    "</con:config>\n" +
                    "</con:testStep>\n" +
                    "<con:properties>\n" +
                    "<con:property>\n" +
                    "<con:name>SID</con:name>\n" +
                    "<con:value>TestSoapUIx20150914114239x556030</con:value>\n" +
                    "</con:property>\n" +
                    "<con:property>\n" +
                    "<con:name>trSID</con:name>\n" +
                    "<con:value>0103144x00030334</con:value>\n" +
                    "</con:property>\n" +
                    "<con:property>\n" +
                    "<con:name>CreateDateTime</con:name>\n" +
                    "<con:value>2015-09-14T11:42:39.18</con:value>\n" +
                    "</con:property>\n" +
                    "</con:properties>\n" +
                    "<con:reportParameters/>\n" +
                    "</con:testCase>\n" +
                    "<con:properties/>\n" +
                    "</con:testSuite>\n" +
                    "<con:requirements/>\n" +
                    "<con:properties/>\n" +
                    "<con:wssContainer>\n" +
                    "<con:outgoing>\n" +
                    "<con:name>security</con:name>\n" +
                    "<con:entry type=\"Username\" username=\"" + wssUsername + "\" password=\"" + wssPassword + "\">\n" +
                    "<con:configuration>\n" +
                    "<addCreated>true</addCreated>\n" +
                    "<addNonce>true</addNonce>\n" +
                    "<passwordType>PasswordDigest</passwordType>\n" +
                    "</con:configuration>\n" +
                    "</con:entry>\n" +
                    "</con:outgoing>\n" +
                    "</con:wssContainer>\n" +
                    "<con:databaseConnectionContainer/>\n" +
                    "<con:oAuth2ProfileContainer/>\n" +
                    "<con:oAuth1ProfileContainer/>\n" +
                    "<con:reporting>\n" +
                    "<con:xmlTemplates/>\n" +
                    "<con:parameters/>\n" +
                    "</con:reporting>\n" +
                    "<con:sensitiveInformation/>\n" +
                    "</con:soapui-project>";

            soapUiProject = soapUiProject.replace("##SID##", "${#TestCase#SID}");
            soapUiProject = soapUiProject.replace("##CURRENT_TIMESTAMP##", "${#TestCase#CreateDateTime}");

            try {
                UtilsManager.storeToFile(file.getAbsolutePath(), soapUiProject);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

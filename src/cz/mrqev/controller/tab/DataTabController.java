package cz.mrqev.controller.tab;

import cz.mrqev.api.ViewApiImpl;
import cz.mrqev.exception.TemplateGroupNotFoundException;
import cz.mrqev.interfaces.api.ViewApi;
import cz.mrqev.main.DefaultConfiguration;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.view.MQWindow;
import cz.mrqev.view.TextHighlighter;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.interfaces.annotation.Responsive;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.*;

public class DataTabController extends BaseTabController {
    //Data
    @Responsive(shiftVertically = 1)
    public Button button_openTemplateMenuButton;
    @Responsive(changeWidth = 1, changeHeight = 1)
    public VirtualizedScrollPane<CodeArea> vsPane;
    @Responsive(changeWidth = 1, changeHeight = 1)
    @Serializable(serializedName = "Data", setMethod = "replaceText")
    public CodeArea codearea_requestData;
    public AnchorPane anchorpane_data;
    @Responsive(shiftHorizontally = 1, shiftVertically = 1)
    public Button button_prettyPrint;
    @Responsive(shiftHorizontally = 0.5, shiftVertically = 1)
    public Button button_openRequest;
    @Responsive(shiftHorizontally = 0.5, shiftVertically = 1)
    public Button button_saveRequest;
    @Responsive(shiftHorizontally = 0.5, shiftVertically = 1)
    public Button button_generateRequest;

    // Template menu
    private static ContextMenu contextMenu_templates = new ContextMenu();
    public static Map<MenuItem, List<MenuItem>> contextMenuCache = new HashMap<>();

    public static DataTabController getInstance() {
        return RequestTabController.getInstance().dataTabController;
    }

    public void initialize() {
        codearea_requestData.setParagraphGraphicFactory(LineNumberFactory.get(codearea_requestData));
        codearea_requestData.setWrapText(true);

        vsPane = new VirtualizedScrollPane<>(codearea_requestData);
        vsPane.setPrefWidth(800);
        vsPane.setPrefHeight(590);

        anchorpane_data.getChildren().remove(codearea_requestData);
        anchorpane_data.getChildren().add(0, vsPane);
        codearea_requestData.getStylesheets().add(getClass().getResource(TextHighlighter.CSS_FILE_PATH).toExternalForm());

        if (contextMenuCache.isEmpty())
            createSoapTemplate();

        // show template menu
        button_openTemplateMenuButton.addEventHandler(MouseEvent.MOUSE_CLICKED, event -> {
            if (event.getButton() == MouseButton.PRIMARY)
                contextMenu_templates.show(button_openTemplateMenuButton, event.getScreenX(), event.getScreenY());
        });
    }

    private void createSoapTemplate() {
        MenuItem groupItem = new MenuItem("Message types");
        groupItem.setDisable(true);
        groupItem.getStyleClass().add("context-menu-title");
        MenuItem soapEnvelope = new MenuItem("SOAP Envelope");
        soapEnvelope.setOnAction(event -> templateManager.addSoapTemplate());
        List<MenuItem> template = new ArrayList<>();
        template.add(soapEnvelope);
        DataTabController.contextMenuCache.put(groupItem, template);
    }

    public void refreshTemplatesMenu() {
        contextMenu_templates.getItems().clear();
        contextMenuCache.forEach((key, value) -> {
            if (!contextMenu_templates.getItems().contains(key)) {
                // add new template group if not exists
                contextMenu_templates.getItems().add(key);
            }
            // add all values to actual template group
            contextMenu_templates.getItems().addAll(value);
        });
    }

    public void openFile(ActionEvent actionEvent) {
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open request file...");

        File f = new File(Main.configManager.getProperty("interface.lastfolder"));
        if (f.exists())
            fileChooser.setInitialDirectory(f);

        File file = fileChooser.showOpenDialog(stage);

        if (file != null) {
            try {
                String content = new Scanner(new File(file.getAbsolutePath())).useDelimiter("\\Z").next();
                codearea_requestData.replaceText(content);
            } catch (FileNotFoundException e) {
                MQDialogBox.openException(e);
            }

            Main.configManager.setProperty("interface.lastfolder", file.getParent());
        }
    }

    public void prettyPrintRequest(ActionEvent actionEvent) {
        if (codearea_requestData.getText().length() > 0) {
            codearea_requestData.replaceText(UtilsManager.prettyPrint(codearea_requestData.getText()));
            codearea_requestData.setStyleSpans(0, TextHighlighter.computeHighlighting(codearea_requestData.getText()));
        }
    }

    public void saveFile(ActionEvent actionEvent) {
        if (codearea_requestData.getText().isEmpty()) {
            MQDialogBox.openError("Error", "No request data", "No request data to save to file");
            return;
        }

        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save request file...");

        File f = new File(Main.configManager.getProperty("interface.lastfolder"));
        if (f.exists())
            fileChooser.setInitialDirectory(f);

        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            try{
                FileWriter outStream = new FileWriter(file.getAbsolutePath());
                BufferedWriter out = new BufferedWriter(outStream);
                out.write(codearea_requestData.getText());
                out.close();
            }catch (Exception e){
                MQDialogBox.openException(e);
            }

            Main.configManager.setProperty("interface.lastfolder", file.getParent());
        }
    }

    public void refreshDataStyling() {
        try {
            if (codearea_requestData.getText()!=null && codearea_requestData.getText().length()>0)
                codearea_requestData.setStyleSpans(0, TextHighlighter.computeHighlighting(codearea_requestData.getText()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void generateRequest(ActionEvent actionEvent) {
        new MQWindow("window/generaterequest", "Generate sample request from XSD", 300, 400, false, codearea_requestData.getScene().getWindow(), false, false, false);
    }
}
package cz.mrqev.controller.tab;

import cz.mrqev.controller.BaseController;
import cz.mrqev.interfaces.BaseConfiguration;
import cz.mrqev.interfaces.TabController;
import javafx.scene.control.Tab;
import javafx.util.Pair;

public abstract class BaseTabController extends BaseController implements TabController {

    @Override
    public String getTabName() {
        throw new UnsupportedOperationException("This method is implemented only in plugin tabs.");
    }

    public abstract void initialize();
}

package cz.mrqev.controller.tab;

import com.ibm.mq.MQException;
import com.ibm.mq.MQQueue;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.controller.IntegerTextFormatter;
import cz.mrqev.interfaces.LogTabControllerInterface;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import cz.mrqev.main.Main;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import cz.mrqev.model.mq.wrapper.transport.MQRequest;
import cz.mrqev.model.cache.MqCache;
import cz.mrqev.model.mq.actions.MQSaver;
import cz.mrqev.model.mq.actions.MQWriter;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.view.MQWindow;
import cz.mrqev.utils.timer.Timer;
import cz.mrqev.interfaces.annotation.Responsive;
import cz.mrqev.interfaces.annotation.Serializable;
import org.fxmisc.richtext.CodeArea;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class MqTabController extends BaseTabController {
    private final static String LOADING_QUEUES_PLACEHOLDER = "loading queues...";

    //MQ
    @Responsive(changeWidth = 1)
    @Serializable(serializedName = "QueueManager")
    public ComboBox combobox_queueManagers;
    @Responsive(changeWidth = 1)
    @Serializable(serializedName = "Queue", getMethod = "getValue", setMethod = "setValue")
    public ComboBox combobox_queues;
    @Responsive(changeWidth = 1, changeHeight = 1)
    public TextArea textarea_logMQ;
    @Responsive(shiftHorizontally = 1)
    public Button button_putMQMessage;
    @Responsive(shiftHorizontally = 1)
    public Button button_openMQConnections;
    @Responsive(shiftHorizontally = 1)
    public Button button_saveQueue;

    // PutMessageOptions
    @Serializable(serializedName = "CopyUIDToClipboard", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_MQ_copyUIDToClipboard;
    @Serializable(serializedName = "FillTimestamps", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_MQ_fillTimestamps;
    @Serializable(serializedName = "FillUID", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_MQ_generateUID;
    @Serializable(serializedName = "AddWSS", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public CheckBox checkbox_addWSS;
    @Serializable(serializedName = "GetNone", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radioButton_none;
    @Serializable(serializedName = "GetReply", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_getReply;
    @Serializable(serializedName = "GetLog", getMethod = "isSelected", setMethod = "setSelected", setMethodParam = boolean.class)
    public RadioButton radiobutton_getLog;
    @Responsive(changeWidth = 1)
    public Separator separator_putMessageOptions;
    @Responsive(shiftHorizontally = 1)
    public MenuButton menuButton_customPlaceholders;

    public Label label_queueName;
    @Responsive(shiftHorizontally = 1)
    public Label label_lastUpdatedQueuesDate;
    @Responsive(shiftHorizontally = 1)
    public Label label_lastUpdated;
    @Responsive(shiftHorizontally = 1)
    public Button button_loadQueues;

    // Browse queue
    @Responsive(shiftHorizontally = 1)
    public Button button_browseQueue;
    @Serializable(serializedName = "BrowseCount")
    public TextField textfield_browseCount;
    @Responsive(changeWidth = 1)
    @Serializable(serializedName = "BrowseFilter")
    public TextField textfield_browseFilter;
    @Responsive(changeWidth = 1)
    public Separator separator_browseQueueOptions;
    @Responsive(changeWidth = 1)
    public Separator separator_log;

    public ArrayList<String> queueList = new ArrayList<>();


    public static MqTabController getInstance() {
        return RequestTabController.getInstance().mqTabController;
    }

    public void refreshQueueManagerList() {
        Platform.runLater(() -> {
            combobox_queueManagers.getItems().clear();
            MqmdTabController.getInstance().combobox_replyQmgr.getItems().clear();
            for (MQConnectionEntry entry : Main.configManager.MQConnections) {
                combobox_queueManagers.getItems().add(entry.getQueueManager() + " - (" + entry.getUserId() + "@" + entry.getHostname() + ":" + entry.getPort() + "/" + entry.getChannel() + ")");
                MqmdTabController.getInstance().combobox_replyQmgr.getItems().add(entry.getQueueManager() + " - (" + entry.getUserId() + "@" + entry.getHostname() + ":" + entry.getPort() + "/" + entry.getChannel() + ")");
            }
        });
    }

    public void refreshCustomPlaceholdersList() {
        placeholderManager.getCustomPlaceholders().forEach(placeholder -> {
            CheckMenuItem item = new CheckMenuItem();
            Label label = new Label(placeholder.getName());
            label.setPrefWidth(124);
            item.setGraphic(label);
            CodeArea codeArea = DataTabController.getInstance().codearea_requestData;

            item.selectedProperty().addListener((observable, oldValue, newValue) -> {
                if (newValue) {
                    codeArea.replaceText(placeholder.fill(codeArea.getText(), this::addLogMessage));
                } else {
                    codeArea.replaceText(placeholder.replace(codeArea.getText(), this::addLogMessage));
                }
            });

            menuButton_customPlaceholders.getItems().add(item);
        });

        if (menuButton_customPlaceholders.getItems().isEmpty()) {
            menuButton_customPlaceholders.setDisable(true);
        } else {
            menuButton_customPlaceholders.setDisable(false);
        }
    }

    public void initialize() {
        refreshQueueManagerList();

        // add tooltips
        checkbox_MQ_fillTimestamps.setTooltip(tooltipManager.createFillTimestampsTooltip());
        checkbox_MQ_generateUID.setTooltip(tooltipManager.createFillSidTooltip());
        label_queueName.setTooltip(tooltipManager.createQueueNameTooltip());

        textfield_browseCount.setTextFormatter(IntegerTextFormatter.getTextFormatterWithDefaultValue(0));
    }

    public void putMQmessage(ActionEvent actionEvent) {
        if (!isQueueManagerSelected() || !isQueueSelected()) return;

        MQConnection connection = null;
        MQQueue queue = null;

        try {
            // initialize mq connection
            connection = new MQConnection(this.getSelectedMqConnection());
            // access queue
            int openOptions = MQConstants.MQOO_OUTPUT + MQConstants.MQOO_FAIL_IF_QUIESCING;
            queue = connection.accessQueue(MqTabController.getInstance().getSelectedQueue(), openOptions);

            // create new request
            MQRequest request;
            String data = DataTabController.getInstance().codearea_requestData.getText();
            MqmdWrapper mqmd = MqmdTabController.getInstance().wrapMqmd();

            MQMessageWrapper mqMessageWrapper = new MQMessageWrapper(data, mqmd);

            if (checkbox_addWSS.isSelected()) {
                request = new MQRequest(
                        mqMessageWrapper,
                        checkbox_MQ_fillTimestamps.isSelected(),
                        checkbox_MQ_generateUID.isSelected(),
                        String.valueOf(MqmdTabController.getInstance().combobox_wssUser.getValue()),
                        MqmdTabController.getInstance().textfield_wssPassword.getText());
            } else {
                request = new MQRequest(
                        mqMessageWrapper,
                        checkbox_MQ_fillTimestamps.isSelected(),
                        checkbox_MQ_generateUID.isSelected());
            }

            if (checkbox_MQ_copyUIDToClipboard.isSelected()) {
                Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard();
                clpbrd.setContents(new StringSelection(request.getSid()), null);
            }
            // initialize mq writer and put message
            byte[] messageId = new MQWriter(queue, request).put();

            // get reply when we have selected radiobutton
            if (radiobutton_getReply.isSelected() && messageId != null) {
                ReplyTabController.getInstance().replyCheckerByCorrelId(messageId);
            }

            queue.close();
            connection.close();

            this.addLogMessage(
                    "message with" + (checkbox_MQ_generateUID.isSelected() ? (" SID " + request.getSid()) : "") + " length " +
                            DataTabController.getInstance().codearea_requestData.getText().length() + " inserted to queue " + String.valueOf(combobox_queues.getValue()), true);

            // get log from database when we have selected Get log radiobutton
            if (radiobutton_getLog.isSelected() && pluginManager.checkClassInterface(LogTabControllerInterface.class)) {
                button_putMQMessage.setDisable(true);

                // get plugin log tab name
                String logTabControllerName = ((LogTabControllerInterface)pluginManager.getClassInstanceByInterface(LogTabControllerInterface.class)).getTabName();
                // get plugin log tab controller interface from request tab controller
                LogTabControllerInterface logTabController = (LogTabControllerInterface) RequestTabController.getInstance().getPluginTabController(logTabControllerName);

                new Timer(3, 1000, Timer.DESC, (actualCounter, stopTimer) -> {
                    if (actualCounter == 0) {
                        Platform.runLater(() -> button_putMQMessage.setDisable(false));
                        RequestTabController.getInstance().tabs.getSelectionModel().select(RequestTabController.LOG_TAB_POSITION);
                        logTabController.logCheckerBySid(request.getSid(), 10, 3000);
                    } else {
                        logTabController.stopLoading(null);
                        Platform.runLater(() -> addLogMessage("switching to log tab in " + actualCounter + " seconds.", true));
                    }
                });
            }

        } catch (MQException e) {
            if (e.reasonCode == 2027) {
                MQDialogBox.openError("Error", "Wrong message type", "For message type Request you have to set reply to queue in MQMQ tab. If you don't want reply, set message type to Datagram");
            } else {
                addLogMessage(e.getMessage(), true);
                e.printStackTrace();
            }
        } catch (IOException e) {
            MQDialogBox.openException(e);
        } finally {
            try {
                if (queue != null)
                    queue.close();
                if (connection != null)
                    connection.close();
            } catch (MQException e) {
                MQDialogBox.openException(e);
            }
        }
    }

    public void browseQueue(ActionEvent actionEvent) {
        if (!isQueueManagerSelected() || !isQueueSelected()) return;

        String title = String.format("Browsing %s on %s", combobox_queues.getValue(), Main.configManager.MQConnections.get(combobox_queueManagers.getSelectionModel().getSelectedIndex()).getQueueManager());

        new MQWindow("window/mqbrowser", title, 850, 550, true, null, false, true, true);
    }


    public void openMQConnections(ActionEvent actionEvent) {
        new MQWindow("window/mqconnection", "Configured MQ connections", 700, 500, false, this.combobox_queueManagers.getScene().getWindow(), false, false, true);
    }

    public void loadQueues(ActionEvent actionEvent) {
        if (!isQueueManagerSelected()) return;

        // load queues in separate thread
        new Thread(() -> {
            MQConnection connection = null;
            try {
                // initialize mq connection
                connection = new MQConnection(this.getSelectedMqConnection());

                // check if queue load is in progress in another thread
                if (!connection.queueLoadInProgress) {
                    String selectedQueueManager = connection.getSelectedConnectionEntry().getQueueManager();

                    connection.queueLoadFromQM = this.getSelectedMqConnection().getQueueManager();
                    connection.queueLoadInProgress = true;
                    this.addLogMessage("loading queues for queue manager " + connection.queueLoadFromQM + " started", true);

                    combobox_queues.getItems().clear();
                    combobox_queues.setValue(LOADING_QUEUES_PLACEHOLDER);
                    combobox_queues.setDisable(true);
                    button_loadQueues.setDisable(true);
                    button_putMQMessage.setDisable(true);
                    button_browseQueue.setDisable(true);
                    button_saveQueue.setDisable(true);

                    try {
                        Collection<String> queues = connection.loadQueues();
                        queues = queues.stream().map(String::trim).collect(Collectors.toList());
                        combobox_queues.getItems().addAll(queues);
                        // for queue filter
                        queueList.clear();
                        queueList.addAll(queues);
                        // update cache
                        String lastUpdatedQueues = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
                        Platform.runLater(() -> label_lastUpdatedQueuesDate.setText(lastUpdatedQueues));

                        MqCache.getInstance().addCachedRecord("lastUpdatedQueues" + selectedQueueManager, lastUpdatedQueues);
                        MqCache.getInstance().addCachedRecord("loadedQueues" + selectedQueueManager, queues);
                    } catch (IOException e) {
                        MQDialogBox.openException(e);
                    }

                    this.addLogMessage("loaded " + combobox_queues.getItems().size() + " queue names from queue manager " + connection.queueLoadFromQM, true);
                    combobox_queues.setValue("");
                    combobox_queues.setDisable(false);
                    button_loadQueues.setDisable(false);
                    button_putMQMessage.setDisable(false);
                    button_browseQueue.setDisable(false);
                    button_saveQueue.setDisable(false);
                    connection.queueLoadInProgress = false;

                } else {
                    this.addLogMessage("loading queues for queue manager " + connection.queueLoadFromQM + " are already in progress, please wait", true);
                }
            } catch (MQException e) {
                combobox_queues.setDisable(false);
                button_putMQMessage.setDisable(false);
                button_loadQueues.setDisable(false);
                button_browseQueue.setDisable(false);
                MQDialogBox.openException(e);

                if (connection != null) {
                    connection.queueLoadInProgress = false;
                }
            }
        }).start();

    }

    public MQConnectionEntry getSelectedMqConnection() {
        return Main.configManager.MQConnections.get(combobox_queueManagers.getSelectionModel().getSelectedIndex());
    }

    public String getSelectedQueue() {
        return String.valueOf(combobox_queues.getValue());
    }

    public String getFilter() {
        return textfield_browseFilter.getText();
    }

    public Void addLogMessage(String message) {
        addLogMessage(message, true);

        return null;
    }

    public void addLogMessage(String message, Boolean withTime) {
        if (withTime)
            message = new SimpleDateFormat("HH:mm:ss").format(new Date()) + " - " + message;
        message += "\n";
        this.textarea_logMQ.appendText(message);
    }

    public void saveQueue(ActionEvent event) {
        if (!isQueueManagerSelected() || !isQueueSelected()) return;

        Stage stage = new Stage();

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose target file...");
        fileChooser.setInitialFileName(new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date()) + "-" + combobox_queues.getValue() + "-" + getSelectedMqConnection().getQueueManager() + ".queue");

        File file = fileChooser.showSaveDialog(stage);

        if (file != null) {
            MQSaver mqSaver = new MQSaver(file.getAbsolutePath(), getSelectedMqConnection(), String.valueOf(combobox_queues.getValue()), 0, "", this::addLogMessage);
            mqSaver.save();
        }
    }

    public boolean isQueueManagerSelected() {
        if (combobox_queueManagers.getSelectionModel().getSelectedIndex() < 0) {
            MQDialogBox.openError("Error", "No queue manager selected", "Please select a QM you want to use");
            return false;
        }

        return true;
    }

    public boolean isQueueSelected() {
        if (String.valueOf(combobox_queues.getValue()).equals("") || String.valueOf(combobox_queues.getValue()).equals(LOADING_QUEUES_PLACEHOLDER)) {
            MQDialogBox.openError("Error", "No queue specified", "Please select/enter a queue you want to use");
            return false;
        }

        return true;
    }

    public void copyQueue(ActionEvent event) {
    }

}

package cz.mrqev.controller;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.Controller;
import cz.mrqev.main.Main;
import cz.mrqev.manager.*;
import cz.mrqev.interfaces.annotation.Serializable;
import cz.mrqev.view.MQNode;
import cz.mrqev.interfaces.annotation.Responsive;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseController implements Controller {
    // managers
    public PlaceholderManager placeholderManager = Main.placeholderManager;
    protected TooltipManager tooltipManager = new TooltipManager();
    protected TemplateManager templateManager = new TemplateManager();
    protected ListenerManager listenerManager = Main.listenerManager;
    protected PluginManager pluginManager = Main.pluginManager;
    public SessionManager sessionManager = new SessionManager();

    public List<MQNode> getAllResponsiveNodesForTab(String tabName) {
        return getNodes(tabName, Responsive.class);
    }
    public List<MQNode> getAllSerializableNodesForTab(String tabName) {
        return getNodes(tabName, Serializable.class);
    }

    // Get annotated nodes from all existing request tab controllers or only from tabName controller
    public List<MQNode> getNodes(String tabName, Class annotation) {
        List<MQNode> nodes = UtilsManager.getAnnotatedNodes(MainController.getInstance(), annotation);

        if (tabName == null) {
            // list all responsible nodes in all tabs
            MainController.getTabControllers().forEach((key, value) -> nodes.addAll(value.getNodes(annotation)));
        } else {
            // list responsible nodes only in given tab with tabName
            nodes.addAll(MainController.getTabController(tabName).getNodes(annotation));
        }

        return nodes;
    }
}

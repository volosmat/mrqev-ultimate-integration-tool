package cz.mrqev.event;

import cz.mrqev.view.MQTab;

import java.util.EventObject;

public class TabCreatedEvent extends EventObject {
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @throws IllegalArgumentException if source is null.
     */
    public TabCreatedEvent(MQTab source) {
        super(source);
    }
}

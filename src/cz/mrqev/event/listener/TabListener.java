package cz.mrqev.event.listener;

import cz.mrqev.event.TabCreatedEvent;

import java.util.EventListener;

public interface TabListener extends EventListener {
    public void tabCreated(TabCreatedEvent event);
}

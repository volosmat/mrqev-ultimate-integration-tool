package cz.mrqev.event;

import java.util.ArrayList;
import java.util.EventListener;

public abstract class EventProducer {

    protected transient ArrayList<EventListener> listeners = new ArrayList<>();

    synchronized public void addListener(EventListener listener) {
        listeners.add(listener);
    }

    synchronized public void removeListener(EventListener listener) {
        listeners.remove(listener);
    }
}

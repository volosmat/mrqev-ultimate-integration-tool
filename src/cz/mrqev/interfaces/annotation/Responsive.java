package cz.mrqev.interfaces.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Responsive {
    // change position of node horizontally (value mean ratio how much shift node)
    double shiftHorizontally() default 0;
    // change position of node vertically (value mean ratio how much shift node)
    double shiftVertically() default 0;
    // make node width bigger (value mean ratio how much scale node)
    double changeWidth() default 0;
    // make node height bigger (value mean ratio how much scale node)
    double changeHeight() default 0;
}

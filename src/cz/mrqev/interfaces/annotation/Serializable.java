package cz.mrqev.interfaces.annotation;

import java.lang.annotation.*;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Serializable {
    // name of serialized variable in stored file
    String serializedName();
    // method of JavaFX node to get data from it
    String getMethod() default "getText";
    // method of JavaFX node to set data to it
    String setMethod() default "setText";
    // data type for set method parameter
    Class<?> setMethodParam() default String.class;
}


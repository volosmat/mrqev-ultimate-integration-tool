package cz.mrqev.interfaces;

import cz.mrqev.interfaces.api.*;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.Map;

public abstract class BaseConfiguration {

    private static BaseConfiguration instance;

    // Dont change this names
    public static final String MQ_TAB_NAME = "MQ";
    public static final String WS_TAB_NAME = "WS";
    public static final String DATA_TAB_NAME = "Data";
    public static final String REPLY_TAB_NAME = "Reply";
    public static final String HEADERS_TAB_NAME = "Headers";
    public static final String BATCH_PUT_NAME = "Batch Put";

    private ViewApi viewApi;
    private DatabaseApi databaseApi;
    private UtilsApi utilsApi;
    private ListenerApi listenerApi;
    private PlaceholderApi placeholderApi;

    public static BaseConfiguration getInstance() {
        if (instance == null) {
            throw new NullPointerException("Plugin configuration was not initialized.");
        }

        return instance;
    }

    public abstract String getName();
    public abstract String getVersion();

    public BaseConfiguration() {
        instance = this;
    }

    /**
     * Default order of core tabs in application
     * @return Map
     */
    public Map<String, Integer> tabOrder() {
        return Map.of(MQ_TAB_NAME, 0, WS_TAB_NAME, 1, DATA_TAB_NAME, 2, REPLY_TAB_NAME, 3, HEADERS_TAB_NAME, 4, BATCH_PUT_NAME, 5);
    }

    public void setViewApi(ViewApi viewApi) {
        this.viewApi = viewApi;
    }

    public ViewApi getViewApi() {
        return viewApi;
    }

    public DatabaseApi getDatabaseApi() {
        return databaseApi;
    }

    public void setDatabaseApi(DatabaseApi databaseApi) {
        this.databaseApi = databaseApi;
    }

    public UtilsApi getUtilsApi() {
        return utilsApi;
    }

    public void setUtilsApi(UtilsApi utilsApi) {
        this.utilsApi = utilsApi;
    }

    public ListenerApi getListenerApi() {
        return listenerApi;
    }

    public void setListenerApi(ListenerApi listenerApi) {
        this.listenerApi = listenerApi;
    }

    public PlaceholderApi getPlaceholderApi() {
        return placeholderApi;
    }

    public void setPlaceholderApi(PlaceholderApi placeholderApi) {
        this.placeholderApi = placeholderApi;
    }
}

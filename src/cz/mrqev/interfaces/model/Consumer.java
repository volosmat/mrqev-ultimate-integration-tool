package cz.mrqev.interfaces.model;

public interface Consumer {

    String getName();
    String getWssUsername();
    String getWssPassword();
    String getHttpEndpointAlias();
    int getHttpPort();
    String getCallableEndpoint(boolean useHttps);
    String getHttpGateway();
}

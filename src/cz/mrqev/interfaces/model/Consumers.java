package cz.mrqev.interfaces.model;

import java.util.List;

public interface Consumers {

    List<Consumer> getConsumers();
    String getPasswordForUser(String wssUsername);
    Consumer getConsumerByWssUsername(String wssUsername);
}

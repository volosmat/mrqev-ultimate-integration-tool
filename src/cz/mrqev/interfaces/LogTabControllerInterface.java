package cz.mrqev.interfaces;

import javafx.event.ActionEvent;

public interface LogTabControllerInterface extends TabController {
    void logCheckerBySid(String sid, int repeatCounter, int delay);
    void logCheckerBySid(String sid);
    void stopLoading(ActionEvent actionEvent);
    void updateLog(ActionEvent actionEvent);
}

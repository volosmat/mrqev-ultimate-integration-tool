package cz.mrqev.interfaces;

import javafx.stage.Stage;

public interface BaseWindow {
    Stage getStage();
    WindowController getController();
    ClassLoader getClassLoader();
}

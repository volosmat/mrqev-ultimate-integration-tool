package cz.mrqev.interfaces;

public interface Controller {
    default void initialize() {};
}

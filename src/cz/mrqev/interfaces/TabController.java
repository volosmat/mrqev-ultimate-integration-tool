package cz.mrqev.interfaces;

public interface TabController extends Controller {
    String getTabName();
}

package cz.mrqev.interfaces;

import javafx.scene.layout.Pane;

public interface WindowController extends Controller {
    void close();
    Pane getPane();
    void setWindow(BaseWindow window);
}

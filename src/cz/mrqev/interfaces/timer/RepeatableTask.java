package cz.mrqev.interfaces.timer;


public interface RepeatableTask {

    void run(int actualCounter, Runnable stopTimer);
}

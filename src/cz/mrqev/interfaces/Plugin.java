package cz.mrqev.interfaces;

import javafx.scene.control.Tab;
import javafx.util.Pair;

import java.util.List;

public interface Plugin {
    /**
     * Initialize whole plugin except of tabs
     */
    void initialize();

    /**
     * Specific function to initialize tabs
     * @return
     */
    List<Pair<TabController, Tab>> initializeTabs();
}

package cz.mrqev.interfaces.api;

import cz.mrqev.interfaces.model.Consumers;
import javafx.beans.value.ChangeListener;

import java.util.function.Function;

public interface ListenerApi {
    void addDataTextChangedListener(ChangeListener<String> listener);
    void addImportConsumersAction(String extensionFilter, Function<String, Consumers> processFunction);
}

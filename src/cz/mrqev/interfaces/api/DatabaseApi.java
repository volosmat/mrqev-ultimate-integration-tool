package cz.mrqev.interfaces.api;

import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseApi {
    Connection createOracleDBConnection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException;
    Connection createDB2Connection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException;
}

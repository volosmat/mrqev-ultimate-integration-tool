package cz.mrqev.interfaces.api;

import cz.mrqev.exception.TemplateGroupNotFoundException;
import cz.mrqev.interfaces.BaseWindow;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.List;

public interface ViewApi {
    void openError(String title, String header, String content);
    void openWarning(String title, String header, String content);
    void openInformation(String title, String header, String content);
    // yes = 1, no = 0, undefined = -1
    int openConfirmation(String title, String header, String content);
    void openException(Exception ex);
    Tab createTabWithTextArea(String name, String content, boolean prettyPrint);
    Tab createTabWithCodeArea(String name, String content, boolean prettyPrint);
    void selectTabByName(TabPane tabPane, String tabName);
    List<Menu> getMenu(String text);
    Menu addMenu(String text);
    MenuItem addMenuItem(Menu toMenu, String text, EventHandler<ActionEvent> onAction);
    String getRequestData();
    void setRequestData(String data);
    MenuItem addTemplateGroup(String name);
    MenuItem addTemplate(MenuItem group, String name, EventHandler<ActionEvent> onAction) throws TemplateGroupNotFoundException;
    MenuItem getTemplateGroup(String name) throws TemplateGroupNotFoundException;
    BaseWindow createAndShowWindow(URL templateResource, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean onTop, ClassLoader classLoader);
    void setQueue(String queueName);
    void addMqLogMessage(String message, boolean withTime);
    void addWsLogMessage(String message, boolean withTime);
    void addBatchLogMessage(String message, boolean withTime);
}

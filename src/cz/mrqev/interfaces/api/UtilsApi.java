package cz.mrqev.interfaces.api;

import cz.mrqev.interfaces.timer.RepeatableTask;

public interface UtilsApi {
    int DB_CACHE_TYPE = 1;
    int MQ_CACHE_TYPE = 2;
    int OTHERS_CACHE_TYPE = 3;

    /**
     * @param repeatCounter - how many times task will be repeated
     * @param delay - delay between task run
     * @param direction - DESC = 1, ASC = 2
     * @param task - task to execution
     */
    void createTimer(int repeatCounter, long delay, int direction, RepeatableTask task);

    /**
     * Pretty print JSON or XML input
     * @param input - json or xml text
     * @return formatted output
     */
    String prettyPrint(String input);

    void setCachedProperty(int cacheType, String key, Object value);
    Object getCachedProperty(int cacheType, String key);
    String getConfigProperty(String key);
    boolean isJson(String input);
    String getRandomAlphanumeric(int lenght);
}

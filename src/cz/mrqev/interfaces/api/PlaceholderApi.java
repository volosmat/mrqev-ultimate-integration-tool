package cz.mrqev.interfaces.api;

import cz.mrqev.interfaces.function.TriFunction;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

public interface PlaceholderApi {
    void addCustomPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill);
    void removeCustomPlaceholder(String name);
    void setGenerateUidPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill);
    void setTimestampsPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill);
}

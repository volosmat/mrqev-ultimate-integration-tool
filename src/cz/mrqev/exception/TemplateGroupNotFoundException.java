package cz.mrqev.exception;

public class TemplateGroupNotFoundException extends Exception {
    public TemplateGroupNotFoundException(String message) {
        super(message);
    }
}

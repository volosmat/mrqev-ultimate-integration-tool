package cz.mrqev.model.cache;

public class DatabaseCache extends BaseCache {

    private static DatabaseCache instance;

    public static DatabaseCache getInstance() {
        if (instance == null) {
            instance = new DatabaseCache();
        }

        return instance;
    }

    @Override
    protected String getPath() {
        return "temp/databaseCache";
    }
}

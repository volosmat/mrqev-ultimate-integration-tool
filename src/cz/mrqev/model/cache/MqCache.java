package cz.mrqev.model.cache;

public class MqCache extends BaseCache {
    private static MqCache instance;

    public static MqCache getInstance() {
        if (instance == null) {
            instance = new MqCache();
        }

        return instance;
    }

    @Override
    protected String getPath() {
        return "temp/mqCache";
    }
}

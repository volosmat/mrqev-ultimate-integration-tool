package cz.mrqev.model.cache;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import cz.mrqev.manager.UtilsManager;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;

public abstract class BaseCache {
    private HashMap<String, Object> baseCache = new HashMap<>();

    @Override
    public String toString() {
        Gson gson = new Gson();
        return gson.toJson(baseCache);
    }

    /**
     * Store cache to file in getPath()
     * Listener is in main class on main window stage
     */
    public void store() {
        try {
            UtilsManager.storeToFile(getPath(), toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Load cache from file in getPath() and store in baseCache map
     * Listener is in main class on main window stage
     */
    public void load() {
        try {
            Gson gson = new Gson();
            Type type = new TypeToken<HashMap<String, Object>>(){}.getType();
            baseCache = gson.fromJson(UtilsManager.loadFromFile(getPath()), type);
        } catch (IOException e) {
            // do nothing, if we have no cache we no need to load it
        }
    }

    public void addCachedRecord(String key, Object value) {
        baseCache.put(key, value);
    }

    public Object getCachedRecord(String key) {
        return baseCache.get(key);
    }

    public boolean contains(String key) {
        return baseCache.containsKey(key);
    }

    protected abstract String getPath();
}

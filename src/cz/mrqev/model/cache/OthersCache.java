package cz.mrqev.model.cache;

public class OthersCache extends BaseCache {
    private static OthersCache instance;

    public static OthersCache getInstance() {
        if (instance == null) {
            instance = new OthersCache();
        }

        return instance;
    }

    @Override
    protected String getPath() {
        return "temp/othersCache";
    }
}

package cz.mrqev.model.database.connection;

import java.sql.*;

public class DB2Connection extends DBConnection {

    public DB2Connection(Connection connection) {
        super(connection);
    }

    public DB2Connection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        super(hostname, username, password, timeout);
    }

    @Override
    protected void loadDriver() throws ClassNotFoundException {
        Class.forName("com.ibm.db2.jcc.DB2Driver");
    }

    @Override
    protected String getConnectionString(String hostname) {
        return "jdbc:db2://" + hostname;
    }
}

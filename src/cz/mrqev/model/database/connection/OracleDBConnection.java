package cz.mrqev.model.database.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class OracleDBConnection extends DBConnection {

    public OracleDBConnection(Connection connection) {
        super(connection);
    }

    public OracleDBConnection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        super(hostname, username, password, timeout);
    }

    @Override
    protected void loadDriver() throws ClassNotFoundException {
        Class.forName("oracle.jdbc.OracleDriver");
    }

    @Override
    protected String getConnectionString(String hostname) {
        return "jdbc:oracle:thin:@" + hostname;
    }
}

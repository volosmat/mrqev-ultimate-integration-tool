package cz.mrqev.model.database.connection;

import cz.mrqev.utils.timer.Timer;

import java.sql.*;
import java.util.HashMap;

public abstract class DBConnection {
    protected Connection connection;

    // store already established connections for future use
    private static HashMap<String, Connection> establishedConnections = new HashMap<>();
    private static Timer invalidateCache;

    protected DBConnection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        // every hour invalidate connection cache, this is almost infinite timer
        if (invalidateCache == null) {
            long delayHour = 60 * 60 * 1000;
            invalidateCache = new Timer(Integer.MAX_VALUE, delayHour, Timer.ASC, (counter, stop) -> {
                closeConnections();
                establishedConnections.clear();
            });
        }

        this.connection = getConnection(hostname, username, password, timeout);
    }

    /**
     * Create instance of connection wrapper from already established connection
     * @param connection - db connection
     */
    protected DBConnection(Connection connection) {
        this.connection = connection;
    }

    private Connection getConnection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        String hashKey = hostname + username;
        Connection establishedConn = establishedConnections.get(hashKey);

        if (establishedConnections.containsKey(hashKey) && establishedConn.isValid(timeout) && !establishedConn.isClosed()) {
            return establishedConnections.get(hashKey);
        } else {
            loadDriver();
            Connection connection = DriverManager.getConnection(getConnectionString(hostname), username, password);
            establishedConnections.put(hashKey, connection);

            return connection;
        }
    }

    /**
     * Load cz.mrqev.model.database driver class
     * @throws ClassNotFoundException - no database driver exists on given path
     */
    protected abstract void loadDriver() throws ClassNotFoundException;

    /**
     * Create connection string from db prefix and alias
     * @param hostname - host:port/cz.mrqev.model.database
     * @return connection string
     */
    protected abstract String getConnectionString(String hostname);

    /**
     * Execute sql comment on actual cz.mrqev.model.database
     * @param sql - command to execute
     * @return - result set
     * @throws SQLException - problem with execution
     */
    public ResultSet executeQuery(String sql) throws SQLException {
        return connection.prepareStatement(sql).executeQuery();
    }

    /**
     * Create statement to prepare DB2 call
     * @param sql - command to execute
     * @return - prepared statement
     * @throws SQLException - problem with statement
     */
    public CallableStatement createStatement(String sql) throws SQLException {
        return connection.prepareCall(sql);
    }

    /**
     * Close all established connections from map
     */
    public static void closeConnections() {
        for (Connection establishedConnection : establishedConnections.values()) {
            try {
                if (!establishedConnection.isClosed())
                    establishedConnection.close();
            } catch (SQLException ignored) {
                // cannot close one connection, it could be closed before, continue with next connection
            }
        }
    }

    public Connection getConnection() {
        return connection;
    }
}

package cz.mrqev.model.json;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class JsonHolder {
    private static final Gson gson = new Gson();

    public static boolean isJson(String json) {
        try {
            gson.fromJson(json, Object.class);
            return true;
        } catch(JsonSyntaxException ex) {
            return false;
        }
    }
}

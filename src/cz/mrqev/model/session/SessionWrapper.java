package cz.mrqev.model.session;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ibm.mq.MQMessage;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import org.apache.commons.configuration2.XMLConfiguration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

public class SessionWrapper {
    private HashMap<String, Optional<Object>> properties = new HashMap<>();

    public void setProperty(String key, Object value) {
        properties.put(key, Optional.ofNullable(value));
    }

    public void setProperties(HashMap<String, Object> propertiesToSet) {
        for (Map.Entry property: propertiesToSet.entrySet()) {
            setProperty((String) property.getKey(), property.getValue());
        }
    }

    public Optional<Object> getProperty(String key) {
        if (properties.containsKey(key))
            return properties.get(key);
        else
            return Optional.empty();
    }

    public Integer getIntProperty(String key) {
        return Integer.parseInt((String) getProperty(key).orElse("0"));
    }

    public Boolean getBoolProperty(String key) {
        return Boolean.parseBoolean((String) getProperty(key).orElse("false"));
    }

    public String getStringProperty(String key) {
        return (String) getProperty(key).orElse("");
    }

    public HashMap<String, Optional<Object>> getProperties() {
        return properties;
    }

    public String toJson() {
        Gson gson = new Gson();
        HashMap<String, Object> result = new HashMap<>();

        for (Map.Entry<String, Optional<Object>> property: properties.entrySet()) {
            if (property.getValue().isPresent())
                result.put(property.getKey(), property.getValue().get());
        }

        return UtilsManager.prettyPrintJson(gson.toJson(result));
    }

    public static SessionWrapper fromJson(String json) {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap<String, Object>>(){}.getType();

        SessionWrapper result = new SessionWrapper();
        result.setProperties(gson.fromJson(json, type));

        return result;
    }

    // legacy mrqev session support
    public static SessionWrapper fromXml(String path) throws ConfigurationException {
        XMLConfiguration config = (new Configurations()).xml(path);
        SessionWrapper result = new SessionWrapper();

        for (Iterator<String> keys = config.getKeys(); keys.hasNext(); ) {
            String key = keys.next();
            result.setProperty(key, config.getProperty(key));
        }

        return result;
    }

    public MqmdWrapper wrapMqmd() {
        MQMessage mqMessage = new MQMessage();

        mqMessage.format = getStringProperty("MQMessageFormat");
        mqMessage.feedback = MQConstants.MQFB_NONE;
        mqMessage.messageType = getIntProperty("MessageType");
        mqMessage.characterSet = getIntProperty("Codepage");
        mqMessage.expiry = getIntProperty("Expiry");

        if (!getStringProperty("ReplyToQueue").equals(""))
            mqMessage.replyToQueueName = getStringProperty("ReplyToQueue");

        if (!getStringProperty("ReplyToQM").equals(""))
            mqMessage.replyToQueueManagerName = getStringProperty("ReplyToQM");

        if (!getStringProperty("MessageId").equals(""))
            mqMessage.messageId = getStringProperty("MessageId").getBytes();
        else
            mqMessage.messageId = MQConstants.MQMI_NONE;

        if (!getStringProperty("CorrelId").equals(""))
            mqMessage.correlationId = getStringProperty("CorrelId").getBytes();
        else
            mqMessage.correlationId = MQConstants.MQCI_NONE;

        mqMessage.persistence = getIntProperty("Persistence");

        if (getBoolProperty("PassCorrelId"))
            mqMessage.report = mqMessage.report | MQConstants.MQRO_PASS_CORREL_ID;

        if (getBoolProperty("PassMessageId"))
            mqMessage.report = mqMessage.report | MQConstants.MQRO_PASS_MSG_ID;

        return new MqmdWrapper(mqMessage);
    }
}

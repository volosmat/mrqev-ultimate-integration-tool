package cz.mrqev.model;

import cz.mrqev.interfaces.function.TriFunction;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

@AllArgsConstructor
public class Placeholder {
    // placeholder e.g. ##name##
    @Getter
    private String name;
    // function to replace placeholder with given value
    private TriFunction<String, String, Consumer<String>, String>  replace;
    // function to generate value to replace
    private Supplier<String> replacementFunction;
    // fill placeholder to input
    private BiFunction<String, Consumer<String>, String> fill;

    public String replace(String input, Consumer<String> log) {
        return replace.apply(input, generateReplacement(), log);
    }

    public String replace(String input, String replacement, Consumer<String> log) {
        return replace.apply(input, replacement, log);
    }

    public String replace(String input, String replacement) {
        return replace.apply(input, replacement, null);
    }

    public String replace(String input) {
        return replace.apply(input, generateReplacement(), null);
    }

    public String fill(String input, Consumer<String> log) {
        return fill.apply(input, log);
    }

    public String fill(String input) {
        return fill.apply(input, null);
    }

    public String generateReplacement() {
        return (replacementFunction != null)?replacementFunction.get():null;
    }
}

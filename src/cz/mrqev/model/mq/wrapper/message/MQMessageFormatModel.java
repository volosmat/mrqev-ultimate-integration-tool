package cz.mrqev.model.mq.wrapper.message;

import lombok.Value;

@Value
public class MQMessageFormatModel {
    private String flag;
    private String value;

    public static MQMessageFormatModel valueOf(String value) {
        return MQMessageFormat.getFormatForValue(value);
    }

    @Override
    public String toString() {
        return value;
    }
}

package cz.mrqev.model.mq.wrapper.message;

import com.ibm.mq.constants.MQConstants;

import java.util.List;

public class MQMessageFormat {
    public static List<MQMessageFormatModel> formats =
            List.of(new MQMessageFormatModel(MQConstants.MQFMT_NONE, "- none -"),
                    new MQMessageFormatModel(MQConstants.MQFMT_STRING, "MQSTR"),
                    new MQMessageFormatModel(MQConstants.MQFMT_DEAD_LETTER_HEADER, "MQDEAD"),
                    new MQMessageFormatModel(MQConstants.MQFMT_CICS, "MQCICS"),
                    new MQMessageFormatModel(MQConstants.MQFMT_IMS, "MQIMS"),
                    new MQMessageFormatModel(MQConstants.MQFMT_IMS_VAR_STRING, "MQIMSVS"),
                    new MQMessageFormatModel(MQConstants.MQFMT_STRING, "MQPCF"),
                    new MQMessageFormatModel(MQConstants.MQFMT_ADMIN, "MQADMIN"),
                    new MQMessageFormatModel(MQConstants.MQFMT_COMMAND_1, "MQCMD1"),
                    new MQMessageFormatModel(MQConstants.MQFMT_COMMAND_2, "MQCMD2"),
                    new MQMessageFormatModel(MQConstants.MQFMT_DIST_HEADER, "MQHDIST"),
                    new MQMessageFormatModel(MQConstants.MQFMT_EVENT, "MQEVENT"),
                    new MQMessageFormatModel(MQConstants.MQFMT_REF_MSG_HEADER, "MQHREF"),
                    new MQMessageFormatModel(MQConstants.MQFMT_RF_HEADER_1, "MQHRF"),
                    new MQMessageFormatModel(MQConstants.MQFMT_RF_HEADER_2, "MQHRF2"),
                    new MQMessageFormatModel(MQConstants.MQFMT_TRIGGER, "MQTRIG"),
                    new MQMessageFormatModel(MQConstants.MQFMT_XMIT_Q_HEADER, "MQXMIT")
            );

    public static MQMessageFormatModel getFormatForFlag(String flag) {
        for (MQMessageFormatModel format: formats) {
            if (format.getFlag().equals(flag)) {
                return format;
            }
        }

        return null;
    }

    public static MQMessageFormatModel getFormatForValue(String value) {
        for (MQMessageFormatModel format: formats) {
            if (format.getValue().equals(value)) {
                return format;
            }
        }

        return null;
    }
}

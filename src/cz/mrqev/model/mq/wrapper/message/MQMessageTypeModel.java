package cz.mrqev.model.mq.wrapper.message;

import lombok.Value;
import org.apache.commons.lang3.StringUtils;

@Value
public class MQMessageTypeModel {
    private int flag;
    private String value;

    public static MQMessageTypeModel valueOf(String value) {
        if (StringUtils.isNumeric(value)) {
            return MQMessageType.getTypeForFlag(Integer.parseInt(value));
        }

        return MQMessageType.getTypeForValue(value);
    }

    @Override
    public String toString() {
        return flag + " - " + value;
    }
}

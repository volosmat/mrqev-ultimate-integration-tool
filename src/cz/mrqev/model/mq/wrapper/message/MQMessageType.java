package cz.mrqev.model.mq.wrapper.message;

import com.ibm.mq.constants.MQConstants;

import java.util.List;

public class MQMessageType {
    public static List<MQMessageTypeModel> types =
            List.of(
                    new MQMessageTypeModel(MQConstants.MQMT_REQUEST, "Request"),
                    new MQMessageTypeModel(MQConstants.MQMT_REPLY, "Reply"),
                    new MQMessageTypeModel(MQConstants.MQMT_REPORT, "Report"),
                    new MQMessageTypeModel(MQConstants.MQMT_DATAGRAM, "Datagram")
            );

    public static MQMessageTypeModel getTypeForFlag(int flag) {
        for (MQMessageTypeModel type: types) {
            if (type.getFlag() == flag) {
                return type;
            }
        }

        return null;
    }

    public static MQMessageTypeModel getTypeForValue(String value) {
        for (MQMessageTypeModel type: types) {
            if (type.toString().equals(value)) {
                return type;
            }
        }

        return null;
    }
}
package cz.mrqev.model.mq.wrapper.message;

import com.google.gson.annotations.SerializedName;
import com.ibm.mq.MQMessage;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.codec.binary.Hex;
import cz.mrqev.view.MQDialogBox;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class MQMessageWrapper {

    // Don't serialize
    private transient MQMessage inputMessage;
    @Getter @Setter
    private transient int position;

    // Serialize
    @Getter @Setter
    @SerializedName("data")
    private String data;
    @Getter
    @SerializedName("mqmd")
    private MqmdWrapper mqmd;

    public MQMessageWrapper(MQMessage inputMessage) {
        try {
            this.inputMessage = inputMessage;
            this.mqmd = new MqmdWrapper(inputMessage);

            byte[] b = new byte[inputMessage.getMessageLength()];
            inputMessage.readFully(b);
            this.data = new String(b, StandardCharsets.UTF_8).replace("\n", "").replace("\r", "");
        } catch (IOException e) {
            MQDialogBox.openException(e);
        }
    }

    public MQMessageWrapper(String data, MqmdWrapper mqmd) {
        this.data = data;
        this.mqmd = mqmd;
    }

    public MQMessage getInputMessage() {
        return inputMessage;
    }

    public int getLength() {
        return mqmd.getLength();
    }

    public String getDateTime() {
        return mqmd.getDateTime();
    }

    public int getVersion() {
        return mqmd.getVersion();
    }

    public byte[] getMessageId() {
        return mqmd.getMessageId();
    }

    public String getMessageIdAsString() {
        return Hex.encodeHexString(mqmd.getMessageId());
    }

    public byte[] getCorrelId() {
        return mqmd.getCorrelId();
    }

    public String getCorrelIdAsString() {
        return Hex.encodeHexString(mqmd.getCorrelId());
    }

    public int getEncoding() {
        return mqmd.getEncoding();
    }

    public int getExpiry() {
        return mqmd.getExpiry();
    }

    public int getMessageType() {
        return mqmd.getMessageType();
    }

    public String getMessageTypeAsString() {
        MQMessageTypeModel type = MQMessageType.getTypeForFlag(mqmd.getMessageType());

        return (type != null) ? type.toString() : "";
    }

    public int getReport() {
        return mqmd.getReport();
    }

    public int getFeedback() {
        return mqmd.getFeedback();
    }

    public int getCharacterSet() {
        return mqmd.getCharacterSet();
    }

    public String getFormat() {
        return mqmd.getFormat();
    }

    public int getPriority() {
        return mqmd.getPriority();
    }

    public int getPersistance() {
        return mqmd.getPersistance();
    }

    public int getBackoutCount() {
        return mqmd.getBackoutCount();
    }

    public String getReplyToQueue() {
        return mqmd.getReplyToQueue();
    }

    public String getReplyToQueueManager() {
        return mqmd.getReplyToQueueManager();
    }

    public String getUserIdentifier() {
        return mqmd.getUserIdentifier();
    }

    public byte[] getAccountingToken() {
        return mqmd.getAccountingToken();
    }

    public String getApplicationIdData() {
        return mqmd.getApplicationIdData();
    }

    public String getPutApplicationName() {
        return mqmd.getPutApplicationName();
    }

    public int getPutApplicationType() {
        return mqmd.getPutApplicationType();
    }

    public String getApplicationOriginData() {
        return mqmd.getApplicationOriginData();
    }

    public byte[] getGroupId() {
        return mqmd.getGroupId();
    }

    public int getMessageSequenceNumber() {
        return mqmd.getMessageSequenceNumber();
    }

    public int getOffset() {
        return mqmd.getOffset();
    }

    public int getMessageFlags() {
        return mqmd.getMessageFlags();
    }

    public int getOriginalLength() {
        return mqmd.getOriginalLength();
    }

    public String printMqmd() {
        return mqmd.toString();
    }

    public String toString() { return data.replace("\\n", ""); }
}

package cz.mrqev.model.mq.wrapper.transport;

import com.google.gson.annotations.SerializedName;
import com.ibm.mq.MQMessage;
import lombok.Getter;
import org.apache.commons.codec.binary.Hex;
import cz.mrqev.view.MQDialogBox;

import java.io.IOException;
import java.text.SimpleDateFormat;

public class MqmdWrapper {

    // Don't serialize
    private transient SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
    @Getter
    private transient int length;
    @Getter
    private transient String dateTime;

    // Serialize
    @Getter
    @SerializedName("version")
    private int version;
    @Getter
    @SerializedName("messageId")
    private byte[] messageId;
    @Getter
    @SerializedName("correlId")
    private byte[] correlId;
    @Getter
    @SerializedName("encoding")
    private int encoding;
    @Getter
    @SerializedName("expiry")
    private int expiry;
    @Getter
    @SerializedName("messageType")
    private int messageType;
    @Getter
    @SerializedName("report")
    private int report;
    @Getter
    @SerializedName("feedback")
    private int feedback;
    @Getter
    @SerializedName("characterSet")
    private int characterSet;
    @Getter
    @SerializedName("format")
    private String format;
    @Getter
    @SerializedName("priority")
    private int priority;
    @Getter
    @SerializedName("persistance")
    private int persistance;
    @Getter
    @SerializedName("backoutCount")
    private int backoutCount;
    @Getter
    @SerializedName("replyToQueue")
    private String replyToQueue;
    @Getter
    @SerializedName("replyToQueueManager")
    private String replyToQueueManager;
    @Getter
    @SerializedName("userIdentifier")
    private String userIdentifier;
    @Getter
    @SerializedName("accountingToken")
    private byte[] accountingToken;
    @Getter
    @SerializedName("applicationIdData")
    private String applicationIdData;
    @Getter
    @SerializedName("putApplicationName")
    private String putApplicationName;
    @Getter
    @SerializedName("putApplicationType")
    private int putApplicationType;
    @Getter
    @SerializedName("applicationOriginData")
    private String applicationOriginData;
    @Getter
    @SerializedName("groupId")
    private byte[] groupId;
    @Getter
    @SerializedName("messageSequenceNumber")
    private int messageSequenceNumber;
    @Getter
    @SerializedName("offset")
    private int offset;
    @Getter
    @SerializedName("messageFlags")
    private int messageFlags;
    @Getter
    @SerializedName("originalLength")
    private int originalLength;

    public MqmdWrapper(MQMessage inputMessage) {
        try {
            this.version = inputMessage.getVersion();

            if (inputMessage.putDateTime != null) {
                this.dateTime = dateFormat.format(inputMessage.putDateTime.getTime());
            }

            this.length = inputMessage.getMessageLength();
            this.messageId = inputMessage.messageId;
            this.correlId = inputMessage.correlationId;
            this.encoding = inputMessage.encoding;
            this.report = inputMessage.report;
            this.feedback = inputMessage.feedback;
            this.characterSet = inputMessage.characterSet;
            this.format = inputMessage.format;
            this.priority = inputMessage.priority;
            this.persistance = inputMessage.persistence;
            this.backoutCount = inputMessage.backoutCount;
            this.replyToQueue = inputMessage.replyToQueueName;
            this.replyToQueueManager = inputMessage.replyToQueueManagerName;
            this.userIdentifier = inputMessage.userId;
            this.accountingToken = inputMessage.accountingToken;
            this.applicationIdData = inputMessage.applicationIdData;
            this.putApplicationName = inputMessage.putApplicationName;
            this.putApplicationType = inputMessage.putApplicationType;
            this.applicationOriginData = inputMessage.applicationOriginData;
            this.groupId = inputMessage.groupId;
            this.messageSequenceNumber = inputMessage.messageSequenceNumber;
            this.offset = inputMessage.offset;
            this.messageFlags = inputMessage.messageFlags;
            this.originalLength = inputMessage.originalLength;
            this.expiry = inputMessage.expiry;
            this.messageType = inputMessage.messageType;
        } catch (IOException e) {
            MQDialogBox.openException(e);
        }
    }

    @Override
    public String toString() {
        return "Version=" + version + "\n" +
                "Report=" + report + "\n" +
                "MsgType=" + messageType + "\n" +
                "Expiry=" + expiry + "\n" +
                "Feedback=" + feedback + "\n" +
                "Encoding=" + encoding + "\n" +
                "CodedCharSetId=" + characterSet + "\n" +
                "Format=" + format + "\n" +
                "Priority=" + priority + "\n" +
                "Persistence=" + persistance + "\n" +
                "MsgId=" + Hex.encodeHexString(messageId) + "\n" +
                "CorrelId=" + Hex.encodeHexString(correlId) + "\n" +
                "BackoutCount=" + backoutCount + "\n" +
                "ReplyToQ=" + replyToQueue + "\n" +
                "ReplyToQMgr=" + replyToQueueManager + "\n" +
                "UserIdentifier=" + userIdentifier + "\n" +
                "AccountingToken=" + Hex.encodeHexString(accountingToken) + "\n" +
                "ApplIdentityData=" + applicationIdData + "\n" +
                "PutApplType=" + putApplicationType + "\n" +
                "PutApplName=" + putApplicationName + "\n" +
                "PutDateTime=" + dateTime + "\n" +
                "ApplOriginData=" + applicationOriginData + "\n" +
                "GroupId=" + Hex.encodeHexString(groupId) + "\n" +
                "MsgSeqNumber=" + messageSequenceNumber + "\n" +
                "Offset=" + offset + "\n" +
                "MsgFlags=" + messageFlags + "\n" +
                "OriginalLength=" + originalLength;
    }
}

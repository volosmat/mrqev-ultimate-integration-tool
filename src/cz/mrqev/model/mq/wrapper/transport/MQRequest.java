package cz.mrqev.model.mq.wrapper.transport;

import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.transport.Request;
import lombok.Getter;

public class MQRequest extends Request {
    @Getter
    private MqmdWrapper mqmd;

    public MQRequest(MQMessageWrapper message, boolean fillTimestamps, boolean generateSid, String username, String password) {
        super(message.getData(), fillTimestamps, generateSid, username, password);
        this.mqmd = message.getMqmd();
    }

    public MQRequest(MQMessageWrapper message, boolean fillTimestamps, boolean generateSid) {
        super(message.getData(), fillTimestamps, generateSid);
        this.mqmd = message.getMqmd();
    }
}

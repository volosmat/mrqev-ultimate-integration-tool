package cz.mrqev.model.mq.actions;

import com.ibm.mq.*;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.controller.tab.MqTabController;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.mq.connection.MQConnection;

import java.io.IOException;
import java.nio.charset.StandardCharsets;


public class MQReader {
    private MQQueue queue;

    public MQReader(MQQueue queue) {
        this.queue = queue;
    }

    public MQMessageWrapper browseMessage(MQGetMessageOptions options) throws MQException {
        MQMessage message = new MQMessage();
        this.queue.get(message, options);

        return new MQMessageWrapper(message);
    }

    public static String replyCheckerByCorrelId(MQConnection connection, String queueName, int waitTime, byte[] CorrelId) {
        try {
            // access queue
            int openOptions = MQConstants.MQOO_INPUT_SHARED + MQConstants.MQOO_INQUIRE + MQConstants.MQOO_FAIL_IF_QUIESCING;
            MQQueue queue = connection.accessQueue(queueName, openOptions);

            MQMessage currentMessage = new MQMessage();
            currentMessage.correlationId = CorrelId;
            MQGetMessageOptions gmo = new MQGetMessageOptions();
            gmo.options = MQConstants.MQGMO_WAIT;
            gmo.waitInterval = waitTime * 1000;
            gmo.matchOptions = MQConstants.MQMO_MATCH_CORREL_ID;
            queue.get(currentMessage, gmo);
            byte[] b = new byte[currentMessage.getMessageLength()];
            currentMessage.readFully(b);
            currentMessage.clearMessage();
            connection.close();
            return new String(b, StandardCharsets.UTF_8);
        }
        catch (MQException e)
        {
            if (e.reasonCode == 2033) {
                if (MqTabController.getInstance() != null) {
                    MqTabController.getInstance().addLogMessage("No message received within 60s.", true);
                } else {
                    System.out.println("No message received within 60s.");
                }
                return null;
            }
        } catch (IOException e) {
            return e.getMessage();
        }
        return null;
    }
}

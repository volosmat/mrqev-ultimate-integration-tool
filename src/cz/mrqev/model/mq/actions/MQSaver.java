package cz.mrqev.model.mq.actions;

import com.google.gson.Gson;
import com.ibm.mq.MQException;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.mq.connection.MQConnectionEntry;
import cz.mrqev.view.MQDialogBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.BiConsumer;

public class MQSaver {

    private String filePath;
    private MQBrowser browser;
    private ArrayList<MQMessageWrapper> messages;
    private BiConsumer<String, Boolean> log;

    public MQSaver(String filePath, MQBrowser browser, BiConsumer<String, Boolean> log) {
        this.filePath = filePath;
        this.browser = browser;
        this.log = log;
    }

    public MQSaver(String filePath, MQBrowser browser) {
        this.filePath = filePath;
        this.browser = browser;
    }

    public MQSaver(String filePath, ArrayList<MQMessageWrapper> messages) {
        this.filePath = filePath;;
        this.messages = messages;
    }

    public MQSaver(String filePath, ArrayList<MQMessageWrapper> messages, BiConsumer<String, Boolean> log) {
        this.filePath = filePath;
        this.messages = messages;
        this.log = log;
    }

    public MQSaver(String filePath, MQConnectionEntry connectionEntry, String queueString, BiConsumer<String, Boolean> log) {
        this.filePath = filePath;
        this.browser = new MQBrowser(connectionEntry, queueString, log);
        this.log = log;
    }

    public MQSaver(String filePath, MQConnectionEntry connectionEntry, String queueString, int browseCount, String filter, BiConsumer<String, Boolean> log) {
        this.filePath = filePath;
        this.browser = new MQBrowser(connectionEntry, queueString, browseCount, filter, log);
        this.log = log;
    }

    public void save() {
        try {
            if (messages == null) {
                messages = browser.browseMessages();
            }

            Gson gson = new Gson();
            String json = gson.toJson(messages);

            UtilsManager.storeToFile(filePath, json);
            if (log != null) {
                log.accept("Queue was successfully saved to file: " + filePath, true);
            }
        } catch (IOException | MQException e) {
            if (Main.calledThroughCommandLine) {
                e.printStackTrace();
            } else {
                MQDialogBox.openException(e);
            }

            if (log != null) {
                log.accept("Error saving queue: " + e.toString(), true);
            }
        }
    }
}

package cz.mrqev.model.mq.actions;

import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.mq.connection.MQConnectionEntry;

import java.util.ArrayList;
import java.util.function.BiConsumer;

public class MQBrowser {
    private boolean browseInProgress = false;
    private MQConnectionEntry connectionEntry;
    private String queueString;
    private int browseCount;
    private String filter;
    private BiConsumer<String, Boolean> log;

    public MQBrowser(MQConnectionEntry connectionEntry, String queueString, int browseCount, String filter) {
        this.connectionEntry = connectionEntry;
        this.queueString = queueString;
        this.browseCount = browseCount;
        this.filter = filter;
    }

    public MQBrowser(MQConnectionEntry connectionEntry, String queueString, BiConsumer<String, Boolean> log) {
        this.connectionEntry = connectionEntry;
        this.queueString = queueString;
        this.browseCount = 0;
        this.filter = "";
        this.log = log;
    }

    public MQBrowser(MQConnectionEntry connectionEntry, String queueString, int browseCount, String filter, BiConsumer<String, Boolean> log) {
        this.connectionEntry = connectionEntry;
        this.queueString = queueString;
        this.browseCount = browseCount;
        this.filter = filter;
        this.log = log;
    }

    public ArrayList<MQMessageWrapper> browseMessages() throws MQException {
        ArrayList<MQMessageWrapper> result = new ArrayList<>();
        MQConnection connection = null;
        try {
            browseInProgress = true;
            // initialize mq connection
            connection = new MQConnection(connectionEntry);

            int openOptions = MQConstants.MQOO_BROWSE | MQConstants.MQOO_INQUIRE | MQConstants.MQOO_READ_AHEAD;
            // access queue
            MQQueue queue = connection.accessQueue(queueString, openOptions);
            // initialize mq reader
            MQReader reader = new MQReader(queue);

            MQGetMessageOptions getMessageOptions = new MQGetMessageOptions();
            getMessageOptions.options = MQConstants.MQGMO_BROWSE_FIRST;// + MQGMO_CONVERT;
            getMessageOptions.matchOptions = MQConstants.MQMO_NONE;

            if (log != null) {
                log.accept("---------------------------------------------------------------", false);
                log.accept("browsing " + queue.getCurrentDepth() + " messages on queue " + queueString, true);
            }

            int i;
            int queueDepth = queue.getCurrentDepth();

            for (i = 1; i <= queueDepth; i++) {
                // check break condition
                if (i > browseCount && browseCount != 0 || !browseInProgress) {
                    break;
                }
                // browse message
                MQMessageWrapper newMessage = reader.browseMessage(getMessageOptions);
                if (filter.equals("") || newMessage.getData().contains(filter) || newMessage.getCorrelIdAsString().equals(filter) || newMessage.getMessageIdAsString().contains(filter)) {
                    newMessage.setPosition(i);
                    result.add(newMessage);
                }
                getMessageOptions.options = MQConstants.MQGMO_BROWSE_NEXT;

                if (i % 50 == 0 && log != null) {
                    log.accept("browsed " + i + " messages from queue " + queueString, true);
                }
            }

            if (log != null) {
                log.accept("browsed " + (i - 1) + " messages from queue " + queueString, true);
                log.accept("ended browsing queue " + queueString, true);
                log.accept("---------------------------------------------------------------", false);
            }

            connection.close();
            browseInProgress = false;

        } catch (MQException e) {
            browseInProgress = false;
            if (e.reasonCode != 2033) {
                throw e;
            } else if (log != null) {
                log.accept("ended browsing queue " + queueString, true);
                log.accept("---------------------------------------------------------------", false);
            }
        } finally {
            if (connection != null)
                connection.close();

            browseInProgress = false;
        }

        return result;
    }

    public void setBrowseInProgress(boolean browseInProgress) {
        this.browseInProgress = browseInProgress;
    }
}

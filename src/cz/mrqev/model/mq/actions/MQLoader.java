package cz.mrqev.model.mq.actions;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ibm.mq.MQException;
import com.ibm.mq.MQQueue;
import com.ibm.mq.constants.MQConstants;
import cz.mrqev.controller.tab.MqmdTabController;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import cz.mrqev.model.mq.wrapper.transport.MQRequest;
import cz.mrqev.model.mq.connection.MQConnection;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.function.Consumer;

public class MQLoader {

    private String targetQueue;
    private String targetQueueMgr;
    private ArrayList<MQRequest> requests = new ArrayList<>();
    private Consumer<String> log;


    public MQLoader(ArrayList<MQRequest> requests, String targetQueue, String targetQueueMgr, Consumer<String> log) {
        this.requests = requests;
        this.targetQueue = targetQueue;
        this.targetQueueMgr = targetQueueMgr;
        this.log = log;
    }

    public MQLoader(String queueFile, String targetQueue, String targetQueueMgr, Consumer<String> log) {
        try {
            String fileContent = UtilsManager.loadFromFile(queueFile);
            ArrayList<MQMessageWrapper> messages = parseQueueExport(fileContent);
            for (MQMessageWrapper message: messages) {
                requests.add(new MQRequest(message, false, false));
            }
        } catch (IOException e) {
            e.printStackTrace();
            if (log != null) {
                log.accept("Error loading queue: " + e.toString());
            }
        }

        this.targetQueue = targetQueue;
        this.targetQueueMgr = targetQueueMgr;
        this.log = log;
    }

    public void load() {
        if (log != null) {
            log.accept("Loading " + requests.size() + " messages to queue " + targetQueue + " on " + targetQueueMgr);
        }

        MQQueue queue = null;
        MQConnection connection = null;

        try {
            connection = new MQConnection(Main.configManager.MQConnections.stream().filter(a -> a.getQueueManager().equals(targetQueueMgr)).findFirst().get());
            // access queue
            int openOptions = MQConstants.MQOO_OUTPUT + MQConstants.MQOO_FAIL_IF_QUIESCING;
            queue = connection.accessQueue(targetQueue, openOptions);

            MQWriter writer = new MQWriter(queue, requests, 0);
            writer.putAll(log);

        } catch (MQException e) {
            log.accept("Load queue ended unexpectedly with error: " + e.getMessage());
        } catch (IOException e) {
            log.accept("Load queue ended unexpectedly. An MQ IO error occurred : " + e);
        } finally {
            try {
                if (queue != null)
                    queue.close();
                if (connection != null)
                    connection.close();
            } catch (MQException e) {
                e.printStackTrace();
            }
        }
    }

    public static ArrayList<MQMessageWrapper> parseQueueExport(String exportedJson) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<MQMessageWrapper>>(){}.getType();
        return gson.fromJson(exportedJson, type);
    }

    public static ArrayList<MQMessageWrapper> parseDataFile(String dataFile) {
        ArrayList<MQMessageWrapper> result = new ArrayList<>();
        // we have no other change to get mqmd from ui
        MqmdWrapper mqmd = MqmdTabController.getInstance().wrapMqmd();

        String[] messageData = dataFile.split("\\r?\\n");
        for (String message: messageData) {
            result.add(new MQMessageWrapper(message, mqmd));
        }

        return result;
    }
}

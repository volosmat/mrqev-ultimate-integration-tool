package cz.mrqev.model.mq.actions;

import com.ibm.mq.*;
import cz.mrqev.model.mq.wrapper.transport.MQRequest;
import cz.mrqev.model.mq.wrapper.transport.MqmdWrapper;
import cz.mrqev.view.MQDialogBox;

import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;

public class MQWriter {
    // queue where messages will be inserted
    private MQQueue queue;
    // requests
    private ArrayList<MQRequest> requests = new ArrayList<>();

    public boolean putInProgress = false;
    private long waitTime = 0;

    /*
    MQMessage mqMessage = new MQMessage();
mqMessage.format = CMQC.MQFMT_RF_HEADER_2;
MQRFH2 mqrfh2 = new MQRFH2();
mqrfh2.setFieldValue("psc", CMQPSC.MQPSC_COMMAND, CMQPSC.MQPSC_REGISTER_SUBSCRIBER);
mqrfh2.setFieldValue("psc", CMQPSC.MQPSC_TOPIC, "WR/FLAGS/" + params.getTopicString());
mqrfh2.setFieldValue("psc", CMQPSC.MQPSC_SUBSCRIPTION_NAME, params.getTopicName());
mqrfh2.setFieldValue("psc", CMQPSC.MQPSC_Q_NAME, "CEBPORTAL.AS1.PUSH.EVENT.OUTPUT");
mqrfh2.write(mqMessage);
mqQueue.put(mqMessage);
https://stackoverflow.com/questions/49616262/issue-while-setting-mqrfh2-header-in-ibm-mq?rq=1
     */
    public MQWriter(MQQueue queue) {
        this.queue = queue;
    }

    public MQWriter(MQQueue queue, long waitTime) {
        this.queue = queue;
        this.waitTime = waitTime;
    }

    public MQWriter(MQQueue queue, MQRequest request, long waitTime) {
        this.queue = queue;
        this.waitTime = waitTime;
        this.requests.add(request);
    }

    public MQWriter(MQQueue queue, ArrayList<MQRequest> requests, long waitTime) {
        this.queue = queue;
        this.waitTime = waitTime;
        this.requests = requests;
    }

    public MQWriter(MQQueue queue, MQRequest request) {
        this.queue = queue;
        this.requests.add(request);
    }

    public void addRequest(MQRequest request) {
        requests.add(request);
    }

    public byte[] put() throws MQException, IOException {
        putInProgress = true;

        if (!requests.isEmpty()) {
            return putAll(null).get(0);
        }

        return null;
    }

    public ArrayList<byte[]> putAll(Consumer<String> log) throws MQException, IOException {
        ArrayList<byte[]> messageIds = new ArrayList<>();

        for (int i=0; i<requests.size(); i++) {
            if (!putInProgress) break;

            MQRequest request = requests.get(i);
            MQMessage message = prepareMessage(request.getMqmd());
            message.writeString(request.getDataForPut());

            MQPutMessageOptions pmo = new MQPutMessageOptions();
            queue.put(message, pmo);
            messageIds.add(message.messageId);

            if (log != null) {
                if (request.getSid() != null) {
                    log.accept("Putting message: " + (i + 1) + "/" + requests.size() + " with SID: " + request.getSid() + ".");
                } else {
                    log.accept("Putting message: " + (i + 1) + "/" + requests.size() + ".");
                }
            }

            // wait after put
            try {
                if (waitTime > 0) Thread.sleep(waitTime);
            } catch (InterruptedException | NumberFormatException e) {
                MQDialogBox.openException(e);
                break;
            }
        }

        return messageIds;
    }

    private MQMessage prepareMessage(MqmdWrapper mqmd) throws MQException {
        MQMessage sendmsg = new MQMessage();

        sendmsg.setVersion(mqmd.getVersion());
        sendmsg.messageId = mqmd.getMessageId();
        sendmsg.correlationId = mqmd.getCorrelId();
        sendmsg.encoding = mqmd.getEncoding();
        sendmsg.expiry = mqmd.getExpiry();
        sendmsg.messageType = mqmd.getMessageType();
        sendmsg.report = mqmd.getReport();
        sendmsg.feedback = mqmd.getFeedback();
        sendmsg.characterSet = mqmd.getCharacterSet();
        sendmsg.format = mqmd.getFormat();
        sendmsg.priority = mqmd.getPriority();
        sendmsg.persistence = mqmd.getPersistance();
        sendmsg.backoutCount = mqmd.getBackoutCount();
        sendmsg.replyToQueueName = mqmd.getReplyToQueue();
        sendmsg.replyToQueueManagerName = mqmd.getReplyToQueueManager();
        sendmsg.userId = mqmd.getUserIdentifier();
        sendmsg.accountingToken = mqmd.getAccountingToken();
        sendmsg.applicationIdData = mqmd.getApplicationIdData();
        sendmsg.putApplicationName = mqmd.getPutApplicationName();
        sendmsg.putApplicationType = mqmd.getPutApplicationType();
        sendmsg.applicationOriginData = mqmd.getApplicationOriginData();
        sendmsg.groupId = mqmd.getGroupId();
        sendmsg.messageSequenceNumber = mqmd.getMessageSequenceNumber();
        sendmsg.offset = mqmd.getOffset();
        sendmsg.messageFlags = mqmd.getMessageFlags();
        sendmsg.originalLength = mqmd.getOriginalLength();
        return sendmsg;
    }
}

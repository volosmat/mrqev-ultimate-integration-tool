package cz.mrqev.model.mq.connection;

import cz.mrqev.main.Main;
import cz.mrqev.model.transport.ssl.Keystore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class MQConnectionEntry {
    @Getter
    private String queueManager;
    @Getter
    private String hostname;
    @Getter
    private String port;
    @Getter
    private String channel;
    @Getter
    private String userId;
    @Getter
    private String userPassword;
    @Getter @Setter
    private String cipherSuite;
    @Setter
    private String keyStore;

    public MQConnectionEntry(String queueManager, String hostname, String port, String channel, String userId, String userPassword) {
        this.queueManager = queueManager;
        this.hostname = hostname;
        this.port = port;
        this.userId = userId;
        this.channel = channel;
        this.userPassword = userPassword;
    }

    public MQConnectionEntry(String queueManager, String hostname, String port, String channel, String userId) {
        this.queueManager = queueManager;
        this.hostname = hostname;
        this.port = port;
        this.userId = userId;
        this.channel = channel;
    }

    public Keystore getKeyStore() {
        return Main.configManager.keystores.get(keyStore);
    }
}

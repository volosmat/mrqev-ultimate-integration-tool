package cz.mrqev.model.mq.connection;

import com.ibm.mq.*;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.pcf.PCFMessage;
import com.ibm.mq.pcf.PCFMessageAgent;
import cz.mrqev.main.Main;
import cz.mrqev.view.MQDialogBox;

import javax.net.ssl.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.cert.X509Certificate;
import java.text.Collator;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MQConnection {
    private MQConnectionEntry connectionEntry;
    private MQQueueManager queueManager;
    // variables for multi-thread queue load
    public boolean queueLoadInProgress = false;
    public String queueLoadFromQM = null;

    public MQConnection(MQConnectionEntry connectionEntry) throws MQException {
        this.connectionEntry = connectionEntry;
        this.updateMqEnvironment();
    }

    public MQConnection(String sessionString) throws MQException {
        String pattern = "(.*?) - \\((.*?)@(.*?):(\\d+?)\\/(.*?)\\)";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(sessionString);

        if (m.find()) {
            // TODO mozno pridat aj moznost hesla ale trebalo by zmenit spôsob ukladanie QM do session
            connectionEntry = new MQConnectionEntry(m.group(1), m.group(3), m.group(4), m.group(5), m.group(2));
        }
        updateMqEnvironment();
    }

    public MQConnectionEntry getSelectedConnectionEntry() {
        return this.connectionEntry;
    }

    public void updateSelectedQueueManager(int position) throws MQException {
        this.connectionEntry = Main.configManager.MQConnections.get(position);
        this.updateMqEnvironment();
    }

    private void updateMqEnvironment() throws MQException {
        MQEnvironment.hostname = this.connectionEntry.getHostname();
        MQEnvironment.port = Integer.parseInt(this.connectionEntry.getPort());
        MQEnvironment.channel = this.connectionEntry.getChannel();
        MQEnvironment.userID = this.connectionEntry.getUserId();
        MQEnvironment.password = this.connectionEntry.getUserPassword();

        if (connectionEntry.getKeyStore() != null) {
            MQEnvironment.sslCipherSuite = connectionEntry.getCipherSuite();
            MQEnvironment.sslSocketFactory = createSSLContext().getSocketFactory();
        } else {
            MQEnvironment.sslCipherSuite = null;
            MQEnvironment.sslSocketFactory = null;
        }

        this.queueManager = new MQQueueManager(this.connectionEntry.getQueueManager());
    }

    public MQQueueManager getQueueManager() {
        return this.queueManager;
    }

    public MQQueue accessQueue(String queueName, int options) throws MQException {
        return this.queueManager.accessQueue(queueName, options,
                null, // default q manager
                null, // no dynamic q name
                null);
    }

    public TreeSet<String> loadQueues() throws IOException, MQException {
        PCFMessageAgent agent = new PCFMessageAgent(this.queueManager);
        PCFMessage request = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q);
        request.addParameter(CMQC.MQCA_Q_NAME, "*");
        request.addParameter(CMQC.MQIA_Q_TYPE, CMQC.MQQT_ALL);
        request.addParameter(CMQCFC.MQIACF_Q_ATTRS, new int[] {CMQC.MQCA_Q_NAME});

        PCFMessage[] response = agent.send(request);

        TreeSet<String> queues = new TreeSet<>(Collator.getInstance());
        for (PCFMessage aResponse : response) {
            System.out.println(aResponse.getStringParameterValue(CMQC.MQCA_Q_NAME));
            queues.add(aResponse.getStringParameterValue(CMQC.MQCA_Q_NAME));
        }

        return queues;
    }

    public void close() throws MQException {
        this.queueManager.close();
    }

    private SSLContext createSSLContext(){
        try{
            TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] chain, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) {
                }
            } };

            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream(connectionEntry.getKeyStore().getKeystoreFile()),connectionEntry.getKeyStore().getKeystorePassword().toCharArray());

            // Create key manager
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(keyStore, connectionEntry.getKeyStore().getKeystorePassword().toCharArray());
            KeyManager[] km = keyManagerFactory.getKeyManagers();

            // Initialize SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(km,  byPassTrustManagers, null);

            return sslContext;
        } catch (Exception e){
            MQDialogBox.openException(e);
        }

        return null;
    }
}

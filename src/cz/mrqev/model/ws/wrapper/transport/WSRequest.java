package cz.mrqev.model.ws.wrapper.transport;

import cz.mrqev.manager.WSSManager;
import cz.mrqev.model.transport.Request;

import java.util.Map;

public class WSRequest extends Request {
    private HttpHeader httpHeader;
    private boolean soap;

    public WSRequest(String data, boolean fillTimestamps, boolean generateSid, boolean soap, String username, String password, HttpHeader httpHeader) {
        super(data, fillTimestamps, generateSid, username, password);
        // create hard copy of header
        this.httpHeader = new HttpHeader(httpHeader);
        this.soap = soap;
        fillHttpHeader();
    }

    public WSRequest(String data, boolean fillTimestamps, boolean generateSid, boolean soap, HttpHeader httpHeader) {
        super(data, fillTimestamps, generateSid);
        this.httpHeader = httpHeader;
        this.soap = soap;
        fillHttpHeader();
    }

    public HttpHeader getHttpHeader() {
        return httpHeader;
    }

    public String getDataForPut()  {
        // set base request
        String putData = data;

        if (fillTimestamps) {
            putData = placeholderManager.getTimestampPlaceholder().replace(putData);
        }

        if (generateSid) {
            putData = placeholderManager.getUidPlaceholder().replace(putData, sid);
        }

        if (addWss && soap) {
            putData = this.fillWssToRequest(putData);
        }

        return putData;
    }

    private void fillHttpHeader() {
        if (addWss && !soap) {
            WSSManager wssManager = new WSSManager(username, password);
            wssManager.addWssToHttpHeader(httpHeader);
        }

        for (Map.Entry<String, String> property : httpHeader.getProperties().entrySet()) {
            String name = property.getKey();
            String value = property.getValue();
            // remove sid placeholder
            if (generateSid) {
                value = placeholderManager.getUidPlaceholder().replace(property.getValue());
            }
            // remove timestamp placeholder
            if (fillTimestamps) {
                value = placeholderManager.getTimestampPlaceholder().replace(value);
            }

            httpHeader.saveProperty(name, value);
        }
    }
}

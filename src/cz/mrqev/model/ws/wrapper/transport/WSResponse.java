package cz.mrqev.model.ws.wrapper.transport;

import cz.mrqev.model.transport.Response;

public class WSResponse extends Response {
    private int webServiceResponseCode;

    public WSResponse(int webServiceResponseCode, String responseData) {
        super(responseData);
        this.webServiceResponseCode = webServiceResponseCode;
    }

    public int getWebServiceResponseCode() {
        return webServiceResponseCode;
    }

}

package cz.mrqev.model.ws.wrapper.transport;

import com.google.gson.internal.LinkedTreeMap;
import cz.mrqev.manager.UtilsManager;

import java.net.HttpURLConnection;
import java.util.*;

public class HttpHeader {
    private HashMap<String, String> properties = new LinkedHashMap<>();

    public HttpHeader(HttpHeader httpHeader) {
        this.properties = httpHeader.getProperties();
    }

    public HttpHeader() {}

    public void saveProperty(String name, String value) {
        if (name != null && value != null && !name.isEmpty()) {
            properties.put(name, value);
        }
    }

    public void saveProperties(LinkedTreeMap<String, String> propertiesToSave) {
        for (Map.Entry property: propertiesToSave.entrySet()) {
            saveProperty((String)property.getKey(), (String)property.getValue());
        }
    }

    public String getProperty(Object name) {
        return properties.get(name);
    }

    public boolean containsKey(Object name) {
        return properties.containsKey(name);
    }

    public void removeProperty(String name) {
        properties.remove(name);
    }

    public HashMap<String, String> getProperties() {
        return properties;
    }

    public ArrayList<String> getPropertiesArray() {
        ArrayList<String> result = new ArrayList<>();

        for (Map.Entry<String, String> property : properties.entrySet()) {
            String name = property.getKey();
            String value = property.getValue().replace("\n", "").replace("\r", "");

            result.add(name + ": " + value);
        }

        return result;
    }

    public HttpURLConnection addToConnection(HttpURLConnection httpConnection) {
        for (Map.Entry<String, String> property : properties.entrySet()) {
            String name = property.getKey();
            // newline is not allowed character for http header
            String value = property.getValue().replace("\n", "").replace("\r", "");

            httpConnection.addRequestProperty(name, value);
        }

        return httpConnection;
    }

    public static Map<String, String> getDefaultPropertyList() {
        return Map.ofEntries(
                Map.entry("Accept", "text/plain"),
                Map.entry("Accept-Charset", "utf-8"),
                Map.entry("Accept-Encoding", "gzip, deflate"),
                Map.entry("Accept-Language", "en-US"),
                Map.entry("Accept-Datetime", UtilsManager.getRfc1123Datetime()),
                Map.entry("Access-Control-Request-Method", "GET"),
                Map.entry("Access-Control-Request-Headers", "GET"),
                Map.entry("Authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="),
                Map.entry("Cache-Control", "no-cache"),
                Map.entry("Connection", "keep-alive"),
                Map.entry("Cookie", "$Version=1; Skin=new;"),
                Map.entry("Content-Length", "348"),
                Map.entry("Content-MD5", "Q2hlY2sgSW50ZWdyaXR5IQ=="),
                Map.entry("Content-Type", "application/x-www-form-urlencoded"),
                Map.entry("Date", UtilsManager.getRfc1123Datetime()),
                Map.entry("Expect", "100-continue"),
                Map.entry("Forwarded", "for=192.0.2.60;proto=http;by=203.0.113.43"),
                Map.entry("From", "user@example.com"),
                Map.entry("Host", "inapp.eu:443"),
                Map.entry("metaheader", "{\"sid\":\"value\", \"buid\":\"value\", \"originalSource\": {\"user\":1006, \"system\": \"CEBBE\", \"company\":1, \"timestamp\": \"\"}}"),
                Map.entry("If-Match", "737060cd8c284d8af7ad3082f209582d"),
                Map.entry("If-Modified-Since", UtilsManager.getRfc1123Datetime()),
                Map.entry("If-None-Match", "737060cd8c284d8af7ad3082f209582d"),
                Map.entry("If-Range", "737060cd8c284d8af7ad3082f209582d"),
                Map.entry("If-Unmodified-Since", UtilsManager.getRfc1123Datetime()),
                Map.entry("Max-Forwards", "10"),
                Map.entry("Origin", "http://www.example.com"),
                Map.entry("Pragma", "no-cache"),
                Map.entry("Proxy-Authorization", "Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=="),
                Map.entry("Range", "bytes=500-999"),
                Map.entry("Referer", "http://example.com/Main_Page"),
                Map.entry("TE", "trailers, deflate"),
                Map.entry("User-Agent", "Mozilla/5.0 (X11; Linux x86_64; rv:12.0) Gecko/20100101 Firefox/12.0"),
                Map.entry("Upgrade", "HTTPS/1.3, IRC/6.9, RTA/x11, websocket"),
                Map.entry("Via", "1.0 fred, 1.1 example.com (Apache/1.1)"),
                Map.entry("Warning", "199 Miscellaneous warning"),
                Map.entry("X-Requested-With", "XMLHttpRequest"),
                Map.entry("DNT", "1"),
                Map.entry("X-Forwarded-For", "129.78.138.66, 129.78.64.103"),
                Map.entry("X-Forwarded-Host", "example.com:443"),
                Map.entry("X-Forwarded-Proto", "https"),
                Map.entry("Front-End-Https", "on"),
                Map.entry("X-Http-Method-Override", "DELETE"),
                Map.entry("X-ATT-DeviceId", "GT-P7320/P7320XXLPG"),
                Map.entry("Proxy-Connection", "keep-alive"),
                Map.entry("X-Csrf-Token", "i8XNjC4b8KVok4uw5RftR38Wgp2BFwql"),
                Map.entry("X-Request-ID", "f058ebd6-02f7-4d3f-942e-904344e8cde5"),
                Map.entry("X-Correlation-ID", "f058ebd6-02f7-4d3f-942e-904344e8cde5")
        );
    }
}

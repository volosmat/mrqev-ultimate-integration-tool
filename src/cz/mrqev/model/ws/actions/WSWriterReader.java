package cz.mrqev.model.ws.actions;

import cz.mrqev.model.ws.connection.HTTPConnection;
import cz.mrqev.model.ws.connection.HTTPSConnection;
import cz.mrqev.model.ws.wrapper.transport.WSRequest;
import cz.mrqev.model.ws.wrapper.transport.WSResponse;

import java.net.URL;


public class WSWriterReader {
    private URL url;
    private WSRequest request;
    private WSResponse response;

    public WSWriterReader(URL url, WSRequest request) {
        this.url = url;
        this.request = request;
    }

    public void callWebService() {
        if (url.toString().startsWith("http://"))
        {
            HTTPConnection connection = new HTTPConnection(url, request);
            response = connection.getResponse();
        } else if (url.toString().startsWith("https://")) {
            HTTPSConnection connection = new HTTPSConnection(url, request);
            response = connection.getResponse();
        }
    }

    public WSResponse getResponse() {
        return response;
    }
}

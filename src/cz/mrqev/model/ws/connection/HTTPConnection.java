package cz.mrqev.model.ws.connection;

import cz.mrqev.controller.tab.ReplyTabController;
import cz.mrqev.controller.tab.WsTabController;
import cz.mrqev.model.ws.wrapper.transport.WSRequest;
import cz.mrqev.model.ws.wrapper.transport.WSResponse;
import cz.mrqev.view.MQDialogBox;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

public class HTTPConnection {
    private WSResponse response;

    public HTTPConnection(URL url, WSRequest request)
    {
        try {
            URLConnection urlConnection = url.openConnection();
            HttpURLConnection httpConnection = (HttpURLConnection) urlConnection;
            request.getHttpHeader().addToConnection(httpConnection);

            httpConnection.setRequestMethod("POST");

            if (request.getMaximumWaitTime()>0) {
                httpConnection.setConnectTimeout(request.getMaximumWaitTime() * 1000);
                httpConnection.setReadTimeout(request.getMaximumWaitTime() * 1000);
            }
            else {
                httpConnection.setConnectTimeout(Integer.parseInt(ReplyTabController.getInstance().textfield_replyWaitTime.getText()) * 1000);
                httpConnection.setReadTimeout(Integer.parseInt(ReplyTabController.getInstance().textfield_replyWaitTime.getText()) * 1000);
            }

            httpConnection.setDoOutput(true);
            httpConnection.setDoInput(true);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(httpConnection.getOutputStream(), StandardCharsets.UTF_8));
            bw.write(request.getDataForPut());
            bw.flush();
            bw.close();

            BufferedReader reader;

            if (String.valueOf(httpConnection.getResponseCode()).startsWith("4") || String.valueOf(httpConnection.getResponseCode()).startsWith("5")) {
                reader = new BufferedReader(new InputStreamReader(httpConnection.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
            }

            String line, output = null;
            while ((line = reader.readLine()) != null) {
                output += line;
            }

            response = new WSResponse(httpConnection.getResponseCode(), output);

            httpConnection.disconnect();

        } catch (SocketTimeoutException e) {
            if (WsTabController.getInstance() != null)
                WsTabController.getInstance().addLogMessage("Could not connect or received response within "+ReplyTabController.getInstance().textfield_replyWaitTime.getText()+ "s.", true);
            else
                System.out.println("Could not connect or received response within "+request.getMaximumWaitTime()+"s.");
        } catch (Exception e) {
            if (WsTabController.getInstance() != null)
                MQDialogBox.openException(e);
            else
                System.out.println(e.toString());
        }
    }

    public WSResponse getResponse() {
        return response;
    }
}


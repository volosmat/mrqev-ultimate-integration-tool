package cz.mrqev.model.ws.connection;

import cz.mrqev.controller.tab.ReplyTabController;
import cz.mrqev.controller.tab.WsTabController;
import cz.mrqev.model.ws.wrapper.transport.WSRequest;
import cz.mrqev.model.ws.wrapper.transport.WSResponse;
import cz.mrqev.view.MQDialogBox;

import javax.net.ssl.*;
import java.io.*;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.X509Certificate;

public class HTTPSConnection {
    private WSResponse response;

    public HTTPSConnection(URL url, WSRequest request) {
        try {
            HostnameVerifier allHostsValid = (hostname, session) -> true;
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            SSLContext sslContext = this.createSSLContext();
            SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            URLConnection urlConnection = url.openConnection();
            HttpsURLConnection httpsConnection = (HttpsURLConnection) urlConnection;
            request.getHttpHeader().addToConnection(httpsConnection);

            httpsConnection.setSSLSocketFactory(sslSocketFactory);
            httpsConnection.setRequestMethod("POST");
            if (request.getMaximumWaitTime()>0) {
                httpsConnection.setConnectTimeout(request.getMaximumWaitTime() * 1000);
                httpsConnection.setReadTimeout(request.getMaximumWaitTime() * 1000);
            }
            else {
                httpsConnection.setConnectTimeout(Integer.parseInt(ReplyTabController.getInstance().textfield_replyWaitTime.getText()) * 1000);
                httpsConnection.setReadTimeout(Integer.parseInt(ReplyTabController.getInstance().textfield_replyWaitTime.getText()) * 1000);
            }

            httpsConnection.setDoOutput(true);
            httpsConnection.setDoInput(true);

            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(httpsConnection.getOutputStream(), StandardCharsets.UTF_8));
            bw.write(request.getDataForPut());
            bw.flush();
            bw.close();

            BufferedReader reader;

            if (String.valueOf(httpsConnection.getResponseCode()).startsWith("4") || String.valueOf(httpsConnection.getResponseCode()).startsWith("5")) {
                reader = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));
            } else {
                reader = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));
            }

            String line, output = "";
            while ((line = reader.readLine()) != null) {
                output += line;
            }

            response = new WSResponse(httpsConnection.getResponseCode(), output);

            httpsConnection.disconnect();
        } catch (SocketTimeoutException e) {
            if (WsTabController.getInstance() != null)
                WsTabController.getInstance().addLogMessage("Could not connect or received response within "+ReplyTabController.getInstance().textfield_replyWaitTime.getText()+ "s.", true);
            else
                System.out.println("Could not connect or received response within "+request.getMaximumWaitTime()+"s.");
        } catch (Exception e) {
            if (WsTabController.getInstance() != null)
                MQDialogBox.openException(e);
            else
                System.out.println(e);
        }
    }

    public WSResponse getResponse() {
        return response;
    }

    private SSLContext createSSLContext(){
        try{
            TrustManager[] byPassTrustManagers = new TrustManager[] { new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                public void checkClientTrusted(X509Certificate[] chain, String authType) {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) {
                }
            } };

            KeyStore keyStore = KeyStore.getInstance("JKS");
            keyStore.load(new FileInputStream("ThubIntAcc2.jks"),"ThubIntAcc2".toCharArray());

            // Create key manager
            KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance("SunX509");
            keyManagerFactory.init(keyStore, "ThubIntAcc2".toCharArray());
            KeyManager[] km = keyManagerFactory.getKeyManagers();

            // Initialize SSLContext
            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(km,  byPassTrustManagers, null);

            return sslContext;
        } catch (Exception e){
            MQDialogBox.openException(e);
        }

        return null;
    }
}

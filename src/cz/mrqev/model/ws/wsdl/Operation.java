package cz.mrqev.model.ws.wsdl;

import lombok.Getter;
import lombok.ToString;
import org.w3c.dom.Node;
import java.util.ArrayList;

@ToString
public class Operation {
    @Getter
    private String name;
    @Getter
    private String request;
    @Getter
    private String response;
    @Getter
    private ArrayList<String> soapHeaders = new ArrayList<>();

    public Operation(String name, String request, String response) {
        this.name = name;
        this.request = request;
        this.response = response;
    }

    public Operation(String name, String request, String response, ArrayList<Node> soapHeaders) {
        this.name = name;
        this.request = request;
        this.response = response;

        for (Node soapHeader : soapHeaders) {
            this.soapHeaders.add(soapHeader.getAttributes().getNamedItem("message").getNodeValue());
        }
    }

    public void addSoapHeader(String headerName) {
        soapHeaders.add(headerName);
    }
}

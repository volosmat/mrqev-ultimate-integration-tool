package cz.mrqev.model.ws.wsdl;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

public class WsdlHolder {
    private Document wsdl;
    private ArrayList<Operation> operations = new ArrayList<>();

    public WsdlHolder(String wsdlFilePath) throws ParserConfigurationException, IOException, SAXException {
        this.wsdl = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(wsdlFilePath));
        listOperations();
    }

    private void listOperations() {
        NodeList elements = wsdl.getElementsByTagName("portType");

        for (int i = 0; i < elements.getLength(); i++) {
            Node portType = elements.item(i);
            Node operation = searchNodesByName(portType.getChildNodes(), "operation").get(0);
            Node input = searchNodesByName(operation.getChildNodes(), "input").get(0);
            Node output = searchNodesByName(operation.getChildNodes(), "output").get(0);

            String[] inputMessageTokens = input.getAttributes().getNamedItem("message").getNodeValue().split(":");
            String[] outputMessageTokens = output.getAttributes().getNamedItem("message").getNodeValue().split(":");

            operations.add(
                    new Operation(
                            operation.getAttributes().getNamedItem("name").getNodeValue(),
                            input.getAttributes().getNamedItem("message").getNodeValue(),
                            output.getAttributes().getNamedItem("message").getNodeValue(),
                            getSoapHeadersForPortType(portType.getAttributes().getNamedItem("name").getNodeValue()))
            );
        }
    }

    private String getSchemaLocationForNamespace(String namespacePrefix) {
        NamedNodeMap attributes = wsdl.getAttributes();

        return "";
    }

    private ArrayList<Node> getSoapHeadersForPortType(String portType) {
        NodeList elements = wsdl.getElementsByTagName("binding");

        for (int i = 0; i < elements.getLength(); i++) {
            Node binding = elements.item(i);
            String[] typeTokens = binding.getAttributes().getNamedItem("type").getNodeValue().split(":");

            if (typeTokens[typeTokens.length-1].equals(portType)) {
                Node operation = searchNodesByName(binding.getChildNodes(), "operation").get(0);
                Node input = searchNodesByName(operation.getChildNodes(), "input").get(0);

                return searchNodesByName(input.getChildNodes(), "header");
            }
        }

        return null;
    }

    private ArrayList<Node> searchNodesByName(NodeList nodes, String nodeName) {
        ArrayList<Node> result = new ArrayList<>();

        for (int i=0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            String nodeNameTokens[] = node.getNodeName().split(":");
            // get last token e.g. soap:header need header
            if (nodeNameTokens[nodeNameTokens.length-1].equals(nodeName)) {
                result.add(node);
            }
        }

        return result;
    }

    @Override
    public String toString() {
        return "WsdlHolder{" +
                "operations=" + operations +
                '}';
    }
}

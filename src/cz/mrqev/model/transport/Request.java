package cz.mrqev.model.transport;

import cz.mrqev.main.Main;
import cz.mrqev.manager.PlaceholderManager;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.manager.WSSManager;
import lombok.Getter;

public abstract class Request {
    // message
    protected String data;

    // request options
    protected boolean fillTimestamps = false;
    protected boolean addWss = false;
    protected boolean generateSid = false;
    // generated sid
    @Getter
    protected String sid;

    // wss
    protected String username;
    protected String password;
    @Getter
    private int maximumWaitTime = 0;

    protected PlaceholderManager placeholderManager = Main.placeholderManager;

    public Request(String data, boolean fillTimestamps, boolean generateSid, String username, String password) {
        this.data = data;
        this.fillTimestamps = fillTimestamps;
        this.setGenerateSid(generateSid);
        this.username = username;
        this.password = password;
        this.addWss = true;
    }

    public Request(String data, boolean fillTimestamps, boolean generateSid) {
        this.data = data;
        this.fillTimestamps = fillTimestamps;
        this.setGenerateSid(generateSid);
    }

    public String getDataForPut()  {
        // set base request
        String putData = data;

        if (this.fillTimestamps) {
            putData = placeholderManager.getTimestampPlaceholder().replace(putData);
        }

        if (this.generateSid) {
            putData = placeholderManager.getUidPlaceholder().replace(putData, sid);
        }

        if (this.addWss) {
            putData = this.fillWssToRequest(putData);
        }

        return putData;
    }

    public void setSid(String sid) {
        this.generateSid = true;
        this.sid = sid;
    }

    public void setMaximumWaitTime(int maximumWaitTime) {
        this.maximumWaitTime = maximumWaitTime;
    }

    public void setGenerateSid(boolean generateSid) {
        this.generateSid = generateSid;
        if (this.generateSid)
            this.sid = placeholderManager.getUidPlaceholder().generateReplacement();
    }

    protected String fillWssToRequest(String request) {
        WSSManager wss = new WSSManager(this.username, this.password);
        request = request.replaceAll("</([a-zA-z0-9]*):Header>", wss.generateWssXml()+"</$1:Header>");

        return request;
    }
}

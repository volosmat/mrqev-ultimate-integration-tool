package cz.mrqev.model.transport;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public abstract class Response {
    @Getter
    private String responseData;
}

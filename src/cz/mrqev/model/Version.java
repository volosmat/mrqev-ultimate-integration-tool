package cz.mrqev.model;

import cz.mrqev.interfaces.BaseVersion;

public class Version extends BaseVersion {
    public Version(String version) {
        super(version);
    }
}

package cz.mrqev.model.xml;

import jlibs.xml.xsd.XSParser;
import lombok.Getter;
import org.apache.xerces.xs.XSModel;

import javax.xml.namespace.QName;

public class XsdHolder {
    @Getter
    private XSModel parsedXsd;

    public XsdHolder(String xsdPath) {
        parsedXsd = new XSParser().parse(xsdPath);
    }

    public String getRootElementName() {
        return parsedXsd.getComponents((short)2).item(0).getName();
    }

    public String getRootElementNamespace() {
        return parsedXsd.getComponents((short)2).item(0).getNamespace();
    }

    public QName getRootElement() {
        return new QName(getRootElementNamespace(), getRootElementName());
    }
}

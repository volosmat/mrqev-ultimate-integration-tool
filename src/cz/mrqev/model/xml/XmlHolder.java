package cz.mrqev.model.xml;

import jlibs.xml.sax.XMLDocument;
import jlibs.xml.xsd.XSInstance;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

public class XmlHolder {
    @Getter
    private String sourceXml;
    @Getter
    private Document parsedXml;

    public XmlHolder(String xml) {
        sourceXml = xml;
        DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            InputStream stream = new ByteArrayInputStream(xml.getBytes());
            parsedXml = builder.parse(stream);
            parsedXml.getDocumentElement().normalize();
        } catch (ParserConfigurationException | IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            // if input is not valid XML ignore it and don't load input queue
        }
    }

    public static XmlHolder fromXsd(String xsdPath) throws TransformerConfigurationException {
        return fromXsd(xsdPath, true, 2);
    }

    public static XmlHolder fromXsd(String xsdPath, boolean generateOptionalElements, int numberOfRepetitions) throws TransformerConfigurationException {
        XsdHolder xsdHolder = new XsdHolder(xsdPath);

        XSInstance xsInstance = new XSInstance();
        xsInstance.minimumElementsGenerated = numberOfRepetitions;
        xsInstance.maximumElementsGenerated = numberOfRepetitions;
        xsInstance.generateOptionalElements = generateOptionalElements; // null means random

        OutputStream osXml = new ByteArrayOutputStream();
        XMLDocument sampleXml = new XMLDocument(new StreamResult(osXml), true, 4, null);
        xsInstance.generate(xsdHolder.getParsedXsd(), xsdHolder.getRootElement(), sampleXml);

        return new XmlHolder(osXml.toString());
    }
}

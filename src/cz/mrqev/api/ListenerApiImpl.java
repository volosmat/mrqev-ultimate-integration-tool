package cz.mrqev.api;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.api.ListenerApi;
import cz.mrqev.interfaces.model.Consumers;
import cz.mrqev.main.Main;
import cz.mrqev.manager.ListenerManager;
import javafx.beans.value.ChangeListener;

import java.util.function.Function;

public class ListenerApiImpl implements ListenerApi {
    @Override
    public void addDataTextChangedListener(ChangeListener<String> listener) {
        ListenerManager listenerManager = Main.listenerManager;

        listenerManager.dataCodeAreaListeners.add(listener);
    }

    @Override
    public void addImportConsumersAction(String extensionFilter, Function<String, Consumers> processFunction) {
        MainController.getInstance().consumersImportFunction = processFunction;
        MainController.getInstance().consumerFileExtFilter = extensionFilter;
    }
}

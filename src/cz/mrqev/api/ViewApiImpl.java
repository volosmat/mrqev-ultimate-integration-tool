package cz.mrqev.api;

import cz.mrqev.controller.tab.BatchPutTabController;
import cz.mrqev.controller.tab.DataTabController;
import cz.mrqev.controller.tab.MqTabController;
import cz.mrqev.controller.tab.WsTabController;
import cz.mrqev.controller.window.MainController;
import cz.mrqev.exception.TemplateGroupNotFoundException;
import cz.mrqev.interfaces.BaseWindow;
import cz.mrqev.interfaces.LogTabControllerInterface;
import cz.mrqev.interfaces.TabController;
import cz.mrqev.interfaces.api.ViewApi;
import cz.mrqev.view.MQBrowseBox;
import cz.mrqev.view.MQDialogBox;
import cz.mrqev.view.MQWindow;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ViewApiImpl implements ViewApi {
    @Override
    public void openError(String title, String header, String content) {
        MQDialogBox.openError(title, header, content);
    }

    @Override
    public void openWarning(String title, String header, String content) {
        MQDialogBox.openWarning(title, header, content);
    }

    @Override
    public void openInformation(String title, String header, String content) {
        MQDialogBox.openInformation(title, header, content);
    }

    @Override
    public int openConfirmation(String title, String header, String content) {
        return MQDialogBox.openConfirmation(title, header, content);
    }

    @Override
    public void openException(Exception ex) {
        MQDialogBox.openException(ex);
    }

    @Override
    public Tab createTabWithTextArea(String name, String content, boolean prettyPrint) {
        return MQBrowseBox.createTabWithTextArea(name, content, prettyPrint);
    }

    @Override
    public Tab createTabWithCodeArea(String name, String content, boolean prettyPrint) {
        return MQBrowseBox.createTabWithCodeArea(name, content, prettyPrint);
    }

    @Override
    public void selectTabByName(TabPane tabPane, String tabName) {
        MQBrowseBox.selectTabByName(tabPane, tabName);
    }

    @Override
    public List<Menu> getMenu(String text) {
        return MainController.getInstance().menuBar_topMenu.getMenus().stream().filter(item -> item.getText().equals(text)).collect(Collectors.toList());
    }

    @Override
    public Menu addMenu(String text) {
        MenuBar menuBar = MainController.getInstance().menuBar_topMenu;
        Menu menu = new Menu(text);
        menuBar.getMenus().add(menu);
        return menu;
    }

    @Override
    public MenuItem addMenuItem(Menu toMenu, String text, EventHandler<ActionEvent> onAction) {
        MenuItem menuItem = new MenuItem(text);
        menuItem.setOnAction(onAction);
        toMenu.getItems().add(menuItem);
        return menuItem;
    }

    @Override
    public String getRequestData() {
        return DataTabController.getInstance().codearea_requestData.getText();
    }

    @Override
    public void setRequestData(String data) {
        DataTabController.getInstance().codearea_requestData.replaceText(data);
        DataTabController.getInstance().refreshDataStyling();
    }

    @Override
    public MenuItem addTemplateGroup(String name) {
        MenuItem groupItem = new MenuItem(name);
        groupItem.setDisable(true);
        groupItem.getStyleClass().add("context-menu-title");
        DataTabController.contextMenuCache.put(groupItem, new ArrayList<>());

        return groupItem;
    }

    @Override
    public MenuItem addTemplate(MenuItem group, String name, EventHandler<ActionEvent> onAction) throws TemplateGroupNotFoundException {
        if (!DataTabController.contextMenuCache.containsKey(group)) {
            throw new TemplateGroupNotFoundException(group.getText());
        }

        MenuItem templateItem = new MenuItem(name);
        templateItem.setOnAction(onAction);
        DataTabController.contextMenuCache.get(group).add(templateItem);

        return templateItem;
    }

    @Override
    public MenuItem getTemplateGroup(String name) throws TemplateGroupNotFoundException {
        List<MenuItem> groups= DataTabController.contextMenuCache.keySet().stream().filter(item -> item.getText().equals(name)).collect(Collectors.toList());

        if (groups.isEmpty()) {
            throw new TemplateGroupNotFoundException(name);
        } else {
            return groups.get(0);
        }
    }

    @Override
    public BaseWindow createAndShowWindow(URL templateResource, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean onTop, ClassLoader classLoader) {
        MQWindow mqWindow = new MQWindow(templateResource, title, width, height, newThread, owner, wait, false, onTop, classLoader);

        // add listener to close window with ESC pressed
        Pane basePane = mqWindow.getController().getPane();
        if (basePane != null) {
            Label focusNode = new Label();
            basePane.getChildren().add(focusNode);
            focusNode.setFocusTraversable(true);
            focusNode.requestFocus();

            basePane.setOnKeyPressed(keyEvent -> {
                // close window (ESC)
                if (keyEvent.getCode() == KeyCode.ESCAPE) {
                    mqWindow.getController().close();
                }
            });
        }

        return mqWindow;
    }

    @Override
    public void setQueue(String queueName) {
        MqTabController.getInstance().combobox_queues.setValue(queueName);
    }

    @Override
    public void addMqLogMessage(String message, boolean withTime) {
        MqTabController.getInstance().addLogMessage(message, withTime);
    }

    @Override
    public void addWsLogMessage(String message, boolean withTime) {
        WsTabController.getInstance().addLogMessage(message, withTime);
    }

    @Override
    public void addBatchLogMessage(String message, boolean withTime) {
        BatchPutTabController.getInstance().addLogMessage(message, withTime);
    }


}

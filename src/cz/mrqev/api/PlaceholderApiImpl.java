package cz.mrqev.api;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.interfaces.api.PlaceholderApi;
import cz.mrqev.main.Main;
import cz.mrqev.manager.PlaceholderManager;
import cz.mrqev.model.Placeholder;
import cz.mrqev.interfaces.function.TriFunction;

import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Supplier;

public class PlaceholderApiImpl implements PlaceholderApi {
    @Override
    public void addCustomPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill) {
        PlaceholderManager placeholderManager = MainController.getInstance().placeholderManager;
        placeholderManager.addCustomPlaceholder(new Placeholder(name, replace, replacementFunction, fill));
    }

    @Override
    public void removeCustomPlaceholder(String name) {
        MainController.getInstance().placeholderManager.removeCustomPlaceholder(name);
    }

    @Override
    public void setGenerateUidPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill) {
        PlaceholderManager placeholderManager = Main.placeholderManager;
        placeholderManager.setUidPlaceholder(new Placeholder(name, replace, replacementFunction, fill));
    }

    @Override
    public void setTimestampsPlaceholder(String name, TriFunction<String, String, Consumer<String>, String> replace, Supplier<String> replacementFunction, BiFunction<String, Consumer<String>, String> fill) {
        PlaceholderManager placeholderManager = Main.placeholderManager;
        placeholderManager.setTimestampPlaceholder(new Placeholder(name, replace, replacementFunction, fill));
    }
}

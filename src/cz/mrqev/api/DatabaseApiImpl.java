package cz.mrqev.api;

import cz.mrqev.interfaces.api.DatabaseApi;
import cz.mrqev.model.database.connection.DB2Connection;
import cz.mrqev.model.database.connection.OracleDBConnection;

import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseApiImpl implements DatabaseApi {
    @Override
    public Connection createOracleDBConnection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        return (new OracleDBConnection(hostname, username, password, timeout)).getConnection();
    }

    @Override
    public Connection createDB2Connection(String hostname, String username, String password, int timeout) throws SQLException, ClassNotFoundException {
        return (new DB2Connection(hostname, username, password, timeout)).getConnection();
    }
}

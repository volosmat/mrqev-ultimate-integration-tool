package cz.mrqev.api;

import cz.mrqev.interfaces.api.UtilsApi;
import cz.mrqev.interfaces.timer.RepeatableTask;
import cz.mrqev.main.Main;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.cache.BaseCache;
import cz.mrqev.model.cache.DatabaseCache;
import cz.mrqev.model.cache.MqCache;
import cz.mrqev.model.cache.OthersCache;
import cz.mrqev.model.json.JsonHolder;
import cz.mrqev.utils.timer.Timer;
import org.apache.commons.lang3.RandomStringUtils;

public class UtilsApiImpl implements UtilsApi {

    @Override
    public void createTimer(int repeatCounter, long delay, int direction, RepeatableTask task) {
        new Timer(repeatCounter, delay, direction, task);
    }

    @Override
    public String prettyPrint(String input) {
        return UtilsManager.prettyPrint(input);
    }

    @Override
    public void setCachedProperty(int cacheType, String key, Object value) {
        getCacheInstance(cacheType).addCachedRecord(key, value);
    }

    @Override
    public Object getCachedProperty(int cacheType, String key) {
        return getCacheInstance(cacheType).getCachedRecord(key);
    }

    @Override
    public String getConfigProperty(String key) {
        return Main.configManager.getProperty(key);
    }

    @Override
    public boolean isJson(String input) {
        return JsonHolder.isJson(input);
    }

    @Override
    public String getRandomAlphanumeric(int lenght) {
        return RandomStringUtils.randomAlphabetic(lenght);
    }

    private BaseCache getCacheInstance(int cacheType) {
        switch (cacheType) {
            case UtilsApi.DB_CACHE_TYPE:
                return DatabaseCache.getInstance();
            case UtilsApi.MQ_CACHE_TYPE:
                return MqCache.getInstance();
            default:
                return OthersCache.getInstance();
        }
    }
}

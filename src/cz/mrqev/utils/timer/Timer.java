package cz.mrqev.utils.timer;

import cz.mrqev.interfaces.timer.RepeatableTask;

public class Timer {
    final static public int DESC = 1;
    final static public int ASC = 2;
    private boolean stopTimer = false;

    public Timer(int repeatCounter, long delay, int direction, RepeatableTask task) {
        new Thread(() -> {
            int actualCounter;
            int end;

            if (direction == DESC) {
                actualCounter = repeatCounter;
                end = 0;
            } else {
                // ASC is default direction
                actualCounter = 0;
                end = repeatCounter;
            }

            while (!stopTimer && ((direction == DESC && actualCounter >= end) || (direction == ASC && actualCounter <= end))) {
                task.run(actualCounter, this::stop);

                if (direction == DESC) {
                    actualCounter--;
                } else {
                    actualCounter++;
                }

                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void stop() {
        stopTimer = true;
    }
}

package cz.mrqev.utils.cmd;

import cz.mrqev.main.Main;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;

public class CommandLineParser {
    private HashMap<String, String> arguments = new HashMap<>();

    public CommandLineParser(String[] args) {
        for (int i=0; i<args.length; i++){
            if (i+1 < args.length && isArgumentWithValue(args[i], args[i+1])) {
                // with value
                arguments.put(args[i], args[i+1]);
                i++;
            } else {
                // only command
                arguments.put(args[i], "");
            }
        }
    }

    public boolean checkArgument(String ... argumentName) {
        for (String name: argumentName) {
            if (!arguments.containsKey(name)) {
                return false;
            }
        }
        return true;
    }

    public String getArgumentValue(String argumentName) {
        return arguments.get(argumentName);
    }

    public Integer getIntArgumentValue(String argumentName) {
        return Integer.parseInt(arguments.get(argumentName));
    }

    private boolean isArgumentWithValue(String argument, String value) {
        // e.g. -argument -4; -argument value
        return argument.startsWith("-") && !StringUtils.isNumeric(argument) && (!value.startsWith("-") || StringUtils.isNumeric(argument));
    }

    public void printHelp() {
        System.out.println("");
        System.out.println("88b           d88              ,ad8888ba,");
        System.out.println("888b         d888             d8\"'    `\"8b");
        System.out.println("88`8b       d8'88            d8'        `8b");
        System.out.println("88 `8b     d8' 88 8b,dPPYba, 88          88  ,adPPYba, 8b       d8");
        System.out.println("88  `8b   d8'  88 88P'   \"Y8 88          88 a8P_____88 `8b     d8'");
        System.out.println("88   `8b d8'   88 88         Y8,    \"88,,8P 8PP\"\"\"\"\"\"\"  `8b   d8'");
        System.out.println("88    `888'    88 88          Y8a.    Y88P  \"8b,   ,aa   `8b,d8'");
        System.out.println("88     `8'     88 88           `\"Y8888Y\"Y8a  `\"Ybbd8\"'     \"8\"");
        System.out.println("v" + Main.version + " by JST <jostehlik@csob.cz> & MVO <mvolosin@csob.cz>");
        System.out.println("");
        System.out.println("Calling MQ / WS:");
        System.out.println("mrqev.bat (-mq | -ws)<repeats> -session <pathtosession> [-prettyprint] [-verbose] [-soap/rest] [-json]");
        System.out.println("-mq | -ws                   - call using MQ / WS (required), optional value is number of repeats (default 1)");
        System.out.println("-session <pathtosession>    - path to session file (required)");
        System.out.println("-soap/rest                  - only for -ws, type of web service call (default -soap)");
        System.out.println("-prettyprint                - pretty prints final response in console");
        System.out.println("-verbose                    - prints out information about process");
        System.out.println("-json                       - returns final response with SID as json");
        System.out.println("");
        System.out.println("Save queue:");
        System.out.println("mrqev.bat -savequeue -queue <queuename> -qmgr <queuemanager> -file <path>");
        System.out.println("-queue <queuename>          - source queue name (required)");
        System.out.println("-qmgr <queuemanager>        - source queue manager (required)");
        System.out.println("-file <path>                - target file (required)");
        System.out.println("");
        System.out.println("Load queue:");
        System.out.println("mrqev.bat -loadqueue -queue <queuename> -qmgr <queuemanager> -file <path>");
        System.out.println("-queue <queuename>          - target queue name (required)");
        System.out.println("-qmgr <queuemanager>        - target queue manager (required)");
        System.out.println("-file <path>                - source file (required)");
        System.out.println("");
        System.out.println("Copy queue:");
        System.out.println("mrqev.bat -copyqueue -sourcequeue <queuename> -sourceqmgr <queuemanager> -targetqueue <queuename> [-targetqmgr <queuemanager>]");
        System.out.println("-sourcequeue <queuename>    - source queue name (required)");
        System.out.println("-sourceqmgr <queuemanager>  - source queue manager (required)");
        System.out.println("-targetqueue <queuename>    - target queue name (required)");
        System.out.println("-targetqmgr <queuemanager>  - target queue manager (if not specified, uses source queue manager)");
    }
}

package cz.mrqev.utils.jar;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarLoader {
    private String jarFilePath;

    public JarLoader(String jarFilePath) {
        this.jarFilePath = jarFilePath;
    }

    public List<Class> loadClassesToRuntime() {
        List<Class> result = new ArrayList<>();

        try {
            JarFile jarFile = new JarFile(jarFilePath);
            Enumeration<JarEntry> entries = jarFile.entries();

            URL[] urls = {new URL("jar:file:" + jarFilePath + "!/")};
            URLClassLoader loader = URLClassLoader.newInstance(urls);

            while (entries.hasMoreElements()) {
                JarEntry entry = entries.nextElement();
                if (entry.isDirectory() || !entry.getName().endsWith(".class")) {
                    continue;
                }

                // -6 because of .class
                String className = entry.getName().substring(0, entry.getName().length() - 6);
                className = className.replace('/', '.');

                Class newClass = loader.loadClass(className);
                result.add(newClass);
            }

        } catch (ClassNotFoundException | IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}

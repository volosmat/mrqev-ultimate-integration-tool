package cz.mrqev.main;

import cz.mrqev.manager.*;
import cz.mrqev.model.cache.OthersCache;
import javafx.application.Application;
import javafx.stage.Stage;
import cz.mrqev.model.cache.DatabaseCache;
import cz.mrqev.model.cache.MqCache;
import cz.mrqev.model.database.connection.DBConnection;
import cz.mrqev.view.MQWindow;
import org.apache.commons.io.FilenameUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Main extends Application {
    final public static String version = "3.0.0";
    final public static double WINDOW_MIN_WIDTH = 816;
    final public static double WINDOW_MIN_HEIGHT = 715;

    // this is size of window when we open it
    final public static double WINDOW_DEFAULT_WIDTH = 816;
    final public static double WINDOW_DEFAULT_HEIGHT = 744;

    public static ConfigManager configManager = new ConfigManager();
    public static ListenerManager listenerManager = new ListenerManager();
    public static PluginManager pluginManager = new PluginManager();
    public static PlaceholderManager placeholderManager = new PlaceholderManager();

    public static boolean calledThroughCommandLine = false;

    @Override
    public void start(Stage primaryStage) throws Exception {
        // load plugin
        String pluginPath = Main.configManager.getProperty("settings.pluginPath");
        if (pluginPath != null) {
            pluginManager.loadPlugin(pluginPath);
        }

        // size means size of space for main grid in main window
        MQWindow window = new MQWindow("window/main", getAppName(), 800, 705, false, null, false, true, false);

        if (window.getStage() != null) {
            // clean all (store cache, configuration, close db connections etc.)
            window.getStage().setOnCloseRequest((event) -> {
                MqCache.getInstance().store();
                DatabaseCache.getInstance().store();
                DBConnection.closeConnections();
                configManager.saveConfiguration();
            });
            window.getStage().setOnHiding((event) -> {
                MqCache.getInstance().store();
                DatabaseCache.getInstance().store();
                DBConnection.closeConnections();
                configManager.saveConfiguration();
            });
        }

        // Deserialize all cache files (it must be here before first controller start to load UI controls from fxml template)
        MqCache.getInstance().load();
        DatabaseCache.getInstance().load();
        OthersCache.getInstance().load();

        // TODO
        //WsdlHolder wsdl = new WsdlHolder("C:\\git\\service-catalogue\\00 Public\\Services\\Payments\\Card\\Card.wsdl");
        //System.out.println(wsdl);
    }

    public static String getAppName() {
        return "MrQev - v"+version;
    }

    public static void main(String[] args) throws IOException {
        // there is difference between cipher specs names in OracleJVM and IbmJVM, this property force IBM MQ library to use Oracle Cipher specs names
        // if you want to run this application in IbmJVM you have to switch value of this option to true
        System.setProperty("com.ibm.mq.cfg.useIBMCipherMappings", String.valueOf(false));

        if (args.length == 0)
            launch(args);
        else {
            calledThroughCommandLine = true;
            CommandLineManager clm = new CommandLineManager(args);
        }
    }
}

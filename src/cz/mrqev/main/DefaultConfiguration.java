package cz.mrqev.main;

import cz.mrqev.interfaces.BaseConfiguration;

public class DefaultConfiguration extends BaseConfiguration {
    @Override
    public String getName() {
        return "No plugin selected";
    }

    @Override
    public String getVersion() {
        return "";
    }
}

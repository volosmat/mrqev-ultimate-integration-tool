package cz.mrqev.view;

import cz.mrqev.controller.window.MainController;
import javafx.stage.Stage;
import cz.mrqev.main.Main;

import java.util.ArrayList;
import java.util.List;

public class MQResponsiveness {
    private Stage windowStage;

    public MQResponsiveness(Stage windowStage) {
        this.windowStage = windowStage;

        // set minimum dimension when program works properly
        windowStage.setMinWidth(Main.WINDOW_MIN_WIDTH);
        windowStage.setMinHeight(Main.WINDOW_MIN_HEIGHT);

        windowStage.widthProperty().addListener((obs, oldValue, newValue) -> {
            if (!Double.isNaN(oldValue.doubleValue()))
                widthChanged(newValue.doubleValue() - oldValue.doubleValue());
        });

        windowStage.heightProperty().addListener((obs, oldValue, newValue) -> {
            if (!Double.isNaN(oldValue.doubleValue()))
                heightChanged(newValue.doubleValue() - oldValue.doubleValue());
        });
    }

    public void recalculateSizeAndPositionOfAllNodesInTab(String tabName) {
        widthChanged(tabName,windowStage.getWidth() - Main.WINDOW_DEFAULT_WIDTH);
        heightChanged(tabName, windowStage.getHeight() - Main.WINDOW_DEFAULT_HEIGHT);
    }

    private void widthChanged(double shift) {
        // tabName = null means recalculate all tabs
        widthChanged(null, shift);
    }

    private void heightChanged(double shift) {
        // tabName = null means recalculate all tabs
        heightChanged(null, shift);
    }

    private void widthChanged(String tabName, double shift) {
        List<MQNode> responsiveNodes = MainController.getInstance().getAllResponsiveNodesForTab(tabName);

        for (MQNode node: responsiveNodes) {
            if (node.shiftHorizontally() != 0) {
                node.getRegion().setLayoutX(node.getRegion().getLayoutX() + shift * node.shiftHorizontally());
            }

            if (node.changeWidth() != 0) {
                node.getRegion().setPrefWidth(node.getRegion().getPrefWidth() + shift * node.changeWidth());
            }
        }
    }

    private void heightChanged(String tabName, double shift) {
        List<MQNode> responsiveNodes = MainController.getInstance().getAllResponsiveNodesForTab(tabName);

        for (MQNode node: responsiveNodes) {
            if (node.shiftVertically() != 0) {
                node.getRegion().setLayoutY(node.getRegion().getLayoutY() + shift * node.shiftVertically());
            }

            if (node.changeHeight() != 0) {
                node.getRegion().setPrefHeight(node.getRegion().getPrefHeight() + shift * node.changeHeight());
            }
        }
    }
}

package cz.mrqev.view;

import javafx.application.Platform;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Optional;

public class MQDialogBox {

    public static void openError(String title, String header, String content) {
        Alert alert = createAlert(title, header, content, Alert.AlertType.ERROR);
        alert.showAndWait();
    }

    public static void openException(Exception ex) {
        Platform.runLater(() -> {
            Alert alert = createAlert("Exception happened :(", "Please send this to your nearest MrQev developer :)", ex.getMessage(), Alert.AlertType.ERROR);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            ex.printStackTrace(pw);
            String exceptionText = sw.toString();

            Label label = new Label("The exception stacktrace was:");

            TextArea textArea = new TextArea(exceptionText);
            textArea.setEditable(false);
            textArea.setWrapText(true);

            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);
            GridPane.setVgrow(textArea, Priority.ALWAYS);
            GridPane.setHgrow(textArea, Priority.ALWAYS);

            GridPane content = new GridPane();
            content.setMaxWidth(Double.MAX_VALUE);
            content.add(label, 0, 0);
            content.add(textArea, 0, 1);

            alert.getDialogPane().setContent(content);
            alert.getDialogPane().setPrefSize(800, 600);

            Platform.runLater(alert::showAndWait);

            ex.printStackTrace();
        });
    }

    public static void openNewVersionWindow(String title, String header, String changelog) {
        Alert alert = createAlert(title, header, changelog, Alert.AlertType.INFORMATION);

        Label label = new Label("Please download new version from confluence :-)");
        Label labelChangelog = new Label("Changelog:");

        TextArea textArea = new TextArea(changelog);
        textArea.setEditable(false);
        textArea.setWrapText(true);

        textArea.setMaxWidth(Double.MAX_VALUE);
        textArea.setMaxHeight(Double.MAX_VALUE);
        GridPane.setVgrow(textArea, Priority.ALWAYS);
        GridPane.setHgrow(textArea, Priority.ALWAYS);

        GridPane content = new GridPane();
        content.setMaxWidth(Double.MAX_VALUE);
        content.add(label, 0, 0);
        content.add(labelChangelog, 0, 1);
        content.add(textArea, 0, 2);

        alert.getDialogPane().setContent(content);
        alert.getDialogPane().setPrefSize(600, 400);

        alert.showAndWait();
    }

    public static void openWarning(String title, String header, String content) {
        Alert alert = createAlert(title, header, content, Alert.AlertType.WARNING);
        alert.showAndWait();
    }

    public static void openInformation(String title, String header, String content) {
        Alert alert = createAlert(title, header, content, Alert.AlertType.INFORMATION);
        alert.showAndWait();
    }

    public static int openConfirmation(String title, String header, String content) {
        Alert alert = createAlert(title, header, content, Alert.AlertType.CONFIRMATION);
        ButtonType buttonYes = new ButtonType("Yes");
        ButtonType buttonNo = new ButtonType("No");
        ButtonType buttonCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonYes, buttonNo, buttonCancel);
        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonYes) {
            return 1;
        } else if (result.get() == buttonNo) {
            return 0;
        } else {
            return -1;
        }
    }

    private static Alert createAlert(String title, String header, String content, Alert.AlertType alertType) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        stage.getIcons().add(new Image(MQWindow.class.getResourceAsStream("/cz/mrqev/view/images/icon.png")));

        return alert;
    }
}

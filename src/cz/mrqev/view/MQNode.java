package cz.mrqev.view;

import cz.mrqev.interfaces.annotation.Responsive;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.layout.Region;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.interfaces.annotation.Serializable;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Optional;

public class MQNode {
    private Region region;
    private Responsive responsive;
    private Serializable serializable;

    public MQNode(Region region) {
        this.region = region;
    }

    public MQNode(Region region, Responsive responsive) {
        this.region = region;
        this.responsive = responsive;
    }

    public MQNode(Region region, Serializable serializable) {
        this.region = region;
        this.serializable = serializable;
    }

    public void setResponsive(Responsive responsive) {
        this.responsive = responsive;
    }

    public void setSerializable(Serializable serializable) {
        this.serializable = serializable;
    }

    public Object getValue() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SingleSelectionModel selectionModel = getSelectionModel();
        if (selectionModel == null) {
            Method method = region.getClass().getMethod(serializable.getMethod());
            return method.invoke(region);
        } else {
            return selectionModel.selectedItemProperty().getValue();
        }
    }

    public String getStringValue() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Optional<Object> value = Optional.ofNullable(getValue());

        return value.isPresent() ? String.valueOf(getValue()) : null;
    }

    public void setValue(Object value) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        SingleSelectionModel selectionModel = getSelectionModel();

        // valueOf method to convert value to correct datatype for node set method
        Method valueOf;
        if (serializable.setMethodParam() == String.class) {
            // String is exception because dont have String.valueOf(String) only Object
            valueOf = serializable.setMethodParam().getMethod("valueOf", Object.class);
        } else {
            // All other datatype e.g Integer. Boolean or custom like MQMessageTypeModel have valueOf(String) method
            valueOf = UtilsManager.getWrapperForPrimitive(serializable.setMethodParam()).getMethod("valueOf", String.class);
        }

        if (selectionModel == null) {
            // get set method
            Method method = region.getClass().getMethod(serializable.setMethod(), serializable.setMethodParam());
            // first invoke valueOf method and then set method
            method.invoke(region, valueOf.invoke(null, value));
        } else {
            // if node contains selection cz.mrqev.model e.g Combobox use this cz.mrqev.model
            selectionModel.select(valueOf.invoke(null, value));
        }
    }

    public Region getRegion() {
        return region;
    }

    public String getSerializableName() {
        return (serializable != null) ? serializable.serializedName() : null;
    }

    public double shiftVertically() {
        return (responsive != null) ? responsive.shiftVertically() : 0;
    }

    public double shiftHorizontally() {
        return (responsive != null) ? responsive.shiftHorizontally() : 0;
    }

    public double changeHeight() {
        return (responsive != null) ? responsive.changeHeight() : 0;
    }

    public double changeWidth() {
        return (responsive != null) ? responsive.changeWidth() : 0;
    }

    private SingleSelectionModel getSelectionModel() {
        try {
            Method method = region.getClass().getMethod("getSelectionModel");
            return (SingleSelectionModel) method.invoke(region);
        } catch (NoSuchMethodException e) {
            return null;
        } catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return null;
        }
    }
}

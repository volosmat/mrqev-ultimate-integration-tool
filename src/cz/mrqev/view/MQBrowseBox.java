package cz.mrqev.view;

import cz.mrqev.controller.window.SearchController;
import javafx.scene.control.Alert;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import cz.mrqev.manager.UtilsManager;
import cz.mrqev.model.mq.wrapper.message.MQMessageWrapper;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.CodeArea;
import org.fxmisc.richtext.LineNumberFactory;

import java.util.HashMap;
import java.util.List;

public class MQBrowseBox {

    public static void openBrowseDialog(MQMessageWrapper message) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);

        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.setAlwaysOnTop(true);
        stage.getIcons().add(new Image(MQWindow.class.getResourceAsStream("/cz/mrqev/view/images/icon.png")));

        alert.getDialogPane().setPrefWidth(1000.0);
        alert.getDialogPane().setPrefHeight(800.0);
        alert.setTitle("Viewing message");
        alert.setHeaderText(null);
        alert.setGraphic(null);

        String prettydata = UtilsManager.prettyPrint(message.getData());

        if (prettydata == null)
            prettydata = message.getData();

        TabPane tabPane = new TabPane();
        Tab messageTab = new Tab();
        messageTab.setClosable(false);
        Tab mqmdTab = new Tab();
        mqmdTab.setClosable(false);
        messageTab.setText("Message");
        mqmdTab.setText("MQMD");

        tabPane.getTabs().addAll(messageTab, mqmdTab);

        CodeArea codearea = new CodeArea(prettydata);
        codearea.setEditable(false);
        codearea.setWrapText(true);
        codearea.setParagraphGraphicFactory(LineNumberFactory.get(codearea));
        codearea.getStylesheets().add(MQBrowseBox.class.getResource(TextHighlighter.CSS_FILE_PATH).toExternalForm());
        codearea.setStyleSpans(0, TextHighlighter.computeHighlighting(codearea.getText()));

        VirtualizedScrollPane<CodeArea> vsPane = new VirtualizedScrollPane<>(codearea);
        vsPane.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);

        TextArea textAreaMqmd = new TextArea("");
        textAreaMqmd.setText(message.printMqmd());

        GridPane.setVgrow(tabPane, Priority.ALWAYS);
        GridPane.setHgrow(tabPane, Priority.ALWAYS);

        GridPane content = new GridPane();
        content.setMaxWidth(Double.MAX_VALUE);
        tabPane.setMaxWidth(Double.MAX_VALUE);
        messageTab.setContent(vsPane);
        mqmdTab.setContent(textAreaMqmd);
        content.add(tabPane, 0, 0);

        // Set expandable Exception into the dialog pane.
        alert.getDialogPane().setContent(content);

        alert.showAndWait();
    }

    // For search and replace window to know where to search
    private static HashMap<String, CodeArea> logTextAreas = new HashMap<>();

    public static Tab createTabWithTextArea(String name, String content, boolean prettyPrint) {
        content = checkContent(content, prettyPrint);

        TextArea textArea = new TextArea(content);
        textArea.setEditable(false);
        textArea.setWrapText(true);
        Tab tab = new Tab(name, textArea);
        tab.setClosable(false);

        return tab;
    }

    public static Tab createTabWithCodeArea(String name, String content, boolean prettyPrint) {
        content = checkContent(content, prettyPrint);

        CodeArea codearea = new CodeArea(content);
        codearea.setEditable(false);
        codearea.setWrapText(true);
        codearea.setParagraphGraphicFactory(LineNumberFactory.get(codearea));
        codearea.setEditable(false);
        logTextAreas.put(name, codearea);

        if (!content.isEmpty()) {
            codearea.getStylesheets().add(MQBrowseBox.class.getResource(TextHighlighter.CSS_FILE_PATH).toExternalForm());
            codearea.setStyleSpans(0, TextHighlighter.computeHighlighting(codearea.getText()));
        }
        VirtualizedScrollPane<CodeArea> vsPane = new VirtualizedScrollPane<>(codearea);
        vsPane.setMaxSize(Double.MAX_VALUE,Double.MAX_VALUE);

        Tab tab = new Tab(name, vsPane);
        tab.setClosable(false);

        return tab;
    }

    /**
     * Check content if its not null and do pretty print if its neccessary
     * @param content - content for textarea
     * @param prettyPrint - if true do pretty print
     * @return - validated content
     */
    private static String checkContent(String content, boolean prettyPrint) {
        if (content == null)
            content = "";

        if (prettyPrint && !content.isEmpty())
            content = UtilsManager.prettyPrint(content);

        return content;
    }

    /**
     * Select specific tab with tabName, if not found select first tab
     * @param tabPane - tabpane with tabs to select
     * @param tabName - tab name for select
     */
    public static void selectTabByName(TabPane tabPane, String tabName) {
        List<Tab> tabs = tabPane.getTabs();

        for (int i=0; i<tabs.size(); i++) {
            Tab tab = tabs.get(i);

            if (tab.getText().equals(tabName)) {
                tabPane.getSelectionModel().select(i);
                return;
            }
        }
        // default select first tab
        tabPane.getSelectionModel().select(0);
    }
}

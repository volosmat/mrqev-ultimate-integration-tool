package cz.mrqev.view;

import cz.mrqev.controller.window.BaseWindowController;
import cz.mrqev.interfaces.BaseWindow;
import cz.mrqev.interfaces.WindowController;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MQWindow implements BaseWindow {
    private static LinkedHashMap<String, MQWindow> alreadyOpened = new LinkedHashMap<>();

    private Stage windowStage;
    private MQResponsiveness responsiveness;
    private ClassLoader classLoader;
    private WindowController windowController;

    public MQWindow(String template, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean maximizable, boolean onTop) {
        URL templateResource = getClass().getResource("/cz/mrqev/view/templates/" + template +".fxml");
        checkAndInitialize(templateResource, title, width, height, newThread, owner, wait, maximizable, onTop);
    }

    public MQWindow(URL templateResource, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean maximizable, boolean onTop) {
        checkAndInitialize(templateResource, title, width, height, newThread, owner, wait, maximizable, onTop);
    }

    public MQWindow(URL templateResource, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean maximizable, boolean onTop, ClassLoader classLoader) {
        this.classLoader = classLoader;
        checkAndInitialize(templateResource, title, width, height, newThread, owner, wait, maximizable, onTop);
    }

    private void checkAndInitialize(URL templateResource, String title, int width, int height, boolean newThread, Window owner, boolean wait, boolean maximizable, boolean onTop) {
        // prevent double opening of same window
        // can open one window multiple times in separated threads
        if (!alreadyOpened.containsKey(title + templateResource.getPath()) || newThread) {
            alreadyOpened.put(title + templateResource.getPath(), this);

            if (newThread) {
                this.createWindowInNewThread(templateResource, title, width, height, owner, maximizable, onTop);
            } else {
                this.createWindow(templateResource, title, width, height, owner, wait, maximizable, onTop);
            }
        }
    }

    /*
     *   Create new window in separate thread for better user experience
     */
    private void createWindowInNewThread(URL templateResource, String title, int width, int height, Window owner, boolean maximizable, boolean onTop) {
        Task<Parent> loadTask = new Task<Parent>() {
            @Override
            public Parent call() throws IOException, InterruptedException {
                return createParent(templateResource);
            }
        };

        loadTask.setOnSucceeded(e -> {
            windowStage = this.createStage(loadTask.getValue(), title, width, height, owner);
            if (maximizable) {
                responsiveness = new MQResponsiveness(windowStage);
            } else {
                windowStage.resizableProperty().setValue(Boolean.FALSE);
            }
            windowStage.getIcons().add(new Image(getClass().getResourceAsStream("/cz/mrqev/view/images/icon.png")));
            windowStage.setAlwaysOnTop(onTop);
            // remove window from opened window map after close
            windowStage.setOnCloseRequest(event -> alreadyOpened.remove(title + templateResource.getPath()) );
            windowStage.setOnHiding(event -> alreadyOpened.remove(title + templateResource.getPath()));
            windowStage.show();
        });

        loadTask.setOnFailed(e -> loadTask.getException().printStackTrace());

        Thread thread = new Thread(loadTask);
        thread.start();
    }

    /*
     *   Create new window in same thread as main class
     */
    private void createWindow(URL templateResource, String title, int width, int height, Window owner, boolean wait, boolean maximizable, boolean onTop) {
        try {
            Parent parent = createParent(templateResource);

            windowStage = this.createStage(parent, title, width, height, owner);
            windowStage.setAlwaysOnTop(onTop);
            // remove window from opened window map after close
            windowStage.setOnCloseRequest(event -> alreadyOpened.remove(title + templateResource.getPath()));
            windowStage.setOnHiding(event -> alreadyOpened.remove(title + templateResource.getPath()));

            if (maximizable) {
                responsiveness = new MQResponsiveness(windowStage);
            } else {
                windowStage.resizableProperty().setValue(Boolean.FALSE);
                windowStage.sizeToScene();
            }
            windowStage.getIcons().add(new Image(getClass().getResourceAsStream("/cz/mrqev/view/images/icon.png")));
            if (!wait)
                windowStage.show();
            else
                windowStage.showAndWait();
        } catch (IOException e) {
            MQDialogBox.openException(e);
        }
    }

    private FXMLLoader createLoader(URL template) {
        return new FXMLLoader(template);
    }

    private Parent createParent(URL template) throws IOException {
        FXMLLoader loader = this.createLoader(template);
        // set class loader (for plugin windows)
        if (classLoader != null) {
            loader.setClassLoader(classLoader);
        }

        Parent parent = loader.load();
        windowController = loader.getController();
        windowController.setWindow(this);

        return parent;
    }

    private Scene createScene(Parent parent, int width, int height) {
        return new Scene(parent, width, height);
    }

    private Stage createStage(Parent parent, String title, int width, int height, Window owner) {
        Scene scene = this.createScene(parent, width, height);
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setScene(scene);
        if (owner != null) {
            stage.initModality(Modality.WINDOW_MODAL);
            stage.initOwner(owner);
        }

        return stage;
    }

    public static void closeLastOpenedWindow() {
        if (alreadyOpened.size() > 1) {
            List<Map.Entry<String,MQWindow>> entryList = new ArrayList<>(alreadyOpened.entrySet());
            Map.Entry<String, MQWindow> lastEntry = entryList.get(entryList.size()-1);

            lastEntry.getValue().windowStage.close();
        }
    }

    @Override
    public Stage getStage() {
        return windowStage;
    }

    public MQResponsiveness getResponsiveness() {
        return responsiveness;
    }

    @Override
    public WindowController getController() {
        return windowController;
    }

    @Override
    public ClassLoader getClassLoader() {
        return classLoader;
    }
}

package cz.mrqev.view;

import cz.mrqev.controller.window.MainController;
import cz.mrqev.controller.tab.RequestTabController;
import cz.mrqev.event.EventProducer;
import cz.mrqev.event.TabCreatedEvent;
import cz.mrqev.event.listener.TabListener;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import cz.mrqev.main.Main;
import cz.mrqev.manager.ListenerManager;
import cz.mrqev.manager.TooltipManager;

import java.io.IOException;
import java.util.EventListener;

public class MQTab extends EventProducer {
    private String template;
    private String title;
    private TabPane owner;

    public MQTab(String template, String title, TabPane owner) {
        this.template = template;
        this.title = title;
        this.owner = owner;
    }

    private void fireTabCreatedEvent() {
        for (EventListener eventListener: listeners) {
            TabListener tabListener = (TabListener) eventListener;
            tabListener.tabCreated(new TabCreatedEvent(this));
        }
    }

    public void createTabDynamically() {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/cz/mrqev/view/templates/" + template + ".fxml"));

        try {
            Tab newTab;
            newTab = new Tab();
            newTab.setClosable(true);

            Label nameLabel = new Label(title);
            newTab.setGraphic(nameLabel);

            TextField renameField = new TextField(title);

            ListenerManager listenerManager = new ListenerManager();
            listenerManager.createProcessTabNameChangeListener(newTab, nameLabel, renameField);
            listenerManager.createInitializeTabNameChangeListener(newTab, nameLabel, renameField);

            // add and select new tab
            owner.getTabs().add(newTab);
            owner.getSelectionModel().selectLast();

            // load fxml template for tab
            Parent parent = loader.load();
            newTab.setContent(parent);

            newTab.setOnCloseRequest(event -> {
                //TODO: add check if session changed
                if (checkIfSaved(event, title)) {
                    checkIfTabIsLastOpened();
                }
            });

            fireTabCreatedEvent();
        } catch (IOException e) {
            MQDialogBox.openException(e);
        }

    }

    public static boolean checkIfTabIsLastOpened() {
        int requestTabsCount = MainController.getInstance().requestTabPane.getTabs().size();
        if (requestTabsCount == 2) {
            // create new empty tab immediately after last tab close
            MainController.getInstance().addRequestTab();
            return true;
        }

        return false;
    }

    public static boolean checkIfSaved(Event event, String tabTitle) {
        //TODO: tady musi jeste byt session check na zmenene hodnoty
        //hlavne driv, nez tohle bude public, aby se nezaviraly neulozene taby kdyz je zaskrtly setting!
        if (Main.configManager.getBoolProperty("settings.alwaysConfirmClosingOfTab")) {
            int result = MQDialogBox.openConfirmation("Closing tab...", "Do you want to save session?", "");
            if (result == 1) { //if yes
                if (RequestTabController.getInstance().sessionManager.saveSession()) {
                    MainController.removeTabController(tabTitle);
                } else {
                    // if we click yes and then dont save e.g. click cancel
                    if (event != null) event.consume();
                    return false;
                }
            } else if (result == 0) { //if no
                MainController.removeTabController(tabTitle);
            } else { //if cancel, we don't want to close the tab
                if (event != null) event.consume();
                return false;
            }
        }

        return true;
    }

    public static String getTabTitle(Tab tab) {
        if (tab.getText() == null) {
            if (tab.getGraphic() instanceof TextField) {
                TextField textField = (TextField) tab.getGraphic();
                return textField.getText();
            } else {
                Label nameLabel = (Label) tab.getGraphic();
                return nameLabel.getText();
            }
        } else {
            return tab.getText();
        }
    }

    public static void changeTabName(String newName) {
        Tab tab = MainController.getInstance().getSelectedTab();
        Label nameLabel = new Label(MQTab.getTabTitle(tab));
        TextField renameField = new TextField(newName);

        ListenerManager listenerManager = new ListenerManager();
        listenerManager.createProcessTabNameChangeListener(tab, nameLabel, renameField);
        listenerManager.createInitializeTabNameChangeListener(tab, nameLabel, renameField);


        MQTab.changeTabName(tab, nameLabel, renameField,true);
    }

    public static void changeTabName(Tab tab, Label nameLabel, TextField renameField, boolean cancelWhenTooltipShows) {
        // hide and remove tooltip if already exists
        if (renameField.getTooltip() != null) {
            renameField.getTooltip().hide();
            renameField.setTooltip(null);
        }

        TooltipManager tooltipManager = new TooltipManager();

        if (renameField.getText().length() < 4) {
            // tab name must be at least 4 chars long
            tooltipManager.createAndShowNodeTooltip(renameField, "Name of tab must be at least 4 characters long.");

            // cancel name change textField when tooltip shows
            if (cancelWhenTooltipShows) {
                tab.setGraphic(nameLabel);
            }

        } else if (!nameLabel.getText().equals(renameField.getText()) && !MainController.tabNameExists(renameField.getText())) {
            // new name is different then old name and its unique (change tab name)
            String oldName = nameLabel.getText();
            RequestTabController controller = MainController.getTabController(oldName);

            // add tab controller with new tab name
            MainController.addTabController(renameField.getText(), controller);
            // remove old tab name from controllers map
            MainController.removeTabController(oldName);

            nameLabel.setText(renameField.getText());
            tab.setGraphic(nameLabel);
        } else if (nameLabel.getText().equals(renameField.getText())) {
            // new name is same as old name (just change textfield to label)
            nameLabel.setText(renameField.getText());
            tab.setGraphic(nameLabel);
        } else if (MainController.tabNameExists(renameField.getText())) {
            // new name already exists (show error tooltip)
            tooltipManager.createAndShowNodeTooltip(renameField, "Name already exists, please choose another.");

            // cancel name change textField when tooltip shows
            if (cancelWhenTooltipShows) {
                tab.setGraphic(nameLabel);
            }
        }
    }
}
